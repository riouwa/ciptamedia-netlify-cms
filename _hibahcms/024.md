---
title: Portal Komunitas
date: 2014-01-27 11:08:00 +07:00
nomor: '024'
foto: /static/img/hibahcms/024.png
nama: Wahyu Hidayat
lokasi: Deli Serdang, Sumatera Utara
durasi: 12 Bulan
target: Pelajar-Mahasiswa-Komunitas di Medan
sukses: |-
  1. Pertambahan data komunitas
  2. Pertambahan kegiatan
  3. Pertambahan downloader kebutuhan aplikasi launcher ceritamedan 1.000 perbulan
tipe: Teks informasi komunitas update, video komunitas
strategi: |-
  Sistem Viral Marketing, e word of mouth marketing.

  Minimal 100 penerima
dana: Rp. 50 Juta
deskripsi: Program pengembangan digital local content
masalah: Mendigitalkan anak-anak muda kreatif
solusi: Merangkul komunitas di Medan, yang mengembangkan komunitasnya dengan mengandalkan
  perangkat selulernya untuk lebih berinovasi, berkreasi.
judul: Portal Komunitas
organisasi: NA
kuantitas: NA
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
