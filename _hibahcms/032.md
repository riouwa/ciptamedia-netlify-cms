---
title: Ciburial Cyber Village
date: 2014-01-27 11:08:00 +07:00
nomor: '032'
foto: /static/img/hibahcms/032.png
nama: Ayi Sumarna
lokasi: Bandung, Jawa Barat
durasi: 6 Bulan
target: Semua Warga Desa Ciburial
sukses: Semua warga Desa Ciburial, di wilayah Desa Ciburial dapat mengakses
  internet secara gratis.
tipe: Perangkat Jaringan Internet Nirkabel
strategi: Melalui sosialisasi, kegiatan roadshow, dan pelatihan-pelatihan. Pendampingan
  intensif.
dana: Rp. 500 Juta
deskripsi: Pembangunan Jaringan Wifi Desa di Wilayah Desa Ciburial untuk mendukung
  pemberdayaan masyarakat Desa Ciburial sebagai Desa Wisata
masalah: Menyediakan akses internet gratis di seluruh wilayah Desa Ciburial
solusi: Membangun titik-titik hostpot (wifi) gratis di seluruh wilayah Desa Ciburial
judul: Ciburial Cyber Village
organisasi: NA
kuantitas: NA
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
