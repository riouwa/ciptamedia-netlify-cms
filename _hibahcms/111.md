---
title: Mount Information
date: 2014-03-10 11:08:00 +07:00
nomor: 111
foto: /static/img/hibahcms/111.png
nama: Nur Hasbiah
lokasi: Sibanggor Julu, Sumatra Utara
organisasi: NA
judul: Mount Information
durasi: 3 Bulan
target: Pengungsi korban gunung & para wisatawan.
sukses: 100 orang per bulan (terutama pemilik android)
tipe: Tipe berupa gambar peta serta keterangan
strategi: |-
  Membuat iklan di televisi, di koran, radio, share di Fb dan media lainnya.

  Dengan memeriksa GPS yang diletakkan pada setiap pegunungan
kuantitas: NA
dana: Rp. 100 Juta
deskripsi: Aplikasi untuk gunung aktif atif dan gunung belum aktif diseluruh indonesia.
  Jadi, keadaan gunung itu akan terdeteksi sendiri. Jika seluler kita memilki aplikasi
  ini, maka akan secara langsung hp kita berdering sebagai tanda bahwa salah satu
  gunung itu memiliki masalah. Dengan memakai GPS kita akan mengetahui keberadaan
  gunung itu dan sekaligus kondisi gunung.
masalah: Masalah keterbatasan informasi untuk masyarakat terpencil di daerah pegunungan
solusi: Membuat aplikasi ini , pertama akan melihat kondisi dan keberadaan gunung
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
