---
title: Game Multiplayer Pembelajaran Beribadah Berbasis Android
date: 2014-04-15 11:08:00 +07:00
nomor: 222
foto: /static/img/hibahcms/222.png
nama: Mohammad Fauzan
lokasi: Sumenep, Madura
organisasi: NA
judul: Game Multiplayer Pembelajaran Beribadah Berbasis Android
durasi: 12 Bulan
target: anak sd untuk tahap pmebelajaran
sukses: rating pengguna dalam 6 bulan
tipe: game multy player untuk pembelajran agama
strategi: upload playstore.lakukan testing secara bertahap aga dapat diperbaiki secara
  bertahap pula, sehingga kepuasan pengguna meningkat
kuantitas: NA
dana: Rp.2 Juta
deskripsi: game multi player yang diperuntukkan untuk anak sd kelas 1 – 5 yang memiliki
  konten untuk pembelajran agama, dimana antar player bisa berkomunikasi, saling membantu
  dalam pembelajran.
masalah: desaign yang atraktif
solusi: mencari refrensi dari game – game lain
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
