---
title: Perancangan Sistem Terdistribusi Menggunakan Mobile Device Untuk Pengukuran
  dan Pengkontrolan Kebakaran Smart Home
date: 2014-04-04 11:08:00 +07:00
nomor: 177
foto: /static/img/hibahcms/177.png
nama: Mohammad Yusuf Hidayat
lokasi: Lumajang, Jawa Timur
organisasi: NA
judul: Perancangan  Sistem Terdistribusi Menggunakan Mobile Device Untuk Pengukuran
  dan Pengkontrolan Kebakaran Smart Home
durasi: 7 Bulan
target: Masyarakat perumahan padat penduduk dan pertokoan atau ruko
sukses: Pengukuran keberhasilan dilakukan dengan uji coba pada suhu yang sesuai
  dengan suhu kebakaran. Karena tidak mungkin penulis melakukan pengukuran pada rumah
  yang kebakaran.
tipe: aplikasi desktop, dan mobile
strategi: |-
  menggunakan jejaring sosial seperti facebook, twitter dll. karena saya masih kuliah.

  membuat penjelasan singkat tentang aplikasi. Entah nanti dengan cara sosialisasi ataupun yang lain.
  dibutuhkan perangkat yang bisa terhubung.
kuantitas: NA
dana: Rp. 550 Juta
deskripsi: Pada proyek ini smartphone berperan sebagai sensor untuk temperature kebakaran
  yang terjadi. Jika suhu sudah memenuhi kriteria terjadi kebakaran, maka smartphone
  akan mengirim informasi kepada PC yang berperan sebagai proxy. Jika sudah masuk
  maka informasi di olah di workfolw management untuk pendistribusiannya, entah itu
  ke pemadam kebakaran, pihak medis, ataupun pemilik rumah itu sendiri.
masalah: Seperti yang sudah diberitakan di media-media masa, kebanyakan kebakaran
  terjadi di area perumahan padat penduduk dan ruko. Jika tidak ada tindakan yang
  cepat maka akan ada banyak kerugian dan korban. Apalagi jika kebakaran tersebut
  terjadi di malam hari ketika semua pemilik rumah sedang terlelap. Seperti yang diberitakan
  di sini http://www.merdeka.com/peristiwa/ruko-di-bekasi-terbakar-dua-balita-tewas-dan-ibunya-kritis.html.
  Ketika kebakaran terjadi, ternyata warga tidak tahu. Maka yang terjadi petugas pemadam
  kebakaran pun tidak hadir.
solusi: Sesuai dengan masalah di atas, maka kami ingin mengajukkan ide untuk membuat
  sistem yang terdistribusi untuk penyampaian informasi kepada pemilik rumah, pemadam
  kebakaran, dan tenaga medis secara langsung ketika terjadi kebakaran tanpa harus
  si pemilik rumah menghubungi pihak-pihak tersebut.
---


### {{ page.nohibah }} - {{ page.title }}

{{ page.deskripsi }}
