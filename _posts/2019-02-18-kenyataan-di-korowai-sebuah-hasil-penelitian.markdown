---
title: Kenyataan di Korowai  (Sebuah Hasil Penelitian)
date: 2019-02-18T06:36:36.898Z
categories:
  - CME
  - Kajian
  - Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai
tags:
  - CME
  - Korowai
  - Papua
  - Suku terasing
  - Antropologi
author: 'Rhidian Yasminta Wasaraka '
comments: true
img: /uploads/blog/peta-kampung-kampung-di-korowai.jpg
---
Daerah yang remote

![](/uploads/blog/korowai-dalam-peta-papua.png)

Peta Papua,  sumber: Society of other-Rupert stach

Wilayah adat suku Korowai berada ditengah-tengah pulau Papua, (jika membayangkan luas Papua, maka bayangkan bahwa Papua itu luasnya 3½ kali pulau jawa) berada di kaki pegunungan bintang, masuk dalam lima kabupaten pemekaran (Mapi, Asmat, Boven Dieogul, Yahokimo dan Pegunungan Bintang). Populasi paling besar dari suku Korowai berada di kabupaten Boven Dieogul dan Mapi, sedangkan Kabupaten Asmat dan Pegunungan Bintang hanya sebagian kecil saja. Walaupun termasuk dalam wilayah lima kabupaten namun jarak untuk menuju ibu kota kabupaten masih sangat jauh.Inilah yang selalu menjadi alasan banyak pihak, bahwa daerah ini jauh dan susah di jangkau. Namun begitu Korowai sendiri adalah daerah kunjungan wisata budaya yang sangat terkenal dikalangan pecinta petualangan terestrial (daratan/hutan). Kepopulerannya bahkan sudah ada sebelum Kepulauan Raja Ampat di Papua Barat, juga tak surut ketika bom Bali dan isu terorisme merebak di Indonesia. Tiap tahun selalu ada saja rombongan group touris yang datang ke wilayah ini. Rata-rata rombongan ini berjumlah 8 sampai 14 orang. dan dalam satu tahun bisa sampai lima group yang datang ke Korowai. mereka datang dengan menggunakan pesawat-pesawat perintis carteran dari Bandara Sentani Jayapura. kemudian landing di bandara Yaniruma atau Danuwage, atau bisa juga dari di daerah Dekai lalu menyusuri sungai untuk masuk ke dusun-dusun tempat pesta dilangsungkan. 

Tak ada Korowai Batu

Yang ada adalah Korowai utara dan Korowai selatan, istilah ini digunakan oleh antropolog Rupert Stach untuk membedakan orang Korowai karena dialeg bahasanya saja yang sedikit berbeda, Jika di utara setiap katanya ada seperti penekanan “dhe’ “ sedangkan selatan ada penekanan “tse” itu saja. Yang lain sama jika dilihat dari tujuh unsur budaya (mata pecaharian, agama, kepemimpinan,dst). Lalu darimana datangnya istilah Korowai Batu? Istilah ini datang dari para guide untuk membedakan daerah yang lebih dihulu atau dihilir itu saja.nama sebenarnya suku Korowai adalah Kholufo artinya manusia dihulu, nama Korowai sendiri kabarnya adalah nama yang diberikan oleh para misionaris yang melakukan kontak pertama dengan mereka diawal tahun 70an

ada hal yang menarik, pada satu kali dalam acara bedah buku dimana saya mendapatkan kehormatan untuk mendampingi guru saya prof Rupert Stach  untuk menjadi narasumber pembanding, seorang peserta bercerita kalau di suku Asmat mereka menyebut orang Korowai dengan istilah Asmat Batu, karena mereka percaya dari Korowailah nenek moyang mereka yang pertama berasal untuk hal terakhir ini kamipun bersepakat bahwa masih harus dibuktikan secara ilmiah.

Ada Dusun ada Kampung

Pola pemukiman masyarakat Korowai sendiri memang unik

Mereka mengenal dua pola pemukiman, yang pertama adalah dusun

Disini masyarakat hidup terpencar-pencar,dan tidak hidup dalam satu komunal yang terdiri dari beberapa puluh rumah, satu keluarga akan hidup berjarak sekitar beberapa ratus meter sampai beberapa kilo jaraknya dari tetangga yang paling dekat. Hingga bisa dikatakan mereka orang yang sangat egaliter, sangat ingin kebebasan. Walaupun begitu mereka tidaklah egois dan individualis saling membantu dan menolong serta berbagi dalam hidup mereka adalah nilai-nilai yang utama, dalam kehidupan sehari-hari.

Satu keluarga akan hidup berpindah dari satu tempat ke tempat lain didalam wilayah ulayat marga mereka, kepindahan dikarenakan persediaan bahan makanan yang sudah menipis di sekitar mereka, karena rumah pohon rusak atau karena ada kematian.

Nah didusun, satu keluarga akan mengenal dan membangun 2 sampai 3 macam, yakni rumah utama (khaim), rumah haid dan rumah melahirkan juga ada rumah untuk orang sakit, tergantung kebutuhan. Rumah rumah ini tingginya berkisar antara ada yang sekitar 3 meter (untuk rumah melahirkan ) sedangkan rumah tinggal berkisar 1m-6m saja, lantai rumah dibuat dari kuli kayu atau kulit nibun (palem hutan) yang telah dibelah, dinding rumah dari pelepah daun sagu sedangkan atapnya dibuat dari anyaman daun sagu.

Sementara penggambaran yang dilihat di media memang sangat berlebihan…rumah tinggi yang sampai 14m atau 20m itu hanyalah rumah yang dibangun oleh anak-anak muda Korowai untuk bersantai dan melihat-lihat pemandangan dari ketinggian disiang hari saja atau hanya untuk pentingan turis dan film dan bukan berfungsi untuk rumah tinggal atau sebuah hunian

Sedangkan rumah pesta, akan dibangun bersama-sama keluarga besar (bisa terdiri dari beberapa marga) saat akan ada pesta ulat sagu .panjang rumah pesta (ngil/Gil) bisa mencapai 25-30M tergantung banyaknya marga yang akan diundang dalam pesta. Rumah pesta adalah satu-satunya rumah yang dibangun berlantaikan tanah.

Selain dusun mereka juga ada yang memiliki rumah-rumah di kampung, kampung-kampung seperti Yaniruma, Mbasman, Yafufla, Avimabul dan Danuwage adalah kampung-kampung yang dibuka oleh pemerintah Indonesia dan para Misionaris dari Gereja-Gereja Reformasi (GGRI) Belanda Dan GIDI (Gereja Injili Di Indonesia) .

Masyarakat Korowai senang tinggal juga di kampung karena di kampung ada sekolah, ada puskesmas atau pelayanan kesehatan dari gereja, dan atau jika rombongan turis tiba maka mereka bisa menghasilkan uang tambahan dari menjadi tenaga porter ataupun menjual barang-barang kerajinan seperti noken, sayek dan pipa rokok.

peta kampung di Korowai-Rupert Stach

Tapi kadang sulit juga bagi mereka hidup di kampung karena dikampung mereka tidak bisa menokok sagu dan berburu, sebab kampung bukanlah wilayah ulayat mereka. (perlu diingat bahwa, bagi orang Papua khususnya Korowai sangat pantang mencuri atau mengambil barang orang lain, menokok sagu yang bukan diwilayah adat mereka bisa dianggap sebagai pencurian yang bisa mendatangkan masalah dan denda)

Pola pemukimanan yang seperti inilah yang menyebabkan para tenaga medis agak susah menjangkau mereka yang sakit.

Saya sendiri punya pengalaman harus menempuh perjalanan 2 hari naik long boat menyusuri sungai, ditambah 7 hari jalan kaki barulah saya tiba di dusun keluarga Molonggantung yang menjadi lokasi penelitian saya.

Tanpa pemimpin

Suku Korowai memang sangat unik, walaupun belum banyak diteliti namun hasil pengamatan dan temuan saya dilapangan dan juga dibenarkan oleh Prof Rupert Stach seorang antropolog senior yang sangat ahli tentang suku Korowai membenarkan bahwa suku ini sebenarnya tidak mengenal system kepemimpinan tetap, seperti suku-suku lain di Papua.

Kepemimpinan yang mereka kenal hanyalah system kepemimpinan sementara, sang pemimpin ini dikenal dengan nama Milon-sang penjaga api suci di pesta ulat sagu. namun kepemimpinan milon sendiripun tidaklah sebuah kepemimpinan yang menjadikan milon sangat istimewa sosok istimewa dan dilayani.

Sebaliknya Milonlah yang jadi sosok yang melayani, memediasi dan mengkoordinir kerja-kerja dari mulai persiapan hingga pesta ulat sagu usai.

Hingga pada dasarnya suku Korowai sangat menjujung tinggi nilai-nilai kesetaraan tak peduli laki-laki atau perempuan, anak-anak atau orang tua, semua harus saling menghormati dan menghargai hak-hak masing-masing.

KOROWAI = KANIBAL ????

Jika orang Koroway adalah orang kanibal sampai saat ini maka saya dan prof. Rupert Stach tidak akan pernah keluar dari sana hidup-hidup, atau para turis yang jumlahnya puluhan bahkan ratusan tiap tahun itu tidak akan pernah bisa melenggang bebas dan menikmati pesta ulat sagu dan berinteraksi dengan masyarakat secara bebas. Nyatanya kami semua bisa dengan bebas keluar masuk Korowai.

Memang dulu sekali, diatas tahun 80an ada praktek membunuh dan memakan orang yang dituduh  memiliki ilmu hitam, dan itu dilakukan supaya si korban tidak bangkit lagi. Dan dalam pandangan orang Korowai manusia yang memiliki ilmu hitam (khahua-suanggi) bukanlah manusia lagi, dia sudah berubah menjadi seperti zombie jadi sah saja jika dihabisi. (menariknya ini sama dengan di film-film bukankah kita sah saja membunuh zombie).

Maka sungguh jahat situs-situs yang menyebutkan kalau orang Korowai melakukan praktek kanibal demi pemenuhan protein sehari-hari.

Diskriminasi Terhadap Perempuan?

Sungguh yang satu ini tidak saya liat dibudaya Korowai. Ini tertuang dalam thesis saya (Makna rumah dalam pandangan suku Korowai-Telaaah Gender dan Kepemimpinan). Wanita-wanita suku Korowai sangat berdaya dan sangat dihormati hak-haknya. Mulai dari hak memilih pasangan, hak reproduksi, pembagian kerja dalam rumah tangga sampai kepeda pola mengasuh anak. Maka sayapun belajar bahwa disuku yang kata banyak orang masih “primitive” ini justru isu kesetaraan gender telah lama dijalankan dalam system masyarakat

Telanjang

Saya kira agar tidak terjadi pengulangan silahkan membaca tulisan saya yang lain, yang sama buat untuk membela mereka dan semua suku pedalaman dari stigma

[Kami Tidak Telanjang ](http://dianyasmin.blogspot.com/2014/08/kami-tidak-telanjang-part1.html)

Saya sendiri berharap, bahwa stigma terhadap suku Korowai dan suku-suku dipedalaman Papua dan dimana saja bisa dihentikan. Saya percaya bahwa sekeras apapun sebuah kebudayaan, dia adalah hasil interaksi interaksi dengan alam dan lingkungan selama berabad-abad. Dan sekeras apapun itu dia telah berhasil menjaga masyarakat ini tetap exist.

Saya percaya bahwa budaya memang akan berubah, namun dia haruslah berjalan secara alami, dan harus berubah kearah yang baik. Sehingga tak akan menciptakan generasi yang shock culture dan gagap dengan budayanya sendiri

Semoga tulisan ini bermanfaat. Manoptofedo-manoptelobo
