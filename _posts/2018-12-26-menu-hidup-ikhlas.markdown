---
title: Menu Hidup Ikhlas
date: 2018-12-22T07:00:51.794Z
categories:
  - CME
  - Berita
  - 11 Ibu 11 Panggung 11 Kisah
tags:
  - teater
  - kisah perempuan
author: Kadek Sonia Piscayanti
comments: true
img: /uploads/blog/img_7495.jpg
---
Menu Hidup Ikhlas adalah judul pentas ke 11  dalam project 11 Ibu 11 Panggung 11 Kisah yang dipentaskan oleh aktor Prof. Dr. Putu Kerti Nitiasih. Prof. Titik, begitu dia dipanggil, menjadi koki dalam pentas ini. Beliau adalah koki kehidupan dalam pengertian sesungguhnya. Pekerjaan koki adalah menyiapkan menu, bahan, mengeksekusi bahan hingga menyajikan. Persis seperti itulah peran yang dijalani Prof. Titik. Di rumah, kampus, komunitas sosial, beliau adalah seorang koki. Perancang menu, pengeksekusi dan pengevaluasi sekaligus. Pentas ke 11 ini adalah sebuah rangkaian akhir dari perjalanan project 11 Ibu 11 Panggung 11 Kisah yang disutradarai Kadek Sonia Piscayanti.



Dipilihnya pentas ini menjadi pentas akhir tentu bukanlah tanpa pertimbangan. Prof. Titik akan menutup pentas ini dengan sebuah pesan bahwa segala rasa yang kita alami dalam kehidupan ini baik itu manis, pahit, asam dan asin harus disambut dengan seimbang. Janganlah terlalu sedih jika sedih, janganlah terlalu bahagia jika bahagia. Semua harus dijalani dengan seimbang dan natural. Ibarat sebuah masakan, terlalu pedas tidak baik, terlalu asin tidak baik, terlalu asam tentu juga tidak baik.



Dalam pentas ini Prof. Titik mengerahkan kemampuannya memasak, mulai dari menyiapkan bahan, memasak hingga menyajikan. Penonton pun mencicipi masakan beliau. Tak hanya memasak makanan, Prof. Titik juga menyiapkan menu lain yaitu loloh, dalam bahasa Indonesianya jamu, yang terbuat dari sirih, lengkuas juga air. Penonton juga mencicipinya, bahkan boleh nambah.



![](/uploads/blog/img_7495.jpg)



Sebagai adegan akhir, dipentaskan pula bagian ketika Prof. Titik berdoa dan memuja Tuhan dengan caranya. Begitu hening dan menghanyutkan. Juga ada bagian ketika ia memeluk foto keluarga yang dibanggakannya. Seolah semua perjuangan ini sudah selesai ketika semua dilakukan untuk keluarga. Ia menitikkan air mata. Juga semua yang merasakannya.



Pentas ke 11 ini diakui paling berat oleh Kadek Sonia Piscayanti sang sutradara sebab secara teknis, Prof Titik adalah aktor yang paling sibuk sehingga hampir tak ada jadwal latihan penuh. Latihan dilakukan di sela-sela jam kantor, jam makan siang, atau jam sore. Semua dilakukan dengan pertimbangan teknis, cermat dan teliti. Sebab sang aktor sangat sibuk.



Namun di luar kendala teknis, Sonia sangat berbahagia bahwa project sebelas ibu ini sudah berjalan lengkap dan lancar. Dia sangat mensyukuri bahwa project ini adalah kolaborasi yang berhasil di antara tim produksi, aktor dan semua penonton yang terlibat termasuk apresiasi dari wartawan, penulis dan pengamat.



Salah seorang penonton yaitu Agung Dirga menyambut baik pementasan 11 ibu ini sebab mengajarkan hal-hal yang tampak sederhana namun bermakna dalam. Misalnya petikan kalimat dalam pentas kemarin malam, "Pernahkah kau mengupas bawang? Pernahkah kau memotong cabai? Pernahkah kau mengunyah jahe?" Pertanyaan sederhana namun jika dianalisis lebih dalam bermakna filosofis bahwa semua proses dalam memasak adalah proses belajar dalam hidup. Ia juga mengajak penonton lebih menghargai Ibu. Pesan hampir sama juga disampaikan I Gede Budasi yaitu Wakil Dekan 3 Fakultas Bahasa dan Seni Universitas Pendidikan Ganesha. Budasi berkata bahwa pentas Prof. Titik mengajarkannya menjadi laki laki yang lebih baik.
