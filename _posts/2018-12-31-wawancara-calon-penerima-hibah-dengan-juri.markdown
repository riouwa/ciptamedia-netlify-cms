---
title: Wawancara Calon Penerima Hibah dengan Juri
date: 2018-04-16T06:26:13.218Z
categories:
  - CME
  - Berita
tags:
  - seleksi
  - hibah
author: Ivonne Kristiani
comments: true
img: /static/img/landing/akses.png
---
Pada 16-17 April 2018 diadakan pertemuan antara beberapa calon penerima hibah yang terpilih dengan para juri Cipta Media Ekspresi di Hotel All Seasons Jakarta. Pertemuan ini bertujuan agar para pendaftar dapat mempresentasikan langsung proyek yang diusungnya sehingga gambaran proyek yang akan dilaksanakan pun dapat dipahami lebih jelas oleh para juri. Sebelumnya, memang para calon penerima hibah ini sudah diminta untuk mengirimkan proposal yang lebih detail kepada panitia, namun dengan adanya pertemuan tatap muka maka para juri dapat berdialog dan bernegosiasi dengan calon penerima hibah mengenai aspek-aspek lain dari proposal mereka yang akan sulit dilakukan hanya dengan korespondensi tertulis. 

Undangan kepada calon penerima hibah untuk bertemu langsung dengan para juri tidak otomatis menandakan bahwa proposal mereka pasti diterima. Hasil dari pertemuan ini kemudian masih akan didiskusikan kembali oleh kedelapan juri. 

Secara total ada 42 orang perempuan yang diundang dalam pertemuan ini. Mereka pun datang dari wilayah yang beragam, ada yang dari Kupang, Waingapu, Jayapura, Pontianak, Bali, Medan, Bandung, Malang, Solo, Jogjakarta, serta Jakarta. Karena kendala jadwal dan keterbatasan untuk hadir, maka untuk beberapa peserta dilakukan wawancara via telepon ataupun Skype.
