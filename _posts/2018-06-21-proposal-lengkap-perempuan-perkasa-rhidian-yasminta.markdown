---
title: Proposal Lengkap - Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai - Rhidian Yasminta Wasaraka
date: 2018-06-21
permalink: /ciptamediaekspresi/proposal-lengkap/perempuan-perkasa
layout: proposal
author: Rhidian Yasminta Wasaraka
categories:
- laporan
- CME
- Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai
---

![0391.jpg](/static/img/hibahcme/0391.jpg){: .img-responsive .center-block }

# Rhidian Yasminta Wasaraka - Perempuan Perkasa Kesetaraan Gender Pada Budaya Suku Korowai

**Tentang penerima hibah**

RHIDIAN YASMINTA WASARAKA, atau biasa disapa Dian,adalah seorang perempuan Papua dari suku Baham dengan ibu dari suku Jawa, wanita kelahiran Manokwari, tigapuluh delapan tahun lalu ini adalah seorang Aktivis lingkungan, peneliti, photographer, blogger sekaligus dosen di Jayapura, namun lebih dari semua itu dia adalah seorang ibu dari seorang putra. Kecintaannya pada Papua sekaligus keprihatinannya terhadap banyaknya stigma yang dilekatkan pada orang Papua, khususnya pada suku Korowai mendorongnya untuk melakukan penelitian sekaligus publikasi sebesar mungkin, dengan harapan public bisa melihat dengan lebih jernih dan mendapatakan informasi yang benar tentang suku ini yang bernama asli Kholufo ini.

**Kontak**

  - Facebook: [facebook.com/dian.y.wasaraka](https://www.facebook.com/dian.y.wasaraka)

  - Blog: [dianyasmin.blogspot.com](http://dianyasmin.blogspot.com/)

  - Atau via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Penelitian ini berlokasi di wilayah adat Suku bangsa Korowai di Kabupaten Boven Digul, Provinsi Papua-Indonesia

**Deskripsi Proyek**

Proyek ini merupakan kegiatan penelitian tentang budaya Suku Bangsa Korowai,yang berfokus pada bagaimana suku ini memandang posisi perempuan dalam kebudayaan mereka, pada kenyataannya walaupun menganut budaya patriarki dalam system kekerabatan namun posisis wanita Korowaipun tak bisa dianggap sepele.

Prinsip equality dan egalitarian adalah hal yang sangat menonjol dalam system kebudayaan mereka, hingga walaupun rumah mereka berjauhan namun prinsip saling berbagi dan menghormati tetap menjadi hal yang utama

- **Tujuan**

  Mencari tahu bagaimana sebenarnya suku Korowai memandang dan memposisikan perempuan dalam kebudayaan serta praktek keseharian mereka.

- **Sasaran**

  Akademisi, praktisi dunia hiburan, pemerintah, dan para pihak yang berkepentingan berinteraksi, membangun dan berinvestasi di wilayah adat suku Korowai

- **Latar Belakang**

  1. Keterkaitan pada kategori: Riset / Kuratorial

     Korowai, adalah suku bangsa yang mendiami pedalaman Boven Diegul Papua, merupakan suku yang memiliki kebudayaan yang unik sebab kemampuan mereka membangun rumah-rumah diatas kanopi pohon yang tinggi dengan peralatan yang sangat sederhana sebuah kemampuan yang sudah hampir punah dan mungkin tinggal satu-satunya didunia. Namun keunikan budaya suku bangsa ini bukan hanya pada kemampuan mereka membangun rumah diatas pohon, namun juga pada kearifan budaya mereka. dari hasil penelitian sebelumnya terindikasi kuat bahwa meskipun menganut budaya patriarki dalam kehidupan berkelurga mereka, namun para pria suku Korowai sangat menghormati para wanitanya, terutama sosok ibu mertua dari garis istri/perempuan.  Rasa hormat ini dicerminkan dalam sopan-satun, tutur kata dan perilaku sehari-hari

     *Sebuah Urgensi*

     Meskipun sangat menghargai sosok perempuan namun pada kenyataanya pengaruh dari luar mengalir sangat deras menggerus budaya dan keyakinan mereka selama ini. kenyataan bahwa wilayah Korowai saat in tengah di kepung berbagai project besar seperti pembangunan jalan trans Papua, tambang emas hingga kelapa sawit, telah menjadikan perempuan dalam posisi terjepit dan terpinggirkan. Suara mereka tidak lagi didengar sebab para “pimpro” dan pengusaha nakal hanya mau enak saja, mereka selalu mengatasnamakan serta berkata “ kami sudah bertemu dan bicara dengan kepala suku”.  selain memang harus diakui bahwa tulisan dan kajian tentang suku Korowai memang masih sangat langka.

     Padahal  sejatinya di Korowai tidak terdapat hirarki kepemimpinan  sebab mereka menjujung nilai kesetaraaan dalam kehidupan mereka. Situasi harap gampang dan mau enaknya saja ini didisain karena ada pembenaran seolah-oleh karena sebagian besar budaya di Papua menganut budaya patriarki maka karena budaya itulah suara kaum perempuan ditiadakan. Situasi ini sangat mengkhawairkan karena selain akan menyebabkan bentrok antar sesama anggota klan, namun juga yang miris adalah hilangnya budaya suku Korowai dikarenakan hilangnya hutan dengan mudah yang selama ini menjadi rumah bagi mereka

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Dari uraian tersebut, maka dapat ditarik benang merah permasalahkan yakni: Bagaimanakah suku bangsa Korowai memandang posisi perempuan dalam kehidupan sehari-hari mereka? yang dikaji dalam tujuh unsur budaya.

  3. Strategi

     untuk memperoleh data maka maka strategi yang dilakukan adalah dengan kegiatan turun lapangan dan melihat secara langsung aktivitas kehidupan masyarakat Korowai di dusun mereka, yang lain adalah dengan mewawancarai narasumber kunci, orang-orang tua dari suku ini yang dianggap sangat memahami budaya mereka secara mendalam, peneliti juga akan berdiskusi dengan beberapa akademisi seperti Professor Rupert Stach dari Cambridge University, beliau adalah antropolog yang memahami budaya Korowai, lalu diskusi tentang politik dan kepemimpinan dengan Professor Tony Rudiansjah dari Universitas Indonesia dan diskusi tentang issue gender pada budaya Papua dengan Dr. Dumatubun dari Universitas Cenderawasih Papua.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Persiapan meyangkut pembelian alat-alat dan kelengkapan lain yang menunjang kegiatan penelitian, termasuk mengirim kabar melalui kurir pesan atau radio SSB kepada tuan dusun di Korowai

        2. Turun lapangan untuk pengambilan data

        3. Mengolah data

        4. Editing dan Cetak Buku

        5. Peluncuruan buku, pameran photo dan diskusi

        6. Laporan

  5. Latar belakang dan demografi pelaku proyek

     Peneliti adalah seorang perempuan Papua, berusian 37tahun dengan latar belakang pendidikan S1 Komunikasi dan S2 Antropologi Budaya

  6. Pemimpin proyek

     adalah seorang perempuan dengan pengalaman  selama lima belas tahun melakukan penelitian dan pendampingan terhadap suku Korowai, penelitian pertama yang dilakukan adalah pada tahun 2003 tentang “Kebajikan Budaya Suku Korowai”

  7. Demografi kelompok target

     Agak sulit memetakan dengan benar berapa jumlah penduduk suku Korowai sebab sebagian besar dari mereka masih hidup berpindah-pindah didalam wilayah ulayat mereka, namun beberapa sumber menyebutkan terdapat jumlah mereka sekitar 5000 jiwa,

  8. Hasil yang diharapkan

     Dari penelitian ini diharapkan didapatnya sejumlah data berupa tulisan dan sejumlah photo yang nantinya akan di cetak dan dipublikasikan dalam bentuk buku. Dalam acara peluncuran buku ini akan dilakukan dua kegiatan yakni pameran photo dan diskusi di tiga tempat yakni

        1. Pameran photo dan diskusi di Geleri ANTARA Jakarta

        2. Pameran photo dan diskusi di Museum Lokabudaya Universitas Cenderawasih

        3. Pameran Photo dan diskusi di Campus Institut Seni dan Budaya Indonesia (ISBI) Tanah Papua

  9. Indikator keberhasilan

        1. Terselenggaranya kegiatan penelitian dilapangan

        2. Terdapat Buku tentang Budaya Suku Korowai

        3. Terselenggaranya publikasi berupa pameran photo dan diskusi di tiga tempat (Galeri Antara, Museum Loka Budaya Uncen dan kampus ISBI tanah Papua)

  10. Durasi Waktu Aktivitas dilaksanakan

      9 bulan

  11. Total kebutuhan dana

      150 juta rupiah

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      150 juta rupiah

  13. Sumber dana lainnya

      Tidak ada

  14. Kontribusi perorangan

      Tidak ada

  15. Kontribusi kelompok target

      Tidak ada
