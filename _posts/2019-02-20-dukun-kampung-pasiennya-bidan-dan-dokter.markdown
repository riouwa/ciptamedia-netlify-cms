---
title: 'Dukun Kampung, Pasiennya Bidan dan Dokter'
date: 2019-02-07T10:28:08.995Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - Sumba
  - Dukun beranak
  - proses melahirkan
author: Martha Hebi
comments: true
img: /uploads/blog/dukun-kampung.jpg
---
![](/uploads/blog/dukun-kampung.jpg)



Dia dikenal sebagai dukun beranak. Ribuan anak lahir dengan selamat dari tangan dinginnya sejak 1969. Masa-masa sulit untuk mendapatkan bantuan medis dalam proses kelahiran di Pulau Sumba. Hampir setiap hari ada saja pasiennya. Ibu rumah tangga, polisi, tentara, PNS, pengusaha, petani bahkan ada bidan dan dokter pula. Namanya Debora Maramba Hi (83 tahun), tapi dia lebih dikenal dengan nama Nyora Pindu. Di Sumba, sebutan Nyora diperuntukkan bagi perempuan yang suaminya guru. Suami Nyora Pindu adalah seorang guru.



Karir Nyora Pindu sebagai dukun pijat dimulai pada tahun 1964 dan ini hanya terbatas untuk kalangan keluarga dan tetangga dekat. Lima tahun kemudian, Nyora Pindu menolong proses kelahiran seorang ibu dan berhasil. Bayi dan ibu selamat. Sejak saat itu, Nyora Pindu lebih dikenal sebagai dukun beranak.

 

“Dari tahun 1969, belum pernah ada bayi atau ibu yang melahirkan meninggal dunia saat saya tolong. Semuanya selamat,” tutur Nyora Pindu mengisap rokok daun lontarnya.



Kamar berukuran 3x4 meter merupakan ruangn kerjanya yang selalu ramai dikunjungi pasien. Saat raganya masih kuat, Nyora Pindu diundang menolong kelahiran ke rumah pasiennya.  Pernah, dalam sehari dia membantu 3 proses kelahiran. 



Ada yang menarik dari pengalamannya, seorang ibu di rumah sakit terkenal di Waingapu dinyatakan dokter bahwa bayinya telah meninggal dalam kandungan sehingga harus menjalani operasi caesar. Sang ibu telah masuk ke dalam ruang untuk menunggu giliran operasi. Diam-diam keluarga ibu tersebut mengundang Nyora Pindu ke rumah sakit. Nyora Pindu datang berbincang dengan sang ibu sambil mengelus perutnya. Lima belas menit kemudian, sang ibu melahirkan tanpa perlu menjalani operasi caesar. Seperti diagnosa dokter, bayinya telah meninggal dunia.



Di kesempatan berbeda, seorang bidan mengakui bahwa proses kelahiran anaknya pernah dibantu oleh Nyora Pindu. Dokter spesialis kandungan terkenal di Waingapu, dr. Ketut Ananda W, Sp.OG mengiyakan bahwa Nyora Pindu pernah menolong istrinya. Seperti apa pertolongan yang diberikan Nyora Pindu kepada istri Dokter Ketut? Tunggu kelanjutannya dalam Buku Perempuan (Tidak) Biasa di Sumba, Era 1965-1998.
