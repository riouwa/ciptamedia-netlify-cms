---
title: Penutupan Pendaftaran Hibah Cipta Media Ekspresi
date: 2018-03-25T05:12:04.103Z
categories:
  - CME
  - berita
tags:
  - ''
author: Ivonne Kristiani
comments: true
img: /uploads/blog/screenshot-2018-12-31-at-12.21.24.png
---
Setelah diluncurkan pada 8 Januari 2018 dan dibuka selama kurang lebih dua bulan, pendaftaran hibah untuk perempuan, Cipta Media Ekspresi, resmi ditutup pada 25 Maret 2018. Menjelang detik-detik penutupan pendaftaran, formulir yang masuk secara daring (_online_) melonjak tajam. Apabila pada sebulan sebelumnya, aplikasi yang masuk berjumlah sekitar 250, maka pada saat penutupan aplikasi yang diterima mencapai 1168 proposal yang berasal dari 34 propinsi di Indonesia. Tidak ada propinsi yang warga perempuannya tidak ikut mengirimkan proposal untuk menerima hibah ini. Proposal yang masuk pun sangat beragam. Hal ini menandakan banyaknya seniman perempuan di Indonesia walaupun selama ini masih kurang representasi.

![](/uploads/blog/screenshot-2018-12-31-at-12.21.24.png)
