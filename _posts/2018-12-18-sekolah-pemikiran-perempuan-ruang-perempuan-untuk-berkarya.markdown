---
title: 'Sekolah Pemikiran Perempuan : Ruang Perempuan untuk Berkarya  '
date: 2018-12-18T12:50:57.785Z
categories:
  - Esai Refleksi
tags:
  - '#SPP #CME'
author: Rena A. Asyari
comments: true
img: /uploads/blog/marlina.jpg
---
Marlina, tatapannya mengisyaratkan banyak pertanyaan. Kakinya adalah saksi perjalanan panjang yang telah ia tempuh untuk mencari keadilan. Ia haus akan jawaban mengapa perempuan selalu dipaksa membasahi dahaga gairah laki-laki. Adegan-adegan yang dipertontonkan Marlina membuat kami sepanjang hampir 90 menit menahan sesak di dada. Kami kerap menahan riak di ujung mata yang kian terasa panas dan ingin tergelincir. Beberapa kali hampir kami tahan agar butiran air mata tak jadi jatuh. Kami para perempuan yang menonton Marlina, seolah berada di sana dan ikut menanggung duka yang dirasakannya.

_Scene_ terakhir, tangisan bayi, dan pelukan dua orang perempuan setidaknya membuat kami lega bahwa film nampaknya akan segera usai. Kali ini tidak seperti yang sudah-sudah, ketika kerap kali saya datang di acara nobar dan diskusi di beberapa tempat. Menonton Marlina mungkin akan menjadi salah satu yang akan saya kenang. Bukan tentang filmnya, tetapi para pemantik diskusi yang membuat forum menjadi hidup. 

Kehadiran kritikus film, aktor, akademisi dan penulis novel, komisioner Komnas Perempuan, kurator, desaigner, pekerja kreatif, dalang, santriwati, fotografer, seniman, penulis sastra, jurnalis, dan arsitek, membawa suasana diskusi menjadi kaya akan pertanyaan dan jawaban. Pembelajaran tentang Keywords yang dibawakan Intan Paramaditha hari sebelumnya membuat kami ‘berlomba’ memberikan ragam keywords untuk film Marlina. 

Kontradiksi, karakter, koboi, dosa, perspektif, ruang aman, intim, mitos, simbol, invasi, individu, self, gairah, support, persahabatan, game. Kata-kata tersebut membuat riuh dan gemuruh suasana kelas pemikiran perempuan. Yang tabu dibahas hingga ke akarnya, yang tradisi diperbincangkan dan dicari hingga ke celahnya. Yang mitos diperdebatkan. Visual, struktur film, estetika film, konsep patriarki, pemikiran dan perasaan perempuan menjadi bahan yang tak habis dibicarakan selama hampir 3 jam. Hanya di kelas SPP CME saya mendapatkan itu semua.

Jauh sebelum menonton Marlina, saya hanyalah perempuan yang tergerak mengikuti hibah dana Cipta Media Kreasi di bulan Maret lalu. Keberuntungan memihak pada saya. Saya mendapat kesempatan mengikuti kelas Sekolah Pemikiran Perempuan yang digelar online dari bulan Juni-September dan ditutup dengan kelas offline di bulan Oktober.

Deg-degan dan panik selalu menghantui setiap kali mendekati akhir bulan. Mengikuti kelas dengan materi dan kajian yang sama sekali baru, membuat saya jauh-jauh hari sudah harus bersiap merangkai kata/latihan untuk menjawab pertanyaan para mentor di kelas online. Hasilnya? Tetap saja jauh dari harapan, saya kebanyakan bengong dan tak sigap bicara. Kadang saya mengira, mungkin beberapa guru tidak sadar jika menarasikan argumen ataupun informasinya dengan bahasa sosial yang cukup rumit (setidaknya ini menurut saya), sehingga sulit untuk dipahami. Tetapi saya sadar sepenuhnya bahwa ini adalah proses pembelajaran. 

Kerap kali saya capai dengan tugas-tugas membaca dan mereview, terkadang bertanya untuk apa semua ini? Kenapa tidak langsung saja diberikan intinya, dikasih jalan, kok seperti dibuat rumit. Beruntung teman saya menyadarkan, ia berkata “inilah namanya sekolah, kamu akan diberikan macam-macam ilmu, hingga sampai pada saat, kamu akan memilih, mengkombinasikan dan mengerucutkan ilmu yang kamu pelajari, kalau ingin langsung dikasih solusi itu namanya kursus”!. 

Ada kurang lebih 22 artikel dan buku baru yang saya baca dari kelas SPP. Dari mulai belajar dan menganalisa gender dengan buku Mansyur Faqih, Kamla Bashin, dan buku saku Gender Surabaya. Mengupas feminisme dengan bell hooks. Mempelajari metode-metode feminis dalam penelitian sosial dengan Schulamit. Dari Linda Nochlin saya menjadi tahu mengapa tidak ada seniwati yang hebat. Mempertanyakan kembali tentang Gender apakah masih menjadi kategori analisis yang bermanfaat, dari artikel Joan. WS.

Tidak hanya itu, saya masih juga diasupi materi dari Melani Budianta, Dolorasa, Heidi Arbucle dan artikel yang sangat menarik dari Titaruby. Mempelajari juga tentang sejarah pergerakan perempuan, dan proyek nasional dan relasi gender. 

Jujur saja, semua bahan pembelajaran tersebut adalah baru bagi saya, tidak pernah saya dapatkan sebelumnya. Meskipun kelas SPP CME sangatlah singkat, tetapi rasanya saya mendapatkan banyak. Tak hanya sampai di situ, kelas offline sebagai penutup kelas SPP CME dibuat sangat menarik, klub cerap yang menyediakan ruang diskusi, membuat saya terus berpikir untuk bertanya dan mempertanyakan ulang yang teman-teman saya sampaikan, menolak ataupun menerimanya, rasanya lalu lintas otak saya sibuk selama kelas offline tersebut. Ruang publikasi karya dan metode penelitian feminis yang kaya akan materi, bukan saja memberikan pengetahuan baru tetapi juga pengalaman dan keinginan untuk selalu mengenangnya.

SPP CME bukan hanya menyuguhkan materi, tetapi mengajarkan keberanian berekspresi, menawarkan cara berpikir yang kritis, jeli dan peka, tidak menjadi perempuan yang patuh, tunduk, yang hanya sebatas mengikuti, tanpa memahami. Mengajak untuk mengkaji ulang, mempertanyakan kembali kesepakatan-kesepakatan yang telah mengakar di masyarakat. 

Rasanya tak ada yang mendamba perpisahan. Berakhirnya kelas SPP CME semoga menjadi awal untuk menyelenggarakan kembali kelas-kelas berikutnya. Saya pikir masih banyak perempuan di Indonesia yang susah mendapatkan akses pengetahuan tentang tubuh, pemikiran dan segala hal tentang perempuan. Semoga panitia, juri, dan mentor diberikan kesehatan serta keluangan waktu agar bisa berbagi pemikiran dan pengetahuan kepada perempuan-perempuan di tanah air. 

Mengenang guru dan mentor di SPP CME, saya jadi teringat dengan para ibu bangsa. Lasminingrat, Kartini, Dewi Sartika, R.A Sutartinah, Sri Sulandari, Siti Sukaptinah. Era boleh berganti, tetapi semangat untuk memperjuangkan perempuan, harus tetap hadir dan membiak. 

Terima kasih banyak yang tak terhingga untuk semua waktu, pengetahuan dan inspirasi yang telah dibagikan. Hormat saya setinggi-tingginya untuk para mentor.
