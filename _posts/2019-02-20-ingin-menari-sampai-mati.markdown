---
title: Ingin Menari Sampai Mati
date: 2019-02-05T10:56:18.139Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - Sumba
  - perempuan
  - menari
author: Martha Hebi
comments: true
img: /uploads/blog/ingin-menari-sampai-mati.jpg
---
![](/uploads/blog/ingin-menari-sampai-mati.jpg)



Masih ingatkah dengan masa kecil Anda? Ya, saat Anda duduk di bangku Sekolah Dasar. Kalau berbuat salah atau terlambat tiba di sekolah biasanya ada hukuman. Kalau di Sumba, ada yang kena cubit di pantat, kena "habok" pakai mistar kayu atau sapu lidi, disuruh berlutut, bersihkan halaman, bersihkan kamar mandi dan masih banyak ragam lainnya.



Alangkah menyenangkan menjadi murid Mama Martha D. Ghabi. Saat beliau menjadi kepala sekolah, para siswa yang terlambat datang ke sekolah, akan dimintai menunggu di luar kelas dan membuat barisan. Mama Martha akan memberi hukuman pada mereka. Dan hukumannya adalah menari bersama. Mama Martha bersama murid-muridnya akan menari diiringi bunyi gong tambur yang berasal dari alunan suara Mama Martha. Sepuluh sampai lima belas menit kemudian hukuman akan berakhir.



Bagi yang menghabiskan masa kecilnya di Desa Hombakaripit, Kodi Utara, Sumba Barat Daya sejak tahun 1970 hingga saat ini, tentu mengenal Mama Martha atau malah menjadi murid tarinya. Bersama suaminya, Bapak Goris Gh. Kaka yang juga seorang seniman, Mama Martha menciptakan tarian-tarian Kodi atau memodifikasinya. Bapak Goris mencipta lagu yang mengiringi tarian Mama Martha. Keempat anak mereka, Wilhelmus Ng. Pala, Heribertus Ra Mone, Patricia Kaka, Oktavianus Kaka akan menjadi tim pertama yang mencicipi karya mereka. 



Hingga saat ini di usianya yang ke-72, Mama Martha masih terus menari. Bersama 5 penari lainnya yang berusia di atas 60 tahun, yang juga adalah murid tarinya selalu mempersembahkan tarian di Gereja Katolik St. Maria Hombakaripit. "Saya ingin menari sampai mati," kata Mama Martha.



Selamat ulang tahun, Sang Penari.
