---
title: Menggugat Diskriminasi Negara Terhadap Kepercayaan Marapu
date: 2019-02-14T06:28:15.889Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - CME
  - Perempuan
  - Sumba
  - Agama
  - Tradisi
author: Martha Hebi
comments: true
img: /uploads/blog/menggugat-diskriminasi-negara-terhadap-kepercayaan-marapu.jpg
---
![](/uploads/blog/menggugat-diskriminasi-negara-terhadap-kepercayaan-marapu.jpg)



Bila sebuah negara melahirkan kebijakan tanpa mempertimbangkan keragaman warganya, maka akan berpotensi menjadi benih diskriminasi berkepanjangan. Bahkan tidak jarang akan menimbulkan konflik antar warganya. Sudah menjadi hukum alam bahwa kelompok atau komunitas minoritas akan kalah atau dipaksa kalah.



Situasi ini terjadi di Indonesia sejak lahirnya Penetapan Presiden Republik Indonesia No.  Nomor 1 tahun 1965 yang kemudian menjadi UU No. 5 tahun 1969 tentang Pencegahan Penyalahgunaan dan/atau Penodaan Agama dimana Negara mengatur agama-agama yang dianut penduduk Indonesia adalah Islam, Kristen (Protestan), Katolik, Hindu, Budha dan Konghucu. Beberapa tahun kemudian, saat kuku Orde Baru mulai mencengkeram negeri ini, Konghucu dipinggirkan melalui Surat Keputusan Menteri Dalam Negeri tahun 1974 dimana kolom agama dalam Kartu Tanda Penduduk (KTP) harus diisi dengan pilihan agama Islam, Kristen, Katolik, Hindu dan Budha.



Kebijakan di atas kemudian diikuti lagi dengan kebijakan lainnya yang bersinggungan dengan urusan agama warga dimana dampaknya melahirkan gesekan antar warga dan munculnya kelompok intoleran. Hingar-bingar Negara dan penganut agama yang diakui resmi juga  antar penganut agama membuat banyak yang lupa bahwa jauh sebelum agama-agama “impor” datang ke Indonesia, telah ada begitu banyak aliran kepercayaan di Indonesia. 



Pada tahun 2017 Kementerian Pendidikan dan Kebudayaan mencatat ada 187 organisasi penghayat yang tersebar di seluruh Indonesia. Di antaranya Marapu di Pulau Sumba. Marapu adalah agama asli dan tertua di Sumba. Kemudian datanglah para misionaris yang mewartakan agama Katolik dan Protestan. Pekabaran agama Protestan di Sumba dirintis oleh Zending GKN (Gereformeerde Kerken in Nederland) mulai tahun 1881. Agama Katolik dperkenalkan oleh imam misionaris Jesuit pada tahun 1889. 



Di kemudian hari, Marapu, sang tuan rumah ini malah digusur dan didiskriminasikan, salah satu alasannya adalah sebagai dampak dari kebijakan Negara. 



Mama Lidda Mau Mudde (76) dari Kampung Tarung, Waikabubak, Sumba Barat adalah salah satu perempuan yang merasakan pahitnya menjadi penganut Marapu di Sumba. Marapu kerap dianggap kafir atau diidentikkan penyembah berhala. Sindirian dan ungkapan langsung tentang identitasnya sebagai seorang penganut Marapu menghiasi hidupnya sehari-hari. 



Mama Lidda menuturkan dirinya pernah mendatangi sekolah dasar anaknya untuk melakukan klarifikasi atas ungkapan guru anaknya yang meyatakan bahwa yang boleh sekolah adalah anak-anak Kristen. Ini terjadi karena masa itu Mama Lidda tidak mencantumkan anak-anaknya adalah penganut Kristen atau Katolik. Ya karena mereka adalah penganut Marapu. Mama Lidda juga pernah marah pada pihak sekokah karena anaknya dipukul. Dan anaknya dipukul karena menganut Marapu. Mama Lidda melakukan advokasi untuk keluarganya yang setia menganut Marapu berhadapan dengan kebijakan Negara yang mewajibkan warganya mencantumkan 1 dari 5 ajaran agama dalam identitas mereka.



Hal lain yang diceritakan Mama Lidda, sebagai penganut Marapu, dirinya dan penduduk Kampung Tarung menjalankan ritual Wulla Poddu yang merupakan bulan suci penganut Marapu. Ada sejumlah larangan yang wajib dipatuhi oleh umat Marapu antara lain menjaga suasana hening dan sakral, tidak boleh melakukan hubungan seks antar suami istri, tidak boleh pesta, tidak boleh ada adat perkawinan, tidak boleh terdengar bunyi gong dan tambur. Di sekitar Kampung Tarung ada masjid, gereja Katolik, gereja Protestan, gereja Bethel. Mama Lidda dan umat Marapu tidak pernah melakukan komplain atas bunyi lonceng gereja yang berdentang di hari minggu atau di jam-jam tertentu setiap hari, begitu juga dengan kumandang azan lima kali sehari. 



“Kami tidak pernah protes karena mereka sedang beribadah. Sama seperti kami juga sedang poddu. Semua larangan yang disampaikan Rato, untuk orang Marapu saja. Jadi kami yang tidak boleh ribut, harus tenang. Sedangkan orang Kristen dan orang Islam tidak kena aturan,” tutur Mama Lida.



Bagaimana cara Mama Lidda menghadapi diskriminasi dari “adik-adiknya”penganut agama “impor” ini? Bagaimana Mama Lidda menggugat Negara yang telah melakukan kekerasan terhadapnya?



Ikuti pula apa kata Pendeta Dr. Andreas A. Yewangoe dari kacamata Protestan dan P. Robert Ramone, CSsR dari kacamata Katolik terhadap situasi diskriminatif yang dialami umat Marapu.



Selengkapnya ada dalam Buku Perempuan (Tidak) Biasa di Sumba Era 1965-1998.
