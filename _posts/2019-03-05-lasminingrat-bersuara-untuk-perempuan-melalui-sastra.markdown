---
title: 'Lasminingrat, Bersuara Untuk Perempuan Melalui Sastra'
date: 2019-01-23T11:59:50.806Z
categories:
  - CME
  - Kajian
  - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
tags:
  - sunda
  - garut
  - Lasminingrat
  - intelektual
  - sastra
author: Rena Amalika Asyari
comments: true
img: /static/img/landing/akses.png
---
Suasana pagi di bawah kaki Gunung Papandayan sangat sejuk. Sesekali angin bertiup semilir memainkan anak rambut. Jalan aspal yang berkelok-kelok menambah pemandangan eksotis setiap mata yang melihatnya. Dari perut bumi di tatar Priangan ini, hampir satu setengah abad yang lalu, lahir karya sastra dari tangan perempuan.            

Lasminingrat, namanya belakangan sudah ramai diperbincangkan di ruang maya. Perlahan dan pasti masyarakat Sunda terlebih generasi muda yang mempunyai akses terbuka pada media digital akan lebih mengenalnya. Di media nasional, majalah Historia Edisi 1 tahun 2012 turut mewartakan Lasminingrat dalam kolomnya “Tapak Perempuan Nusantara” sebagai salah satu perempuan yang membuka jalan bagi gerakan perempuan, bersanding dengan 49 tokoh perempuan lainnya.

Lasminingrat saat itu mungkin tak pernah membayangkan, namanya akan banyak dibincangkan di forum-forum seminar akademisi, atau bahkan obrolan sederhana warung kopi di daerah Garut. Pusaranya di Garut, kian sering didatangi anak-anak sekolah, pengunjung biasa maupun peneliti. Pemikirannya dibedah oleh manusia abad 21.

Semula, ia tak berharap menjadi besar. Seperti pengakuannya yang ia tuliskan di bukunya, ia hanya menulis dan mencatat. Ia tidak pernah menunda menulis dan tidak pernah menunda membaca. Kegemarannya menulis bukan karena ia sudah pintar hingga tahu semua jawaban, justru karena ia tidak mengetahui banyak hal, maka ia menulis. Ia menulis hanya karena ia takut lupa, menulis menurutnya bisa melupakan kegelisahan dan kebimbangan hati. Ia sangat senang belajar, menurutnya pula segala kemampuan dan ilmu kita jika tidak diasah bisa mudah lupa bahkan hilang.

Karya-karya sadurannya masyhur saat itu. Peneliti sunda Mikihiro Moriyama menyebutkan bahwa karya Lasminingrat menjadi bacaan wajib anak pribumi saat itu, Tjarita Erman yang terbit tahun 1875 di Batavia, saduran dari karya Hendrick Van Eichenfels bahkan dicetak hampir 6000 eksemplar, dan saat itu dijual dengan harga f 0,4.

<br>

**Suara Perempuan dalam Sastra Lasminingrat**

Selain didaulat sebagai penulis dongeng anak-anak karena popularnya Tjarita Erman saat itu, Lasminingrat juga menulis dongeng untuk kalangan remaja dan dewasa. Karyanya yang berjudul Warnasari Jilid 2 meunang njalin tina boekoe walanda diterbitkan di Batavia tahun 1887. Jika Tjarita Erman bentuknya seperti novel dan berisi banyak pengajaran untuk anak-anak usia SD, maka Warnasari Jilid 2 yang terdiri dari 5 cerita pendek ini lebih membukakan ruang-ruang dialektika baru bagi pembaca remaja dan dewasa.

Cerita-cerita dalam Warnasari lebih hidup. Menyuguhkan persoalan yang kompleks. Ambisi kekuasaan, keberanian, tekad, percintaan, perjodohan mewarnai kisah-kisah yang ada di dalamnya. Salah satu kisah yang menarik adalah yang berjudul “Sang Radja Poetri jeung Saderekna Doewa Welas” yang banyak membincang tentang perempuan. Tentang tubuh, tabiat, stigma, intimidasi dan diskriminasi yang kerap diterima oleh perempuan.

Kisah bermula dari seorang perempuan yang terus menerus mengandung sebanyak dua belas kali. Suaminya, tak mau tahu tentang tubuh perempuan yang tak ada habisnya digantungi kehidupan manusia baru. Perempuan seperti tak punya kuasa atas tubuhnya. Suaminya tak pernah puas, ia hanya ingin anak perempuan. Dengan segala rasa sakit yang ditanggungnya, perempuan seharusnya diapresiasi dan ditinggikan. Sayangnya, peristiwa melahirkan seolah hanya menjadi tanggung jawab perempuan.

Beruntung anak yang ketiga belas adalah perempuan. Tetapi, menjadi petaka bagi semua kakak-kakaknya yang laki-laki. Ayah mereka tak lagi peduli. Sang ayah pun membenci dan hendak membunuh kedua belas anak lelakinya. Tentu saja ini tidak adil, bukankah rasa kasih sayang, cinta dan kemanusiaan tak mengenal jenis kelamin? seperti yang disinggung oleh bell hooks dalam bukunya Feminism is for Everybody, bahwa setiap manusia berhak mendapatkan keadilan, entah itu laki-laki, perempuan, anak-anak maupun orang dewasa.

Dalam tulisannya tersirat Lasminingrat menegaskan bentuk ketidakadilan tersebut. Ia menggugat dan berkali-kali dengan keras tak sepakat dengan intimidasi. “…semoga hati kamu tidak begitu, saya benar-benar tidak terima, karena semuanya juga anak, dan betapa sayangnya, semuanya sudah besar dan berprilaku baik, dan lagi tak terbayang betapa menderitanya saya, membunuh anak yang sudah besar dan tidak punya dosa”. (Sang Radja Poetri jeung Saderekna Doewa Welas, hal. 47).

Perempuan yang mengandung semua putranya itu ternyata tak punya kuasa sedikitpun atas daging yang pernah bercokol di tubuhnya. Keputusan suaminya tak bisa diubah. Kebencian suaminya pada darah dagingnya tak bisa ia bantah. Ia, perempuan yang menyerahkan seluruh tubuh dan kehidupannya selama mengandung, menyusui dan membesarkan kedua belas anaknya ternyata sama sekali tak punya hak untuk bersuara.

Kedua belas anak lelaki itu akhirnya kabur agar tidak dibunuh sang ayah. Ibunya, perempuan yang mengasihinya hanya bisa menangis, tak dapat berbuat banyak. Tangisnya membuat suaminya jengah, di matanya perempuan itu sangat cengeng. Tak sedikitpun kepedulian suaminya atas kedukaan yang tengah dirasakan istrinya.

Lasminingrat dalam cerita saduran “Sang Radja Poetri jeung Saderekna Doewa Welas” juga menyisipkan peran perempuan yang hanya dibatasi dalam ruang domestik saja. Anak perempuan yang ketiga belas itu menyusul kakaknya. Ketika kakak-kakaknya yang dua belas orang dan semuanya laki-laki bekerja di luar rumah, adiknya yang perempuan hanya disuruh untuk memasak dan menunggui rumah, membersihkan perabotan hingga rumah tampak lebih bersih dan menyenangkan karena ada sentuhan tangan perempuan. Begitu juga di akhir cerita, ketika semua laki-laki terlibat dalam peperangan, para perempuan hanya boleh menunggu di dalam keraton saja.

Ketika Lasminingrat menuliskan kisah ini, hak perempuan untuk mendapatkan keadilan dan perlakuan yang setara dengan laki-laki sangat terbatas. Perempuan hanya boleh menempati ranah domestik.   Satu hal lagi yang sangat menarik, Lasminingrat menceritakan tentang pilunya menjadi seorang perempuan. Intimidasi dan diskriminasi kerap diterimanya, yang lebih menyedihkan yang menghukum adalah juga seorang perempuan.

Diceritakan anak perempuan ketiga belas tadi sudah dewasa dan hendak dipersunting seorang Raja, tetapi para perempuan kerajaan saling membisiki ratu, memengaruhinya agar tak menerima calon menantu pilihan anaknya hanya karena asal-usulnya tidak jelas.

Pada abad 19, sudah menjadi keharusan kaum menak harus menikah dengan kaum menak pula, maka tidak heran jika kelanggengan kekuasaan bisa bertahan berpuluh-puluh generasi berikutnya. Menjadi anak seorang HoofdPenghoeloe Penghulu Limbangan, menjadikan Lasminingrat bagian dari lingkaran menak, lingkaran itu terus dipelihara dan diperbesar melalui pernikahan. Pernikahan pertamanya dengan Raden Tamtu Somadiningrat anak Bupati Sumedang kemudian menikah kedua kalinya dan menjadi istri kedua Raden Wiratanudatar VIII Bupati Limbangan.

Tetapi, dalam cerita fiksinya Lasminingrat seolah menghapus tradisi pernikahan kaum menak. Sang Raja tetap memilih perempuan yang ia ingin nikahi meskipun asal usulnya tidak jelas.   Lasminingrat juga dengan sengaja menampar keras para perempuan yang hobinya bergosip, yang lebih banyak bicara daripada bekerja. Lasmi tak segan-segan menyebutnya “lidah ular”, menurutnya mulia dan tidaknya seorang perempuan bukan berasal dari status sosial, tetapi dari akal dan keluhuran budi.  Ia menulis : “… yang bisa bicara serta bicaranya benar itu perak, yang diam dan tidak cerewet itu emas, sedangkan yang bisa bicara tapi menjelek-jelekkan orang lain itu namanya lidah ular”. (Sang Raja Putri, hal. 66).

Ketidaksukaannya pada perempuan yang suka menggosip ia tuliskan juga pada kisah “Tjarita Oray Bodas”. “…Malahan kepada istri yang sangat disayanginya juga tidak pernah diberitahukan. Menurut Radja istrinya tidak bisa menyimpan rahasia, “biwir nyiru rombengeun, ceuli lentaheun\[1]” tidak lama berita diterima langsung saja diceritakan pada sahabatnya. Katanya menitip cerita dengan syarat tidak boleh diceritakan lagi, tetapi akhirnya berita itu diteruskan kepada siapa saja hingga akhirnya menjadi rahasia bersama-sama, malahan di Negara tersebut ada istilah: rahasia perempuan, rahasia bersama-sama”.

Lasminingrat, ia seorang perempuan tetapi ia mempunyai daya kritis yang tinggi terhadap kaumnya. Bukan semata-mata untuk mengolok-olok perempuan, tetapi memberi pandangan bahwa masih banyak perempuan yang mempunyai sifat buruk yang akhirnya stigma negatif pun tak pelak dialamatkan kepada semua perempuan sebagai penggosip. Meskipun sebetulnya dengan menyebut perempuan dengan istilah demikian berarti ada stigma negatif perempuan sebagai penggosip dari masyarakat patriarki yang juga telah terinternalisasi pada dirinya.

Menjadi perempuan yang hidup di abad 19 tidaklah mudah. Lasminingrat yang terlahir sebagai anak hoofd penghulu Raden H.M. Moesa tetap saja mengalami banyak kesulitan. Ibunya harus rela dipoligami, bersanding dengan enam perempuan lainnya. Tentu saja Lasminingrat akrab dengan kehidupan dan cara hidup perempuan di sekelilingnya. Cerita “Sang Radja Putri” menjadi refleksi kehidupan tentang betapa jauhnya rentang keadilan bagi perempuan dan laki-laki saat itu. Pembatasan ruang ekspresi bagi perempuan dan laki-laki disuarakannya berkali-kali.

Apa maksud Lasminingrat memilih menyadur cerita “Sang Radja Poetri jeung Saderekna Doewa Welas” dan “Tjarita Oray Bodas” yang banyak menarasikan tentang perempuan? Mungkin saja secara samar-samar Lasminingrat ingin menanamkan pemahaman kepada setiap  perempuan agar dapat membela dirinya, membela tubuhnya. Lasminingrat juga ingin agar setiap perempuan dapat bersikap adil dan mendukung kaumnya, bukan malah menjatuhkan dengan alasan apapun. Lasminingrat yang kala itu sadar benar bahwa buku-bukunya  menjadi bacaan wajib anak sekolah seolah sengaja menyebarkan kesetaraan dan keadilan bagi setiap manusia, laki-laki dan perempuan sejak dini.  

<br>

**Catatan Akhir**

\[1] Istilah peribahasa dalam bahasa sunda yang artinya segala macam diceritakan tidak bisa menyimpan rahasia, suka terburu-buru menceritakan kepada orang lain, tanpa konfirmasi dulu kebenarannya.

<br>

__

**Daftar Pustaka**

Historia, “50 Tapak Perempuan Nusantara”,  Majalah Historia, No. 1 Tahun 1, 2012, hh. 45.

Moriyama, Mikihiro. 2013. Semangat Baru, Komunitas Bambu, Jakarta.

Effendie, Deddy, 2011. Raden Ajoe Lasminingrat 1843-1948 Perempuan Intelektual Pertama di Indonesia, Studio Proklamasi, Garut.

Lasminingrat, R. A., 1887. Warnasari jilid 2 meunang tjalin tina boekoe walanda, Landsdrukkerij, Batavia.

Lasminingrat, R. A., 1875, Tjarita Erman, Landsdrukkerij, Batavia.

Lasminingrat, R.A., 1876, Warnasari atawa Roepa-Roepa Dongeng di salin tina basa walanda kana basa sunda, Landsdrukkerij, Batavia.

hooks, bell, 2000. Feminism is For Everybody, South End, Cambridge.

<br>

\*_Artikel ini pertama kali terbit di_ [Jurnal Perempuan](https://www.jurnalperempuan.org/wacana-feminis/lasminingrat-bersuara-untuk-perempuan-melalui-sastra) dan _dipublikasikan ulang dengan izin penulis dan penerbit._
