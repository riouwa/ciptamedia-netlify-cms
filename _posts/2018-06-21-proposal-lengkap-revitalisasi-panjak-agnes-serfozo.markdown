---
title: Proposal Lengkap - Revitalisasi Akses Panjak Perempuan Dalam Seni Seblang - Agnes Serfozo
date: 2018-06-21
permalink: /ciptamediaekspresi/proposal-lengkap/revitalisasi-panjak
layout: proposal
author: Agnes Serfozo
categories:
- laporan
- CME
- Revitalisasi Akses Panjak Perempuan Dalam Seni Seblang
---

![0150.jpg](/static/img/hibahcme/0150.jpg){: .img-responsive .center-block }

# Agnes Serfozo - Revitalisasi Akses Panjak Perempuan Dalam Seni Seblang

**Tentang penerima hibah**

Agnes Serfozo, lahir di Budapest, Hongaria, tanggal 22 November tahun 1980. Setelah SMA melanjutkan studi di ELTE-BTK Budapest di jurusan sastra dan antropologi budaya. Tahun 2004 menerima beasiswa bernama Darmasiswa dari Kementerian Pendidikan RI, sehingga dapat kesempatan belajar pedalangan di STSI Surakarta selama setahun. Setelah studinya mendalami pedalangan gaya pokok Surakarta, memutuskan mendalami pengetahuannya dan skillnya di bidang sindhenan (vokal putri Jawa) dengan cara belajar ke pesindhen-pesindhen senior. Sedangkan studi S1 di ELTE-BTK lulus di tahun 2007 dengan titel Sarjana Sastra Hongaria. Sejak tahun 2009 aktif sebagai pesindhen panggung untuk mengisi acara klenengan maupun wayangan. Telah pentas kolaborasi bersama dalang-dalang di beraneka daerah pulau Jawa sehingga dapat pengalaman dan skill dalam sindhenan gaya Solo, Semarang, Yogyakarta, Banyumas, Jawa Timuran maupun vokal khas Banyuwangi. Sering terlibat dalam pentas wayang dalang-dalang seperti Ki Manteb Soedharsono, Ki Enthus Susmono, Ki Purbo Asmoro, Ki Anom Suroto. Tahun 2015 melanjutkan studi akademik di prodi pascasarjana UNS Surakarta di program kajian budaya (cultural studies). Objek formal yang diangkat dalam penelitiannya adalah sebuah ritual kelompok etnis Osing yang disebut Seblang, di desa Bakungan, kecamatan Glagah, kabupaten Banyuwangi.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Desa Bakungan merupakan sebuah kelurahan di sebelah barat perbatasan kota Banyuwangi, yang secara administratif masuk ke kecamatan Glagah, kabupaten Banyuwangi. Masyarakat Bakungan setiap tahun mengadakan seni ritual Seblang dalam rangka upacara bersih desa, yang secara adat dilaksanakan sehabis Idul Adha di hari kamis atau minggu berikutnya. Bakungan memiliki 4 lingkup yaitu Krajan, Watulo, Karangasem, Gaplek. Tempat pelaksanaan Seblang secara tradisional adalah di sanggar adat desa yang berada di wilayah Krajan, Bakungan.

**Deskripsi Proyek**

Revitalisasi akses panjak perempuan dalam Seni Seblang adalah sebuah proyek yang berupaya memberi kesempatan untuk penabuh-penabuh (panjak) perempuan berpartisipasi lagi dalam seni Seblang. Pada awalnya panjak dalam seni Seblang adalah ibu-ibu desa Bakungan yang memiliki skill bermain gamelan, serta memiliki hafalan gendhing-gendhing repertoire Seblang. Namun, sekitar lima tahun lalu perubahan secara tidak alami, yaitu panjak yang semula putri diganti dengan panjak laki-laki, dengan alasan biaya tata rias dan kostum panjak perempuan sangat membebani anggaran pelaksanaan Seblang, yang sumbernya adalah iuran masyarakat setempat. Alasan lainnya adalah, bahwa di hari pelaksanaan Seblang para perempuan desa Bakungan sibuk mempersiapkan masakan yang diperlukan oleh ritual bersih desa, sehingga banyak laki-laki setempat beranggapan bahwa bila panjak perempuan sehabis masak-masak harus rias dan perform, itu sangat tidak mungkin dan merepotkan. Akhirnya para panjak perempuan tidak bisa mengikuti seni Seblang sebagai penabuh, dan kegiatan karawitan mereka tidak dilanjutkan dalam bentuk apapun. Namun, para panjak perempuan sebenarnya ingin sekali tampil dalam seni Seblang sebagai penabuh, karena ini satu-satunya eksresi seni yang mereka pernah menjalankan. Hasil kajian menunjukkan, bahwa skill karawitan mereka lebih bagus ditimbang skill panjak laki-laki, dan para panjak perempuan memiliki hafalan gendhing yang lebih banyak. Dalam prakteknya, gendhing yang disajikan dalam seni Seblang merupakan fragmen dari gendhing aslinya yang lebih panjang secara struktural maupun durasi. Revitalisasi akses panjak perempuan dalam seni Seblang adalah sebuah upaya memberi akses untuk berkesenian kepada mereka yang selama ini terpinggirkan, tetapi juga memiliki visi mendokumentasikan gendhing-gendhing Seblang sebagaimana itu tersimpan dalam memori panjak perempuan.

- **Tujuan**

  Tujuan proyek Revitalisasi panjak perempuan dalam seni Seblang adalah menggerakkan kembali panjak perempuan  supaya dapat berpartisipasi dalam pementasan.

- **Sasaran**

  1. Masyarakat Bakungan di Krajan yang mengadakan seni ritual Seblang dalam rangka upacara bersih desa

  2. Perempuan-perempuan Bakungan yang memiliki pengalaman dan skill dalam bidang karawitan khususnya sajian gendhing-gendhing Seblang

- **Latar Belakang**

  1. Keterkaitan pada kategori: Akses

     Perempuan-perempuan Bakungan yang memiliki pengalaman dan skill dalam bidang karawitan khususnya sajian gendhing-gendhing Seblang, saat ini tidak memiliki akses untuk berkesenian sesuai kemampuan dan keinginan mereka. Sehingga mereka butuh akses untuk mencapai keinginan mereka untuk bisa bergabung lagi dalam seni Seblang.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Seblang merupakan seni ritual upacara bersih desa yang mengekspresikan konsep kepercayaan penduduk sehingga dianggap sakral. Namun akibat perkembangan jaman terjadi banyak pergeseran dalam kebiasaan pelaksanaan Seblang sehingga sakralitasnya semakin dapat dipertanyakan. Pada asalnya Seblang mencerminkan worldview masyarakat petani: mitos kesuburan yang sangat melekat dengan entitas perempuan, di mana bumi pertiwi dipercayai sebagai ibu, dan kesuburan menjelma dalam wujud Dewi Sri. Namun, kepercayaan ini telah keropos keutuhannya karena perubahan ekologi (berkurangnya lahan pertanian akibat pembangunan), perubahan budaya (semakin heterogennya penduduk Bakungan secara etnisitas, kedudukan sosial maupun latar belakang budaya) serta Karen perubahan gaya hidup (dari masyarakat petani menjadi masyarakat pinggir kota). Salah satu perubahan dalam tata laksana Seblang adalah, pemain gamelan yang semulanya perempuan semua, diganti panjak laki-laki dengan alasan biaya. Akhirnya, perempuan-perempuan setempat tidak memiliki akses untuk berkesenian sesuai pengalaman dan kemampuan yang milikinya.

     Perubahan tersebut diatur oleh senter yang memiliki prestige-power-privilage. Kepentingan senter mengabaikan peran perempuan dalam Seblang secara konkrit (kehadiran fisik sebagai panjak) maupun kehadirannya sebagai entitas feminism yang secara immanen lebih dekat dengan kekuatan alam khususnya kesuburan pertumbuhan flora. Perubahan pergantian panjak perempuan ke panjak laki-laki juga berdampak ke keberlangsungan kelestarian tradisi gendhing-gendhing Seblang. Perempuan Bakungan sebagai perempuan bergay hidupa tradisional, berperan sebagai pengasuh utama dalam edukasi informal generasi-generasi muda. Dalam praktek kesehariannya perempuan melalui nyanyian, dogeng dan dolanan bersama anak-anak, memiliki peran kunci dalam proses mewariskan dan menanam citarasa estetika gendhing lokal ke mereka.

     Dengan revitalisasi akses panjak perempuan, perempuan desa Bakungan mendapatkan kesempatan berlatih gamelan untuk mempersiapkan pentas Seblang tahun 2018. Aktivitas latihan maupun pementasan akan akan berdampak positif ke permasalahan yang sudah dipaparkan di atas.

  3. Strategi

     Pergantian panjak perempuan ke panjak laki-laki merupakan peristiwa yang cukup sensitif di kehidupan dan memori kolektif warga-warga Bakungan. Perubahan tersebut dilakukan oleh ketua adat zaman itu secara mendadak, sehingga pergeseran ini merubahkan perintah dari yang dianggap posisinya lebih senter, dan tidak dapat dinegosiasi. Panjak perempuan telah merasa tersinggung, kemampuannya tidak dipakai lagi, sedangkan mereka ingin sekali tampil lagi. Revitalisasi akses panjak perempuan membutuhkan strategi mapan bagaimana bisa memotifasi mereka yang telah terpinggirkan untuk mau berpartisipasi lagi. Kunci atau penggerak utama dalam strategi dan pendekatan tersebut adalah ketua adat sekarang, dia selama menjadi ketua adat cukup bijaksana dalam menjaga kepentingan warga masyarakat Bakungan, khususnya dalam pelaksanaan seni ritual Seblang. Kemudian, perempuan-perempuan Bakungan memiliki jaringan informal sesuai standar kehidupan masyarakat desa adat, di mana jiwa gotong royong dan kebersamaan masih cukup kuat. Ketiga, AMAN (Aliensi Masyarakat Adat Nusantara) memiliki anggota pemuda-pemuda di desa Bakungan, mereka adalah generasi penyambung yang cukup responsive terhadap isu-isu terkait seni Seblang maupun adat setempat, sehingga mereka bisa mensosialisasikan proyek revitalisasi di kalangan pemuda, supaya mereka mendapatkan pemahaman terhadap pentingnya peran panjak perempuan dalam seni Seblang.

  4. Aktivitas dan keterkaitan pada sasaran

     Pertama, tahap sosialisasi, untuk memberitahu maksud dan tujuan proyek ini sehingga semua pihak yang terlibat bisa mendukung kelancarannya dan kesuksesannya. Tahap sosialisasi juga menjangkau proses mengumpulkan kembali panjak perempuan yang sudah telah berpengalaman sehingga memiliki skill, namun juga tidak menutup kemungkinan untuk membuka pintu kesempatan selebar-lebarnya di depan perempuan-perempuan, yang ingin bergabung bermain gamelan dalam rangka seni Seblang, meskipun belum memiliki pengalaman. Panjak perempuan desa Bakungan pada umumnya berusia 50-60 tahun, dan belum ada regenerasi. Dengan memberi kesempatan kaum usia muda dan menengah untuk bergabung, akan mempersiapkan regenerasi panjak perempuan di desa Bakungan.

     Tahap kedua merupakan tahap latihan perempuan panjak untuk memapankan kemampuannya yang telah lama tidak digunakan, supaya sesuai taraf yang diharapkan, yaitu bisa gabung dalam pementasan seni Seblang 2018. Latihan akan dilaksanakan selama 4 bulan dua kali seminggu, dipimpin oleh pelatih yaitu pengendang Seblang. Selain latihan karawitan, tahap kedua juga mencakupi persiapan piranti-piranti yang akan dibutuhkan saat pementasan, seperti kostum, alat tata rias dan tempat penyimpanan kostum dsb.

     Tahap ketiga adalah pementasan dan pendokumentasian pementasan, serta dokumentasi suara dan pembuatan transkrip. Tahap ketiga ini cenderung keterkaitannya dengam pelestarian gending-gending Seblang sebagaimana disimpan di memori panjak perempuan. Warisan lisan ini perlu didokumentasikan maupun ditranskrip ke nilai not karawitan, biar bisa menjadi sumber bagi yang mau mempelajari gendhing-gendhing yang disajikan dalam seni Seblang.

  5. Latar belakang dan demografi pelaku proyek

     Pelaku proyek adalah perempuan dengan pengalaman di bidang karawitan sebagai pesindhen, sekaligus peneliti di ranah ilmu kajian budaya. Latar belakang ganda (sebagai seniman dan sekaligus peneliti) akan mempermudah dalam pelaksanaan proyek, yang dari satu sisi membutuhkan pengertian holistik dan detil terhadap keutuhan estetika dan konsep seni Seblang, sedangkan dari sisi lain butuh pemahaman dari pandangan ilmu kritis-sosial terhadap fenomena perubahan-perubahan yang telah terjadi dalam tata pelaksanaan seni Seblang.

  6. Demografi kelompok target

     Panjak perempuan yang dulu pernah gabung dalam seni Seblang, adalah golongan swasta usia 50-60 tahun, kebanyakan dari keluarga masyarakat petani dengan jenjang pendidikan minim (Sekolah Dasar atau Sekolah Rakyat), yang tidak memiliki pekerjaan tetap (srabutan), namun memiliki profesi samping sebagai tukang pijet, dukun, dukun bayi, pemilik warung sederhana dsb. Harapannya, melalui sosialisasi dapat melibatkan perempuan dari kalangan generasi lebih muda.

  7. Hasil yang diharapkan

     Revitalisasi akses panjak perempuan dalam seni Seblang dapat meningkatkan kesadaran terhadap peran perempuan sebagai penjaga kearifan lokal mengenai keseimbangan ekologi yang tercermin dalam gendhing-gendhing Seblang. Kehadiran panjak perempuan dalam seni Seblang akan membuat seni ritual ini lebih utuh secara konseptual dan secera estetika, di mana perempuan dianggap sebagai entitas yang memiliki hubungan erat dengan kesuburan, alam floral dan hasil panen.

     Indikator sukses proyek ini adalah bila kaum perempuan desa Bakungan terdorong dan termotivasi memulai lagi latihan karawitan yang sejak lima tahun terakhir tidak berlanjut. Harapannya, karena sebagian perempuan sudah memiliki pengalaman sebagai panjak dalam seni Seblang, akan mencapai tingkat kecukupan dalam sajian gendhing-gendhing Seblang dalam waktu dekat. Indeks kesuksesan yang diharapkan adalah bila sebagian dari mereka dapat ikut serta dalam pementasan Seblang berikutnya tanggal 26 Agustus 2018.
     
  8. Indikator keberhasilan

     Keterlibatan dan keikutsertaan panjak perempuan dalam seni Seblang tahun 2018, serta materi gendhing Seblang dalam bentuk rekaman suara maupun transkrip notasi adalah dua indikator utama keberhasilan proyek.

  9. Durasi Waktu Aktivitas dilaksanakan

     Proyek Revitalisasi panjak perempuan dalam seni Seblang akan terlaksana dalam durasi waktu 5 bulan, Antara bulan Mei 2018 sampai bulan September 2018.

  10. Total kebutuhan dana

      Kurang lebih 50 jt rupiah

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      50 juta rupiah

  12. Sumber dana lainnya

      Tidak ada

  13. Kontribusi perorangan

      Tidak ada

  14. Kontribusi kelompok target

      Meluangkan waktu untuk (1) ikut rapat (2) turut latihan (3) berpartisipasi pada upacara Seblang 2018 (4) ikut memberikan saran generasi muda yang potensial untuk diajak berseni
