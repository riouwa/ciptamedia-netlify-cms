---
title: Revolusi Tini Wahyuni di Panggung 11 Ibu
date: 2018-11-18T03:50:46.574Z
categories:
  - CME
  - Berita
  - 11 Ibu 11 Panggung 11 Kisah
tags:
  - teater
  - kisah perempuan
  - ''
author: Kadek Sonia Piscayanti
comments: true
img: /uploads/blog/img_5832.jpg
---
Pentas 11 Ibu 11 Panggung 11 Kisah memasuki pentas ke-9 dengan aktor Tini Wahyuni dan kisah Kinanti Cahaya Hidupku. Pentasnya digelar pada tanggal 17 November 2018 di rumah Tini Wahyuni Jalan Ngurah Rai Nomor 25 Singaraja. Pentas ini adalah sebuah refleksi perjalanan seorang Tini Wahyuni sebagai perempuan, sebagai ibu, dan sebagai manusia yang berevolusi dari waktu ke waktu. Seperti dikatakan sutradara pada awal pentas bahwa Project 11 ibu 11 panggung 11 kisah adalah project mendengar. Ibu ibu yang selama ini menjadi pusat kekuatan keluarga, yang selalu melindungi keluarga, yang selalu berbuat sebaik mungkin untuk keluarga, punya sesuatu untuk dibagi. Hal ini tidak mudah karena demikian sibuknya seorang ibu kadang ia lupa mendengarkan dirinya sendiri atau mendengar orang lain. Ia sibuk dengan kebutuhannya menjadi pusat kekuatan keluarga. Ia sibuk mengurusi teknis teknis tetek bengek kehidupan sehingga lupa bahwa ada sesuatu yang berharga dari dirinya yang patut didengar, patut dibagi. Project ini adalah project yang memungkinkan 11 ibu untuk mendengar dan didengar. Targetnya adalah membuat publik (meski terbatas) mendengar pula dengan jernih.



 



Bagi Tini Wahyuni, alasannya berbagi melalui project ini, ada dua. “Pertama tujuan yang ditawarkan project teater ini. Sebab tujuannya adalah untuk mendengar, berbagi, dan mengijinkan saya menjadi diri sendiri, maka saya tertarik. Kedua, karena project teater bagi saya adalah sebuah terapi, bagi diri saya dan bagi orang lain yang mengalami kisah mirip seperti saya. Dengan mendengar cerita saya, barangkali orang dapat mengambil pelajaran bermakna tentang hidup, perceraian dan kematian.”



 

![](/uploads/blog/img_5832.jpg)



Seorang Tini Wahyuni kini berada di titik kulminasi dari seluruh kehidupannya. Perceraiannya dua kali sangat banyak memberinya waktu belajar. Sempat kehilangan hak asuh Kinanti anak tunggalnya sempat membuatnya limbung. Dia juga meninggalkan dunia dokter yang digelutinya. Ia kini menjadi seniman pelukis, penulis dan penyendiri. Namun Ibu Tini menyadari bahwa kini ia memang lebih nyaman sendiri. Perceraiannya yang kedua mengajarkannya bahwa perceraian adalah sebuah cara mempersiapkan kematian yang damai. Teater adalah salah satu upaya terapi untuk mendengar dan didengar. Disini kemudian ada faktor edukasi bahwa aktor adalah pembawa pesan cerita, nilai nilai dan catatan catatan penting untuk dilihat kembali, diacu kembali, atau direfleksi, dan bahkan didebat kembali. Sebab bukan semata cerita itu inti persoalannya namun nilai nilai di balik cerita itu. Tini Wahyuni adalah seorang yang unik dengan kepribadian INTJ yaitu Introversion, Intuition, Thinking and Judgment. Secara umum orang dengan kepribadian ini bersifat penyendiri, suka berpikir sendiri dan menganalisa sendiri dan memutuskan sendiri bahkan menghakimi sendiri. Ia sadar dengan kelemahan kepribadian ini yang cenderung membuatnya tertutup dan sinis pada dunia baru. Namun di project ini, Tini Wahyuni berevolusi menjadi pribadi yang damai dengan bantuan proses yoga dan meditasi yang dijalaninya. Ia bisa berbagi kisah dan bahkan menganggap bahwa berbagi kisah adalah sebuah terapi. Ia berevolusi menjadi manusia yang lebih baik.



Pentas kemarin menjadi hujan air mata bagi penonton yang mengetahui kisah Tini Wahyuni. Anaknya Kinanti Praditha terlihat terharu dan memeluk Mamanya. Beberapa temannya dari alumni SMAN 1 Singaraja angkatan 1983 juga memberi kesaksian betapa tangguhnya Tini Wahyuni. Bahkan menangis memeluk Tini Wahyuni. Suasana haru tak dapat dibendung.



Naomi Srikandi, seorang seniman teater sekaligus project ini mengatakan bahwa awalnya gagasan yang dibawa sutradara project ini Kadek Sonia Piscayanti ini adalah gagasan yang terkesan ambisius, membawa 11 ibu yang bukan aktor dalam 11 panggung teater berbeda. Namun Naomi sejak awal sangat mendukung gagasan Sonia bahwa teater adalah ruang alternatif saling mendengar di antara perempuan, ruang aman dan nyaman untuk berbagi. Bagi Naomi, Sonia sudah mampu membawakan gagasan itu tidak hanya sebagai gagasan di kepala namun sebagai ruang nyata yang terjadi dalam proses pembuatan teater ini. Dia pribadi sangat mendukung gagasan ini dan berharap teater dengan pendekatan documenter ini menjadi kegiatan berkelanjutan.
