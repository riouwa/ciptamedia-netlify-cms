---
title: 'Lasminingrat Lawan Perjodohan di Tatar Sunda Lewat Sastra '
date: 2019-02-08T09:45:30.064Z
categories:
  - CME
  - Kajian
  - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
tags:
  - garut
  - sunda
  - Lasminingrat
author: Rena Amalika Asyari
comments: true
img: /static/img/landing/akses.png
---
Pembaca sastra klasik Indonesia mengenal tema perjodohan terutama dari buku Siti Nurbaya karya Marah Rusli, yang diterbitkan Balai Pustaka pada tahun 1922. Cerita Siti Nurbaya  menjadi refleksi tentang kehidupan perempuan Indonesia yang mengalami perjodohan.

Sekitar 40 tahun kemudian, Pramoedya Ananta Toer melahirkan Gadis Pantai, kisah perkawinan paksa di mana orang tua dengan sukarela menyerahkan anak perempuannya kepada laki-laki yang sudah mapan, dengan harapan kehidupan keluarganya akan membaik.

Novel-novel lain yang menentang perjodohan dan poligami dan memperjuangkan hak-hak perempuan antara lain Azab dan Sengsara (Merari Siregar, 1920), Karam dalam Gelombang (Kedjora, 1920), Salah Asuhan (Abdul Muis, 1928), Asmara Djaja (Adinegoro, 1928), Karena Mertua (Nur Sutan Iskandar, 1953), Memutuskan Pertalian (Tulis Sutan Sati, 1978), Kehilangan Mestika (Hamidah, 1935), Layar Terkembang (Sutan Takdir Alisjahbana, 1937), dan yang tak kalah populer adalah Manusia Bebas karya Soewarsih Djojopuspito (1975).

Namun tak banyak yang tahu, hampir 35 tahun sebelum Siti Nurbaya hadir dan memantik kegelisahan pemikiran perempuan, dari Garut, Jawa Barat, muncul sebuah karya berjudul Warnasari Jilid 2 (1887). Buku yang ditulis oleh Raden Ayu Lasminingrat ini berupa kumpulan cerpen saduran dongeng-dongeng Eropa, diterbitkan di Batavia oleh penerbit Landsdrukkerij.

Meskipun Warnasari adalah cerita saduran, tetapi Lasminingrat berhasil mengganti cerita bernuansa Eropa dengan nuansa lokal yang khas tanah Priangan. Dari sekian banyak buku yang dibacanya, ia banyak memilih cerita dengan tokoh utama perempuan dengan segala konfliknya.  

Salah satu cerita sadurannya yang berjudul Tjarita Oraj Bodas (Cerita Ular Putih), berkisah seorang putri raja yang enggan untuk menikah. Lamaran pangeran-pangeran dari negeri seberang ditampiknya. Ayah dan ibunya pun bingung, dan hati mereka terus-terusan gundah karena buat mereka pernikahan adalah maha penting. Pernikahan merupakan sebuah simbol eksistensi dan reputasi keluarga. Apa jadinya jika rakyatnya mengolok-olok sang raja hanya karena putri semata wayang tidak mau menikah?

“…Saya menolak semua yang melamar karena belum ada yang cocok. Bukannya saya tidak mau mengikuti, bukan juga karena saya tidak sayang pada orang tua, justru karena saya sangat sayang; jika saya menikah dengan yang tidak saya cintai itu mudah saja, hanya akhirnya siapa yang akan menanggung kalau bukan diri saya sendiri…”

Cerita ini seolah menjadi perpanjangan tangan Lasminingrat dalam mengungkapkan perasaannya. Bahwa perempuan harus paham tentang konsekuensi dari perjodohan. Setiap perempuan harus mengenal pasangannya, karena yang akan mengarungi biduk rumah tangga adalah ia sendiri, bukan orang lain.

Pesan lain yang hendak dititipkan Lasminingrat untuk seluruh perempuan pribumi saat itu melalui tokoh putri tersirat dalam kalimat, “…dan itu pula yang saya pikirkan, orang tua sudah tua, sekarang saya dihargai karena saya masih punya orang tua, nanti jika orang tua saya sudah meninggal, tidak akan ada yang mengutamakan saya, tidak akan ada yang memberi hormat, apalagi jika saya menikah secara sembrono, tidak memilih pasangan yang sayang pada saya atau kepada orang tua saya, bagaimana orang tua tidak khawatir meninggalkan saya?...”.     

Lasminingrat sebagai perempuan sadar benar kehormatan, keistimewaan, dan peruntungan yang didapatnya karena ia adalah anak seorang ulama besar di Limbangan, Garut. Ia menganjurkan perempuan untuk selektif. Perempuan juga berhak menentukan pada siapa hatinya harus berlabuh, tidak boleh asal menerima laki-laki saja.

“…Dan bagaimana tidak enaknya melihat atau mendengar,  lumrahnya lelaki hanya sayang suka dan cinta pada awalnya saja, jarang-jarang yang selamanya, saya sekedar ikhtiar sebelum datang takdir, siapa tahu ada lelaki yang hatinya tetap baik , jika hanya sekedar cinta saja semua yang melamar saya pasti cinta, tetapi saya tidak cinta dengan tingkah lakunya, itu alasan kenapa saya menolak, bukannya tidak mengikuti, bukan tidak patuh pada orang tua, bukan juga menghindar dari takdir tidak mau menikah.” (Tjarita Oraj Bodas, hlm 115, 116)

Sang putri raja pun akhirnya mengadakan sayembara untuk memilih pasangan. Ia ingin menikah tetapi dengan yang terbaik. Meskipun ia tidak mengenal calonnya, setidaknya dengan sayembara ia sudah berhasil menyeleksi lelaki yang ingin menikahinya.   

Ada nada perlawanan di sini. Lasminingrat seolah mengajak bicara kaumnya tentang cinta yang sukar sekali menemukan jalannya. Berbeda dengan laki-laki, perempuan tidak mempunyai banyak kesempatan untuk memilih. Perempuan dianggap tidak tahu cara menemukan cinta. Akhirnya orang tua sering kali memilihkan jalan pintas, perjodohan. Sayembara, uji nyali dan kekuatan, bukan untuk mendapatkan pasangan yang dicintai, hanya sebatas menyeleksi.

Perjodohan kerap terjadi pada kalangan bangsawan atau di tanah Sunda disebut menak. Sejauh yang saya telusuri, belum diketahui apakah Lasminingrat juga mengalami perjodohan seperti yang ia siratkan dalam bukunya.

Lasminingrat menikah dua kali. Pernikahan pertama adalah dengan Raden Tamtoe Somadiningrat, anak Bupati Sumedang, Pangeran Soegih. Setelah suami pertamanya meninggal, Lasminingrat menikah kembali dengan R.A.A Wiratanoedatar VIII, Bupati Limbangan terakhir yang kemudian menjadi Bupati Garut pertama.

Pernikahan para bangsawan kerap kali bersifat politis. Biasanya orang tua akan menjodohkan anak perempuannya dengan koleganya, sesama bupati untuk merebut atau melanggengkan kekuasaan. Suka cita orang tua pun diluapkan dengan pesta pernikahan yang meriah. Eddi S. Ekadjati dalam bukunya yang berjudul Empat Sastrawan Sunda Lama menyinggung sedikit pesta pernikahan kaum menak R. Martanagara dan R. Ajeng Sangkaningrat yang digelar sangat mewah, pesta dan kesenian digelar berhari-hari.

Senada dengan di dunia nyata, dalam cerita fiksinya, Lasminingrat menggambarkan pesta pernikahan kaum menak yang digelar penuh suka cita “Tujuh hari tujuh malam, tidak berhenti bersuka ria. Siang malam beramai-ramai Radja terlalu bahagia putrinya sudah menikah. Sekarang sudah dikabul semua keinginan Radja”. (Tjarita Oraj Bodas, hlm.132)

Pernikahan bagi perempuan Jawa dan Sunda saat itu bukan hanya merupakan peristiwa sakral tetapi sebuah kewajiban. Tulisan pedas Kartini pada sahabatnya Stella meneguhkan tentang beratnya menjadi perempuan kala itu. “Saya ingin bebas agar saya boleh, dapat berdiri sendiri, tidak perlu bergantung kepada orang lain, agar….agar tidak harus kawin. Tetapi kami harus kawin, harus, harus. Tidak kawin adalah dosa yang paling besar bagi seorang perempuan Islam, cela yang paling besar yang ditanggung seorang gadis Bumi-putera dan keluarganya. (Kartini:1899)”.

Saya jadi membayangkan, andai saja Lasminingrat hidup pada zaman yang sudah terbuka, mungkin gugatan-gugatannya tersebut akan diucapkan dengan lugas, bukan hanya melalui cerita saduran.

Namun Melihat bahasa dan istilah-istilah kelokalan yang kental, rasa-rasanya seperti bukan membaca karya saduran. Peneliti sastra Sunda dari Jepang, Mikihiro Moriyama menyebutkan, Lasminingrat adalah penulis yang mula-mula yang menggunakan kata ganti orang pertama atau “koela” (saya) dalam tulisannya.

Hampir 150 tahun telah berlalu sesudah Lasminingrat menulis karyanya. Para penggerak emansipasi boleh berbangga, hari ini kita bisa menikmati hasilnya. Perjodohan sudah semakin jarang, terlebih di kota besar. Jikalau masih terdengar kasus tentang perjodohan, seiring berjalannya waktu, meskipun harus diakui kemajuannya sangat lambat, perlahan-lahan, semua perempuan dapat memilih untuk menambatkan hatinya.

<br>

\*_Artikel ini pertama kali terbit di_ [_Magdalene.co_](https://magdalene.co/story/lasminingrat-lawan-perjodohan-di-tatar-sunda-lewat-sastra) _dan dipublikasikan ulang dengan izin penulis dan penerbit._
