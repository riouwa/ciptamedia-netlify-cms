---
title: Proposal Lengkap - Film Dokumenter Kekerasan Perempuan di Dunia Kerja - Luviana Ariyanti
date: 2018-06-21
permalink: /ciptamediaekspresi/proposal-lengkap/stop-kekerasan
layout: proposal
author: Luviana Ariyanti
categories:
- laporan
- CME
- Film Dokumenter Kekerasan Perempuan di Dunia Kerja
---

![0642.jpg](/static/img/hibahcme/0642.jpg){: .img-responsive .center-block }

# Luviana Ariyanti - Film Dokumenter Kekerasan Perempuan di Dunia Kerja

**Tentang penerima hibah**

Luviana, adalah jurnalis/ penulis dan aktivis  perempuan dan peburuhan. Ia pernah bekerja sebagai jurnalis di radio, media cetak, televisi dan media online selama kurang lebih 20 tahun. Selama kurang lebih 20 tahun, Luviana juga aktif dalam memperjuangkan nasib para perempuan pekerja media di organisasi Aliansi Jurnalis Independen (AJI), di Serikat Pekerja Media dan Industri Kreatif untuk Demokrasi  (SINDIKASI), di Komite Aksi Perempuan  (KAP) dan Pokja Buruh Perempuan, jaringan yang terdiri dari berbagai organisasi buruh dan organisasi perempuan yang memperjuangkan nasib para buruh perempuan di Indonesia. Hingga saat ini, Luviana terlibat dalam advokasi kebijakan perburuhan dan kebijakan media, menjadi pengajar media di universitas dan pengajar literasi media di sekolah buruh. Ia memimpin website perempuan www.Konde.co

**Kontak**

  - Facebook: [facebook.com/luviana.luviana.35](https://www.facebook.com/luviana.luviana.35)

  - Instagram: [@luviana_nusantara](https://www.instagram.com/luviana_nusantara/)

**Lokasi**

Proyek mengambil lokasi di Jakarta, Bogor, Depok, Tangerang dan Bekasi (Jabodetabek)

**Deskripsi Proyek**

Dunia media dan pekerja industri kreatif saat ini dipadati oleh para perempuan pekerja muda. Mereka bekerja sebagai reporter, produser, web designer, pembuat film, webmaster, blogger, periset, designer grafis, social media campaign, artis dan seniman. Banyak dari mereka yang bekerja secara lepas. Namun di dunia kerja ini ternyata syarat oleh adanya stereotyping sekaligus pelecehan seksual dan diskriminasi yang menimpa para pekerja perempuan.

Sejak dalam perekrutan kerja, pekerja media di TV harus memenuhi syarat perekrutan: menarik/ cantik dan kurus. Jika tidak menarik, maka mimpi untuk bekerja di TV akan sia-sia. Syarat perekrutan ini selalu ada dalam kriteria penerimaan pekerja.  

Kumalasari, seorang artis, harus bekerja keras untuk menguruskan badan agar mendapatkan peran utama di televisi di Indonesia. Sedangkan Dhiar, mendapatkan kekerasan seksual yang dilakukan oleh atasannya di sebuah media nasional di tempatnya bekerja, hal-hal yang tidak pernah ia bayangkan sebelumnya ketika ia memilih bekerja di media. Ega, seorang perempuan pekerja kreatif mengalami depresi hebat karena harus memenuhi target deadline pekerjaan di dunia industri kreatif yang datangnya bertubi-tubi. Kisah lain yaitu kisah Ino, perempuan karena pilihan orientasi seksualnya kemudian dilarang untuk tampil di televisi.

Sejumlah pekerja film dan seniman perempuan mengalami hal yang tak jauh beda, harus melayani rayuan agar karir mereka menapak dengan bagus. Sedangkan jurnalis perempuan, masih harus memperjuangkan hak normatif perempuan pekerja seperti cuti haid, cuti melahirkan dan ruang menyusui agar mereka nyaman dalam bekerja.

Dalam bekerja, juga terdapat banyak diskriminasi terhadap pekerja perempuan di media dan industri kreatif. Pembagian porsi sebagai perempuan menarik dan tidak menarik, dengan sendirinya melakukan diskriminasi antar perempuan. Praktek cat calling yang banyak terjadi di media dan industri kreatif juga merupakan konstruksi dari anggapan yang membuat stereotype menarik dan tidak menarik pada perempuan.

Stereotipe ini juga melekat dalam berbagai tayangan: dari film, sinetron, infotainment, telewicara, hingga berita. Gambaran tentang perempuan pemarah, pencemburu, pendendam ada dalam stereotype tayangan sinetron. Stereotipe cantik ini kemudian menjalar di ruang-ruang redaksi dan produksi di pemberitaan televisi,di film/ industri kreatif.

Disinilah muncul berbagai persoalan: ada persoalan kelas dari pemberi kerja kepada penerima kerja (perempuan),persoalan hirarkhi yang membentuk budaya dan menjadikan perempuan sebagai korban dan ketergantungan ekonomi perempuan.

Film ini akan menyajikan persoalan komoditas yang menimpa para pekerja perempuan di media dan industri kreatif yang “berpengaruh” cukup besar terhadap perkembangan media baru (new media) bagi masyarakat di Indonesia.

- **Tujuan**

  Pembuatan film bertujuan agar film menjadi alat kampanye dan menjadi alat advokasi kebijakan pemerintah dalam pembuatan konvensi stop kekerasan di dunia kerja secara umum dan stop kekerasan perempuan di media dan industri kreatif secara khusus

- **Sasaran**

  1. Film digunakan lembaga dan jaringan buruh perempuan sebagai media kampanye dan advokasi di lingkungan pemerintah, serikat pekerja, organisasi, universitas, industri media, pengusaha dan masyarakat umum untuk menggalang dukungan terhadap “stop kekerasan terhadap perempuan di media dan industri kreatif.”

  2. Film bisa menjadi rujukan dokumen dan data contoh kisah-kisah pelecehan dan kekerasan yang  menimpa perempuan di media dan industri kreatif

- **Latar Belakang**

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Berbagai jaringan perempuan di Indonesia dan dunia, saat ini sedang mulai mengkampanyekan kampanye stop kekerasan di dunia kerja dan mendesakkan agar segera diratifikasinya Konvensi Stop Kekerasan di Dunia kerja oleh pemerintah.

     Dunia kerja adalah sebuah ruang dimana terdapat banyak ketergantungan ekonomi dari pekerja ke perusahaan. Dunia kerja terjadi sejak masa perekrutan hingga pekerja sampai di rumah.

     Kekerasan di dunia kerja di media terjadi sejak di masa perekrutan, ketika berada di tempat kerja sampai para pekerja dalam proses dan pulang sampai di rumah.

     Kekerasan ini bisa terjadi dalam bentuk-bentuk seperti kekerasan fisik, kekerasan seksual (termasuk perkosaan dan serangan seksual), kekerasan verbal, bullying, kekerasan psikologi dan intimidasi, pelecehan seksual, ancaman kekerasan, kekerasan ekonomi dan keuangan, dan memata-matai.

     Ruang lingkup kerja mencakup dalam tempat kerja secara fisik, termasuk ruang publik dan domestik di mana ruang-ruang itu adalah tempat kerja, di tempat-tempat di mana pekerja dibayar atau mengambil makan, perjalanan dari dan ke tempat kerja, selama perjalanan yang berhubungan dengan pekerjaan, di acara-acara atau aktivitas sosial yang berhubungan dengan pekerjaan, dan selama training yang berhubungan dengan pekerjaan dan melalui komunikasi yang berhubungan dengan pekerjaan yang difasilitasi oleh teknologi informasi dan komunikasi.

     Kekerasan terhadap perempuan pekerja media dan pekerja kreatif umumnya dilakukan pada saat:

       1. Perekrutan

          Syarat-syarat perekrutan yang bias bagi pekerja media perempuan TV karena hanya wajah yang menarik dan cantik dan postur tubuh yang proporsional yang bisa diterima sebagai pekerja.

          Selain itu harus mau bekerja di setiap saat dan setiap waktu jika dibutuhkan dan harus mau bekerja di bawah tekanan, padahal perempuan yang berprofesi sebagai ibu, umumnya harus mengambil alih pekerjaan domestik dan tak dibantu laki-laki suaminya. Bekerja dengan penuh tekanan ini tidak mempertimbangkan posisi perempuan sebagai pekerja.

       2. Tempat Kerja

             - Jam kerja yang sangat tinggi (kelebihan jam kerja), apalagi perempuan ibu umumnya juga bekerja di sektor domestik.

             - Mitos yang didengungkan di tempat kerja kepada para pekerja media: jika bekerja tidak melebihi jam kerja maka dianggap tidak mempunyai etos kerja, banyak perempuan yang hamil dan menyusui mendapatkan cap kurang etos kerja yang tinggi

             - Pelecehan dan diskriminasi terhadap perempuan yang terjadi di tempat kerja

             - Larangan dalam membuat Serikat Pekerja, Lembur tak pernah dibayar, PHK sewenang-wenang

             - Kebijakan yang mendiskriminasi perempuan

        3. Pulang Kerja

           Karena jam kerja yang tinggi, ketika pulang bekerja, para perempuan yang bekerja di media dan industri kreatif sering sekali pulang malam, ketakutan pulang malam selalu menghantui perempuan, seperti takut jika diperkosa, diganggu, dirampok, diikuti laki-laki.

        4. Di rumah

           Sampai rumah, para perempuan tak henti bekerja, seorang ibu harus bekerja secara domestik seperti memasak, membersihkan rumah, mencuci, menjaga anak dan menemaninya belajar. Perempuan  yang bekerja di media dan industri kreatif kadang masih mengerjakan pekerjaannya di rumah yaitu menyelesaikan deadline pekerjaan.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Masalah yang ingin diatasi di media dan industri kreatif antaralain:

        1. Komoditas terhadap perempuan di tempat kerja (media dan industri kreatif)

        2. Kebijakan yang menjadikan perempuan pekerja sebagai korban konstruksi ini

        3. Kekerasan dan pelecehan yang berakibat pada diskriminasi pekerja perempuan

     Mengapa mengakhiri kekerasan di dunia kerja ini merupakan sesuatu yang penting? Hingga saat ini Indonesia belum memiliki Undang-Undang (UU) nasional yang melindungi pekerja baik formal maupun informal dari kekerasan, diskriminasi dan pelecehan di dunia kerja. Padahal faktanya masih banyak pekerja terutama perempuan yang mengalami kekerasan dan pelecehan namun belum berani melaporkan ke aparat hukum karena belum ada jaminan hukumnya. Oleh karena itu perlu satu peraturan atau konvensi yang mengatur perlindungan pekerja dari kekerasan dan pelecehan yang nantinya dapat menjadi acuan aturan hukum nasional.

     Hal lain, Indonesia telah terikat secara hukum dengan Konvensi Internasional tentang Penghapusan Segala Bentuk Diskriminasi Terhadap Perempuan karena pemerintah Indonesia telah meratifikasinya melalui UU No. 7 Tahun 1984.  Perlindungan pekerja perempuan dari berbagai bentuk kekerasan dan pelecehan seksual di tempat kerja menjadi salah satu perhatian dalam Rekomendasi Umum Komite CEDAW No. 19 tentang Kekerasan Terhadap Perempuan Tahun 1992.

     Film ini akan digunakan sebagai alat advokasi kebijakan dan kampanye dalam perjuangan untuk stop kekerasan terhadap perempuan di dunia kerja secara umum dan stop kekerasan terhadap perempuan di media dan industri kreatif secara khusus.

  3. Strategi

     Strategi yang dilakukan yaitu dengan menggalang dukungan bersama kelompok buruh, organisasi perempuan dan kelompok buruh perempuan, universitas dan organisasi jurnalis untuk menggunakan film sebagai alat advokasi stop kekerasan perempuan di dunia kerja dan di media/ industri kreatif.

     Dalam jaringan advokasi ini, Luviana (penerima hibah) sudah terlibat dalam advokasi stop kekerasan di dunia kerja bersama kelompok buruh, kelompok perempuan dan selama ini aktif dalam mengadvokasi stop kekerasan di dunia kerja pada pemerintah dan kelompok lain seperti pengusaha.

     Setelah menggalang dukungan, film ini selanjutnya akan menjadi alat kampanye dan advokasi jaringan untuk kebijakan stop kekerasan perempuan di media dan industri kreatif.

  4. Aktivitas dan keterkaitan pada sasaran
  
     Pemutaran film sebagai bagian dari kampanye dan advokasi akan dilakukan di berbagai kota di Indonesia yang akan dimulai pada: Agustus 2018. Pemutaran film dan diskusi di berbagai kota dan tempat ini akan menjadi jembatan masuk untuk menggalang dukungan stop kekerasan bagi perempuan di media dan industri kreatif.

     Selain itu, film juga akan diputar dalam sejumlah acara advokasi stop kekerasan di dunia kerja di pemerintah dan berjaringan dengan organisasi perburuhan di daerah, nasional dan internasional.

     Selain pemutaran dan diskusi, juga akan melibatkan lembaga-lembaga perburuhan, lembaga perempuan, serikat pekerja, universitas, lembaga media dan jurnalis yang terlibat dalam penulisan resensi film dan pengguna sosial media, blogger dan terlibat dalam advokasi stop kekerasan terhadap perempuan di media.

     Pemutaran film yang marak dilakukan akan menumbuhkan kesadaran kritis. Kesadaran kritis masyarakat dan pemerintah inilah yang akan mempermudah proses advokasi kebijakan stop kekerasan perempuan di media dan industri kreatif.

        1. Sasaran Pemerintah dan DPR

           Film akan diputar untuk memberikan data stop kekerasan terhadap perempuan di media dan industri kreatif dan mengubah kebijakan untuk perempuan di dunia kerja

        2. Universitas dan Publik

           Film akan diputar di universitas memberikan kesadaran kritis bagi para mahasiswa yang akan bekerja di media

        3. Serikat Pekerja

           Film akan diputar di sejumlah serikat buruh untuk memberikan ruang pembelajaran tentang masih banyaknya kekerasan yang terjadi pada perempuan di dunia kerja dan mengubah kebijakan di kalangan internal serikat pekerja

        4. Industri media

           Film sebagai alat kampanye untuk mengubah eksploitase, kekerasan dan diskriminasi yang banyak terjadi di industri media

  5. Latar belakang dan demografi pelaku proyek

     Sutradara dan produser film adalah perempuan penerima hibah (Luviana) yang sudah selama 20 tahun bekerja di media dan aktif dalam advokasi kebijakan perburuhan dan kebijakan media.

     Pengerjaan proyek juga dibantu tim produksi film yaitu director of fotografi, laki-laki yang selama ini bekerja sebagai cameraperson di film dokumenter dan film layar lebar. Juga dibantu oleh satu produser perempuan.

  6. Pemimpin proyek

     Pemimpin proyek adalah perempuan dengan pengalaman selama 20 tahun bekerja di media dan selama kurang lebih 15 tahun aktif dalam advokasi pekerja, perempuan dan media

  7. Demografi kelompok target

        - Pemerintah yang menjadi sasaran target utama dalam mengeluarkan kebijakan stop kekerasan di dunia kerja.

        - Serikat pekerja, yang memberikan dukungan advokasi dan mengubah kebijakan serikat pekerja agar berperspektif perempuan

        - lembaga perempuan dan lembaga media, yang memberikan dukungan pada advokasi

        - Universitas dan publik, memberikan pendidikan kritis pada mahasiswa dan literasi media pada mahasiswa dan publik

  8. Hasil yang diharapkan

     Dari penelitian ini diharapkan didapatnya sejumlah data berupa tulisan dan sejumlah photo yang nantinya akan di cetak dan dipublikasikan dalam bentuk buku. Dalam acara peluncuran buku ini akan dilakukan dua kegiatan yakni pameran photo dan diskusi di tiga tempat yakni

        1. Film diputar di berbagai daerah di Indonesia dan menjadi alat kampanye advokasi kebijakan stop kekerasan di dunia kerja dan advokasi kebijakan pada pemerintah dalam konvensi mengakhiri kekerasan di dunia kerja

        2. Film menyumbangkan data untuk Lembaga dan jaringan masyarakat sipil atas persoalan pekerja perempuan di media dan industri kreatif

        3. Film menjadi alat kampanye perubahan kebijakan di di serikat-serikat pekerja, di media, industri kreatif, perusahaan

        4. Film menjadi alat kampanye/ sosialisasi bagi para mahasiswa di universitas dan masyarakat umum untuk mengubah konsep konstruksi tentang dunia kerja

  9. Indikator keberhasilan

        1. Film diputar di berbagai daerah di Indonesia

        2. Film diputar minimal 25 kali dalam setahun

        3. Film akan ditonton minimal oleh 2500 orang dalam 1 tahun

        4. Film akan menjadi data penting bagi advokasi dan perubahan kebijakan di pemerintah

        5. Film menjadi tontonan kritis bagi industri media, mahasiswa, serikat pekerja dan publik

  10. Durasi Waktu Aktivitas dilaksanakan

         1. 2 bulan pembuatan film (Mei-Juli 2018)

               - Mei: riset dan shooting

               - Juni: transkrip film, time code dan pengumpulan bahan produksi (grafis, dll)

               - Juli: Editing

         2. Launching dan Pemutaran Film mulai: Agustus 2018

  11. Total kebutuhan dana

         1. 50 juta

         2. Tambahan untuk launching di Jakarta dan luar Jakarta: 10 juta (jika ada)

  12. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

         1. 50 juta (biaya produksi film)

         2. Tambahan untuk biaya launching dan pemutaran film di luar kota: 10 juta (jika ada)

  13. Sumber dana lainnya

      Tersedianya tempat pemutaran film

  14. Kontribusi perorangan

      Tidak ada

  15. Kontribusi kelompok target

      Tersedianya tempat pemutaran film dan narasumber secara cuma-cuma oleh puluhan lembaga yang mendukung advokasi

      Pemerintah menyediakan ruang pemutaran film yang diakses publik
