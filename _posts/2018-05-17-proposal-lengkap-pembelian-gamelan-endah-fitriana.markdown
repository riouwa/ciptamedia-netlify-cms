---
title: Proposal Lengkap - Pembelian Gamelan - Endah Fitriana
date: 2018-05-17
permalink: /ciptamediaekspresi/proposal-lengkap/pembelian-gamelan
layout: proposal
author: Endah Fitriana
categories:
- laporan
- CME
- Pembelian Gamelan
---

![0164.jpg](/static/img/hibahcme/0164.jpg){: .img-responsive .center-block }

# Endah Fitriani - Pembelian Gamelan

**Tentang penerima hibah**

Endah Fitriana adalah sukarelawan pengajar tradisi Macapatan di Omah Kendeng, tempat perempuan Sedulur Singkep bertemu dan bertukar pikiran mempercakapkan nilai-nilai spiritual kritis di Kendeng Utara.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

Omah Kendeng, Pati, Jawa Tengah

**Deskripsi Proyek**

- **Tujuan**

  Melestarikan tradisi Macapatan sebagai media belajar sehari hari antar generasi perempuan Sedulur Sikep kepada anak anak di Kendeng Utara menggunakan gamelan.

- **Sasaran**

  Anak-anak mempraktekkan Macapatan

- **Latar Belakang**

  1. Keterkaitan pada kategori: Lintas Generasi

     Perpindahan pengetahuan dari yang tua ke yang muda terkait nilai-nilai spiritual mereka mengenai pentingnya masyarakat terlibat dalam persoalan lingkungan hidup sejak di lingkup terkecilnya yaitu keluarga

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

    Aktivitas Macapatan tidak menarik untuk dilakukan karena gamelan sebagai infrastruktur pendukung rusak parah.

  3. Strategi

     Membeli seperangkat gamelan baru

  4. Aktivitas dan keterkaitan pada sasaran

     Aktivitas Macapatan dapat dilakukan dengan baik dan perpindahan pengetahuan lintas generasi dapat terjadi

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan dengan pengalaman mengajar selama 7 tahun di Omah Kendeng, berumur 45 tahun berasal dari kelas menengah masyarakat, dan secara berangsur-angsur jumlah anak yang diajar bertambah dari 14 orang menjadi 36 orang di Omah Kendeng.

  6. Demografi kelompok target

     Tradisi Macapatan ditargetkan untuk anak-anak berumur 5-14 tahun atas ijin orang tuanya.

  7. Hasil yang diharapkan dan indikator keberhasilan

     Endah berharap anak anak tumbuh dg nilai.nilai ajaran Sedulur Sikep (Samin):

     1. Tidak iri hati dg sesama.

     2. Tidak bertengkar.

     3. Tidak mengutamakan masalah duniawi.

     4. Tidak mengganggu orang lain.

     5. Tidak mengambil hak/ milik orang lain.

     6. Tidak sombong.

     7. Sabar.

     8. Jujur.

     9. Saling menghormati.

  8. Indikator keberhasilan

     1. Jumlah anak anak yang belajar meningkat

     2. Keantusiasan anak-anak bertambah

  9. Durasi Waktu Aktivitas dilaksanakan

     Mei 2018 - Januari 2019

  10. Total kebutuhan dana

      384 juta

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      384 juta

  12. Sumber dana lainnya

      Tidak ada

  13. Kontribusi perorangan

      Pengajar sukarela

  14. Kontribusi kelompok target

      Partisipasi dalam program
