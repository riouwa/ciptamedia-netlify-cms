---
title: Rapat Penentuan Konsep Hibah Cipta Media Ekspresi
date: 2017-11-04T04:58:08.030Z
categories:
  - CME
  - berita
tags:
  - hibah
  - cipta media ekspresi
author: Ivonne Kristiani
comments: true
img: /uploads/blog/2017-11-04-16.22.23.jpg
---
![](/uploads/blog/2017-11-04-16.22.23.jpg)

Pada 4 November 2017, untuk pertama kalinya kedelapan juri Cipta Media Ekspresi bertemu muka di Hotel Liberty, Jakarta untuk bertukar pikiran dan mematangkan konsep hibah untuk seniman perempuan ini. Hadir dalam pertemuan ini adalah Lisabona Rahman sebagai Ketua Juri, Heidi Arbuckle sebagai penggagas hibah terbuka Cipta Media, serta anggota tim juri lainnya yaitu Andy Yentriyani, Cecil Mariani, Nyak Ina Raseuki, Mama Aleta Ba'un, Intan Paramaditha, dan Naomi Srikandi. Panitia yang turut hadir adalah Siska Doviana (Manajer Proyek), Ivonne Kristiani (Asisten Proyek), dan Dwi Satria Utama (Staf IT & Desain).

Hasil dari rapat kerja ini adalah dokumen kerangka acuan kerja yang menjadi dasar prinsip dan lingkup hibah Cipta Media Ekspresi. Dalam kesempatan ini, dimatangkan bahwa hibah ini diberikan dengan tujuan untuk memperkuat peran perempuan sebagai pelaku kebudayaan yang aktif mewujudkan cara berpikir dan bertindak yang merayakan perbedaan di dalam masyarakat, terutama dari sudut pandang dan pengalaman perempuan. Hibah ini juga dikhususkan untuk melakukan aktivitas seperti pembuatan atau pengkajian karya baru, menjalin kerjasama dengan para pelaku kebudayaan lain, dan melakukan perjalanan untuk tujuan mencipta atau menampilkan karya. Karya yang dimaksud dapat berupa karya cipta, kajian, serta pemilihan benda-benda untuk dipamerkan atau diarsipkan dengan maksud mempertunjukkan atau berbagi. 

Rapat ini juga menentukan lima kategori hibah yang merupakan prioritas untuk didanani dari hibah ini. Kelima kategori tersebut adalah Akses, Perjalanan, Kerjasama/Kolaborasi, Lintas Generasi, serta Riset/Kajian/Kuratorial.

Jumlah total dana yang dapat diakses melalui Cipta Media Ekspresi adalah Rp 3,5 milyar. Hibah ini terbuka untuk proyek yang akan dijalankan sampai 28 Februari 2019. 

Rapat juga disertai dengan pengambilan gambar profil masing-masing juri oleh fotografer Eva Tobing.
