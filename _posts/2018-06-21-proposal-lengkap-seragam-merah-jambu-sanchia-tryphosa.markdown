---
title: Proposal Lengkap - Seragam Merah Jambu - Sanchia Tryphosa Hamidjaja
date: 2018-06-21
permalink: /ciptamediaekspresi/proposal-lengkap/seragam-merah-jambu
layout: proposal
author: Sanchia Tryphosa Hamidjaja
categories:
- laporan
- CME
- Seragam Merah Jambu
---

![0014.jpg](/static/img/hibahcme/0014.jpg){: .img-responsive .center-block }

# Sanchia Tryphosa Hamidjaja - Seragam Merah Jambu

**Tentang penerima hibah**

Sanchia Hamidjaja adalah seniman visual / kartunis berbasis di Jakarta, Indonesia. Ia memperoleh gelar S1 jurusan Desain Komunikasi Visual dari Swinburne National Institute of Technology, Mebourne, Australia pada tahun 2004. Sanchia menekuni karirnya yang pendek di bidang periklanan sebagai desainer grafis, dan pengarah artistik dan ilustrator di suatu studio animasi, sembari menerima pekerjaan sampingan sebagai pelukis mural. 

Setelah 7 tahun bekerja di industri periklanan, Sanchia memutuskan untuk fokus menjadi seniman visual dan ilustrator lepas. Karyanya mencerminkan berbagai media kreatif, seperti narasi visual / komik dan kartun. Pada tahun 2011 Sanchia berpameran tunggal untuk pertama kali di Inkubator Gallery yang bertajuk The Yin & Yang Dogs. Sejak itu Sanchia banyak berpameran di beberapa galeri dan museum ibukota, juga bermural di beragam hotel dan restoran. Saat ini Sanchia menetap di Tangerang Selatan bersama suami dan anaknya.

**Kontak**

  - Instagram: [@sanchimilikiti](https://www.instagram.com/sanchimilikiti/)

**Lokasi**

Jakarta

**Deskripsi Proyek**

Novel grafis mengangkat berbagai peranan perempuan sebagai ibu dan istri, mengangkat situasi politik Indonesia dan berbagai efek korupsi dalam kemanusiaan, menyentuh permasalahan gender dan sosial politik. Bab I dari novel grafis mengenai pengalaman hidup istri anggota Brimob, kewajibannya sebagai anggota Bhayangkari, meredupnya jati diri dan ketakutan ketakutan yang konstan dalam pekerjaan suaminya akan bebas diakses dengan lisensi CC-BY.

- **Tujuan**

  1. Membuat novel grafis fiksi yang terinspirasi dari kehidupan nyata, dengan gaya realisme. Sebuah cerita yang merepresentasikan kehidupan domestik perempuan Indonesia, dengan latar belakang institusi, situasi sosial dan politik yang ekstrim. Dengan harapan dapat mencetuskan pertanyaan-pertanyaan dan diskusi-diskusi tentang peranan wanita dari pilihan-pilhan jalan hidup yang sangat terpengaruh dengan tekanan norma-norma yang ada dalam rumah tangga, dan lapisan institusi tersebut.

  2. Menantang dan mengembangkan praktisi pribadi dalam media komik dan ilustrasi.

- **Sasaran**

  1. Menyelesaikan jilid 1 dari proyek yang saat ini sudah melalui beberapa draft naskah, dan sekitar 25-30 halaman sketsa. Diharapkan untuk selesai hingga siap cetak.

  2. Melakukanan riset visual untuk Jilid ke 2 dari buku ini yang latar belakang ceritanya di Aceh.

  3. Menemukan penerbit yang tepat di kemudian hari.

  4. Mengembangkan komunitas seni komik dan ilustrasi melalui genre yang berbeda di indonesia dan dengan terwujudnya proyek ini diharapkan menginspirasikan lebih banyak seniman perempuan untuk bercerita, dan berkarya lewat komik.

- **Latar Belakang**

  1. Keterkaitan pada kategori: Kerjasama / Kolaborasi

     Novel grafis ini terinspirasi dengan kehidupan nyata, dalam pengembangan ceritanya terjadi kerjasama antara perupa dan narasumber dan juga kerjasama antara penterjemah dan penyunting naskah.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

        1. Novel grafis ini mengangkat berbagai masalah peranan seorang ibu dan istri, situasi politik Indonesia, berbagai ripple efek korupsi dalam kemanusiaan. Membuka sisi lain dari kultur kepolisian, kesehatan jiwa, Aceh pasca konflik dan tsunami. Meneliti peranan perempuan dalam kondisi rumah tangga dengan kelas dan subkultur yang berbeda beda. Juga mengangkat kontras budaya Jakarta dan Aceh beserta dampak-dampak konflik didalamnya.

        2. Kurangnya karya novel grafis indonesia yang merepresentasikan cerita-cerita perempuan Indonesia.

        3. Kesulitan meluangkan waktu untuk mengerjakan proyek ini secara intesif, dikarenakan kewajiban dalam rumah tangga serta pekerjaan sebagai ilustrator lepas, diharapkan hibah dana dapat memberi dukungan berupa fasilitas yang dibutuhkan demi mewujudkan proyek ini.

  3. Strategi

     Dibutuhkan ruang bekerja diluar rumah, yakni studio, agar dapat mengerjakan novel ini dengan intensif, serta membuka kegiatan internship / magang bagi mahasiswa ilustrasi / seni rupa, untuk membantu mempercepat proses yang pada umumnya akan memakan waktu jauh lebih lama. Yang sekaligus membagi pengalaman proses berkarya. Juga kerjasama dengan penterjemah dan penyunting naskah komik.

  4. Aktivitas dan keterkaitan pada sasaran

        1. Aktivitas untuk sasaran A:

           Membuka kegiatan internship / magang bagi mahasiswa ilustrasi / seni rupa, untuk membantu mempercepat proses yang pada umumnya akan memakan waktu jauh lebih lama. Yang sekaligus membagi pengalaman proses berkarya. Juga kerjasama dengan penterjemah dan penyunting naskah komik.

        2. Aktivitas untuk sasaran B:

           Menjalin network di Aceh untuk dapat melakukan perjalanan kesana, untuk tahap riset visual yang sangat dibutuhkan sebagai referensi gambar, jilid ke 2 pada buku ini. Supaya terlihat betul perbedaan ilustrasi suasana jilid 1 yang berlatar belakang di jakarta dan jilid 2 yang bertempatkan di Aceh. Bertemu narasumber untuk membantu mendalami budaya dan keadaan pasca-konflik disana, sesuai dengan latar belakang cerita di balik grafis novel saya.

        3. Aktivitas untuk sasaran C:

           Diharapkan dengan menyelesaikan Jilid 1, hasilnya dapat dijadikan proposal untuk di presentasikan kepada calon penerbit.

        4. Aktivitas untuk sasaran D:

           Buku novel grafis dapat di distribusikan kepada berbagai kalangan komunitas komik maupun non-komik.

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah perempuan dengan pengalaman berkarya 10 tahun terakhir, khususnya media ilustrasi dan desain grafis, memiliki komitmen membuat komik bentuk panjang (30-80 halaman). Mengerti betul setiap tahap prosesnya. Anggota tim lainnya adalah penyunting sekaligus penerjemah yang mampu menterjemahkan dari bahasa inggris ke indonesia, karena saat ini proyek yang setengah jadi ini masih dalam bahasa inggris. Serta asisten studio, yaitu mahasiswa desain atau senirupa, perempuan / laki-laki usia 20-30 tahun yang akan dibina ilustrator (pemimpin proyek) untuk belajar proses dan membantu mempercepat prosesnya.

  6. Demografi kelompok target

     Perempuan dan laki-laki usia 20-35 tahun dengan ketertarikan cerita fiksi tentang kemanusiaan, sosial dan politik.

  7. Hasil yang diharapkan dan Indikator keberhasilan

     Keberhasilan proyek ini tercapai ketika terselesaikan dalam waktu yang di rencanakan, dalam biaya yang efisien, konsistensi kualitas gambar, dan story telling yang baik, jelas dalam mengungkapkan pesan-pesan, atau isu-isu yang membuka sudut pandang para pembaca, baik di Indonesia maupun secara global.

  8. Durasi Waktu Aktivitas dilaksanakan

     9 bulan

  9. Total kebutuhan dana

     IDR 88.000.000

  10. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      IDR 88.000.000

  11. Sumber dana lainnya

      Belum ada

  12. Kontribusi perorangan

      Perupa akan menyelesaikan jilid-jilid selanjutnya dalam 3 tahun kedepan dengan bantuan hibah dana dari sumber-sumber lain.

  13. Kontribusi kelompok target

      Memicu berdirinya komunitas genre baru dalam dunia komik. Menginspirasikan lebih banyak seniman perempuan untuk bercerita, dan berkarya lewat komik.
