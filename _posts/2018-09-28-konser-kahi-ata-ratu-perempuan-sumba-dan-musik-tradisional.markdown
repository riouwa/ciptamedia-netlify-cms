---
title: Konser Kahi Ata Ratu - Perempuan Sumba dan Musik Tradisional
date: 2018-08-17T07:26:10.669Z
categories:
  - CME
  - Acara
  - Perempuan Sumba dan Musik Tradisional
tags:
  - sumba
  - sumba timur
  - musik tradisional
  - musisi perempuan
  - jungga
author: Nensi Dwi Ratna
comments: true
img: /uploads/blog/whatsapp-image-2018-08-01-at-14.56.14-1-.jpeg
---
![](/uploads/blog/picture1.png)

Berangkat dari keprihatinannya atas kurangnya peminat dari musik tradisional  dikalangan generasi muda, maka Ata Ratu, seorang seniman musik tradisional serta pencipta lagu–lagu Sumba, terpanggil untuk mengadakan riset  lagu–lagu yang dulu yang hampir punah serta mengadakan kolaborasi dengan generasi tua guna pelestariannya. 

Ata Ratu merupakan peraih Hibah Cipta Media Ekspresi 2018 untuk kategori riset serta kerja sama dan kolaborasi. Cipta Media Ekspresi adalah sebuah program yang diselenggarakan guna mendukung karya perempuan, menyebarkan pikiran – pikiran mereka untuk memperkaya masyarakat dan merayakan keragaman pengetahuan dan ekspresi kreativitas perempuan di Indonesia.

Ata Ratu berhasil menampilkan hasil karya dan risetnya di Bentara Budaya Bali ( BBB ) pada minggu (05/08/2018). Musik ini pula yang dibawakannya di ajang program Europalia, sebuah event yang menampilkan karya – karya musisi Indonesia di Belgia dan beberapa negara eropa lainnya.

Kahi Ata Ratu lahir di Sumba Timur, tepatnya di Desa Rindi. Sejak berusia 13 tahun ia telah menulis lagu dan menampilkan musik di Sumba dalam berbagai macam kegiatan pemerintahan maupun acara - acara adat yang dinyanyikan dalam bahasa local ( kambera ).

Dia adalah salah satu dari sedikit perempuan yang piawai memain – nyanyikan alat musik jungga, alat musik petik yang lebih dikenal dalam ranah musik laki – laki selama lebih dari 40 tahun dan dia dikenal sebagai _“the queen of jungga”._

Musik–musik ini merupakan bentuk musik sumba yang relative modern, dipengaruhi oleh gaya sebelumnya yang menggunakan jungga dengan dua senar, jelasnya.

Dalam kesempatan ini, Ata Ratu yang juga pencipta lagu, mempresentasikan hasil risetnya berupa memainkan musik tradisional dan musik ciptaannya serta pemutaran video dokumenter.

![](/uploads/blog/picture8.png)

Sebagaimana terjadi di banyak daerah di nusantara, musik tradisional juga mengalami kaderisasi atau penekun generasi muda, serta menjadi suatu yang langka bahkan di kampung halamannya sendiri. Generasi muda lebih condong mendalami musik–musik modern yang mudah diakses melalui smartphone serta internet.

Menurut Ata Ratu, ditengah langkanya promosi dan pengenalan akan musik tradisional, di Sumba masih ada pemusik tradisional yang gigih dan tekun mengupayakan pengembangan musik tradisional Sumba. Walau kebanyakan masih didominasi pemusik laki – laki. Terbilang jarang ada perempuan yang fasih memainkan  musik tradisional sumba, sekaligus menciptakan lagu.

“Oleh karena itu, saya ingin melakukan riset mengenai lagu – lagu dulu yang hampir punah dan juga melakukan kolaborasi dengan generasi tua untuk dapat mempertahankan kelestarian lagu – lagu dulu yang sudah tidak dikenal oleh generasi muda saat ini. Saya ingin mengatakan kepada dunia dan generasi muda saat ini bahwa wanita juga mampu berkarya dan juga bisa memimpin para pria”, ungkap Ata Ratu.

Sebagai pendukung program ini, dihadirkan juga sejumlah foto dokumentasi karya Joseph Lamont. Acara ini diselenggarakan atas kerja sama Bentara Budaya Bali dan Organ Budaya, sebuah jaringan kerja sama antar orang – orang dan kelompok yang memiliki minat dan kepedulian terhadap budaya Indonesia, serta didukung oleh Cipta Media Ekpresi.

![](/uploads/blog/picture8.png)
