---
title: Perempuan Intelektual di Sunda
date: 2019-02-24T09:32:37.060Z
categories:
  - CME
  - Kajian
  - Pendokumentasian Perempuan Intelektual Sunda Lasminingrat
tags:
  - garut
  - sunda
  - lasminingrat
  - intelektual
author: Rena Amalika Asyari
comments: true
img: /static/img/landing/akses.png
---
Tetes embun mulai menyapa alam Priangan. Biru yang bergunung-gunung dan memanjang menyisakan celah sempit untuk kilau matahari pagi. Perkebunan teh menghampar sepanjang Cikajang, beradunya tangan-tangan terampil perempuan dengan pucuk daun menjadi pemandangan pagi yang kerap disaksikan dengan khidmat oleh sekelompok burung. Sepagi itu paruh mereka sudah bernyanyi dan menari centil.

Garut, 1911. Dari kejauhan terdengar suara lembut alunan gamelan, sederhana dan hidup. Ada 200 anak. Beberapa sedang berlarian, sebagiannya lagi berada di ruangan kelas yang terbuat dari bambu, menjahit, membordir, menyetrika, mendekor, sebagiannya lagi ada di ruang paling belakang, dapur, belajar membuat api dari pasokan batu-bata, mempelajari arah angin dan menanak nasi. Anak-anak terlihat akrab dengan gurunya, mereka tak berjarak. Sakola Kautamaan Istri, yang dibangun dari dana sukarela dan subsidi pemerintah tahun 1913 sebesar f 1200 adalah harapan Kepala desa, petani, pedagang agar anaknya berhasil karena mendapat pendidikan modern. Di Sakola Kautaman Istri setiap individu memiliki hak untuk bertanya dan mendapat jawaban.

<br>

**Guru Intelektual**

Tahun 1860, Levysson Norman diangkat jadi pejabat pemerintahan Hindia Belanda. Tanah priangan yang mempesona membuat ia mengunjunginya, saat itulah ia tertarik pada gadis pribumi 17 tahun yang tampak kecerdasannya. Lasminingrat, anak penghulu besar Limbangan Garut Moehammad Moesa. Tanpa ragu, ia pun mengajukan diri menjadi pembimbing dan membawanya ke Sumedang, Lasminingrat akan diajarinya pendidikan barat.

Lasminingrat cukup beruntung. Ayahnya berpikiran terbuka, Moesa sadar benar pendidikan barat, modern membawa pengaruh baik untuk masa depan anak-anaknya. Kedekatannya dengan K. F Holle pemilik perkebunan teh di Cikajang membuat wawasannya luas. Ia tak gentar meski orang-orang di sekilingnya memandang ia sebagai kafir karena pandangannya terhadap barat. Moesa tak peduli. Di atas kecaman masyarakat, tahun 1874 ia mendirikan sekolah Eropa pertama yang dilakukan pribumi Bizondere Eureopeesche School’, dengan menggunakan biaya sendiri dan menghimpun dana dari kalangan menak Limbangan. Baru pada tahun 1876, sekolah ini menerima subsidi dari pemerintah sebesar 100 gulden per bulan. Jumlah muridnya mencapai 100 orang. Sekolah ini menjadi pembuka pintu perubahan bagi peradaban bangsa di Nusantara.

<br>

**Buah Pikir**

Melihat pergerakan ayahnya, Lasminingrat menjadi perempuan remaja yang berpikir bebas. Ia sangat tidak suka dengan pandangan sekitarnya bahwa perempuan harus selalu di rumah dan menjadi pendamping suaminya. Dari ayahnya, tuan Holle dan Norman ia belajar tentang ilmu pengetahuan barat. Kegemarannya membaca, mengantarkan ia pada sikap kritis dan gemar menulis. Dalam suratnya tahun 1874 kepada Veth, Tuan Holle menyebutkan Lasminingrat banyak menyadur cerita-cerita dongeng karangan Grimm, cerita-cerita dari negeri dongeng J.A.A Goeverneur dan cerita-cerita lainnya ke dalam bahasa Sunda.

Carita Erman menjadi salah satu karya Lasminingrat yang ditulis tahun 1875 dalam bahasa Sunda. Carita Erman merupakan terjemahan dari Christoph van Schmid. Carita Erman memiliki banyak peminat yang tersebar bukan hanya di daerah Sunda saja dan dicetak sebanyak 6015 eksemplar. Jumlah yang sangat banyak untuk masyarakat di Nusantara yang waktu itu masih buta huruf. Tahun 1911 Carita Erman dicetak ulang dalam aksara Jawa, tahun 1919 diterjemahkan ke dalam bahasa Melayu dengan judul Hikayat Erman oleh M.S Cakrabangsa dan tahun 1922 dicetak dalam aksara Latin. Hikayat Erman berkisah tentang Herman, seorang anak yang diculik dari orang tuanya. Hikayat Erman Ini dilengkapi dengan ilustrasi dan waktu itu harganya hanya f 0,25, di sumber lain ada juga yang menyebutkan dijual seharga f 0,4.

Karya lainnya yang terkenal yaitu Warnasari jilid 1 dan jilid 2 yang terbit tahun 1876 dan 1887. Warnasari berisi saduran cerita-cerita dongeng dunia. Karya-karya Lasminingrat menjadi bahan bacaan wajib anak sekolah.

<br>

**Lasminingrat dan Pemikirannya**

Melalui karyanya, Lasminingrat menuangkan pemikirannya. Kegelisahannya pada kondisi masyarakat di daerah Sunda tentang status perempuan yang tidak lebih tinggi dari laki-laki sangat mengganggunya. Hal ini terlihat dari petikan tulisannya:

_… abdi teh noe mawa nampik ka sadaja noe ngalamar tina tatjan aja noe tjotjog; lain abdi teu hajang ngiring kersa, lain abdi teu njaah ka gamparan, noe mawi tina sakijeu oge bawaning njaah; oepami abdi lakijan teu djeung pamilih noe sae dapon bogoh patoetna bae gampil pisan, nanging ahirna saha noe lingsem moen teu roentoet abdi nja laki rabi, sareng nja eta noe diemoetkeun teh koe abdi, tina gamparan geus sepoeh, ajeuna abdi dioepamaan dihargaan aja keneh gamparan, engke mah oepami gamparan geus teu aja, mowal aja noe ngoepamakeun mowal aja noe ngadjenan, katambah abdi lakijan gagabah, teu milih noe pinjaaheun ka diri abdi atawa ka sepoeh, sakoemaha gamparan melangna ninggalkeun abdi? Djeung sakoemaha teu raosna ningali atawa ngadangoe, ari loembrah lalaki tea, njaah resep bogoh ngan keur anjar bae, arang-arang noe kitoe salawasna, noemawi abdi sakadar-kadar ihtijar samemeh dongkap ka takdir, soegan aja lalaki noe tetep atina hade, oepami ngoeroes bogoh patoet bae woengkoel sadaja oge noe geus ngalamar ka abdi bogoh patoetna mah, ngan abdi teu bogoh polahna, kitoe margina noemawi abdi nampik, lain baha kana kersa, lain teu noeroet ka sepoeh, lain moengkir kana takdir lain teu hajang lakijan…._ (Tjarita Oraj Bodas, hlm 115,116)

… Saya bukannya menolak lamaran yang belum ada dan yang sudah ada; bukannya saya tidak mau mengikuti, bukannya saya tidak sayang pada orang tua, justru yang membuat saya begini karena saya sayang; jika saya menikah dengan yang tidak saya cintai itu mudah saja, hanya akhirnya siapa yang akan menanggung kalau bukan diri saya sendiri, dan itu pula yang saya pikirkan, orang tua sudah tua, sekarang saya dihargai karena saya masih punya orang tua, nanti jika orang tua saya sudah meninggal, tidak akan ada yang mengutamakan, tidak akan ada yang memberi hormat, ditambah lagi jika saya menikah secara sembrono, tidak memilih yang sayang pada saya atau ke orang tua, sebagaimana orang tua khawatir meninggalkan saya? Dan bagaimana tidak enaknya melihat atau mendengar, yang lumrahnya lelaki, sayang suka cinta pada awalnya saja, jarang-jarang yang selamanya, saya sekadar ikhtiar sebelum datang takdir, siapa tahu ada lelaki yang tetap hatinya baik, jika hanya sekadar cinta saja semua yang melamar ke saya semuanya cinta, tetapi saya tidak cinta sama tingkah lakunya, itu alasan kenapa saya menolak, bukannya tidak mengikuti, bukan tidak patuh ke orang tua, bukan juga menghindar dari takdir tidak mau menikah…. (penj: penulis)

Menarik sekali membedah pemikiran Lasminingrat, di zamannya ia sudah mendobrak tabu, perjodohan dan pernikahan dini yang dialami perempuan tidak ia sepakati. Lasminingrat mencoba memilih suami, ia tidak mau sembarangan memilih, ia memilih calon pendampingnya sesuai dengan kehendak hatinya, tidak mau dipaksa, karena dia berkeyakinan yang mengalami sakit dan pedihnya berumah tangga adalah dia, bukan orang lain. Menurutnya, kebanyakan laki-laki hanya suka di awal saja, jarang ada yang setia, bukannya dia tak mau bersuami, dia juga paham kalau takdir memang tak bisa lagi dihalangi, tapi setidaknya dia masih bisa memilih. Lasminingrat menekankan bahwa perempuan pun punya hak pilih. Lasminingrat menikah tahun 1865 ketika usianya 22 tahun, usia yang cukup mapan pada zaman itu.

Tentu sangat berat di tahun 1860-an menghadapi pola pikir masyarakat yang masih sangat patriarki. Lasminingrat membuka jalan untuk generasi berikutnya akan pentingnya hak-hak perempuan dan kebebasan pada setiap individu untuk memilih.

Selain peduli pada hak-hak perempuan, Lasminingrat juga menaruh perhatian pada pendidikan untuk anak-anak, terlukis dalam Carita Erman halaman 4, “Anak-anak harus selalu ditemani, tidak boleh ditinggalkan barang sekejap pun, harus diajak jalan-jalan ke kebun dan dikenalkan pada beragam jenis bunga, agar tahu mana yang boleh dimakan dan mana yang tidak. Selain itu, tidak boleh marah, melotot, ngambek apalagi memukul anak, karena anak kecil belum bisa berpikir”.

Lasminingrat juga menunjukkan kepeduliannya pada pengetahuan alam, ia bercerita tentang proses terjadinya pohon, meja, kursi, dan makanan. Segala sesuatunya terbuat dari biji. Biji ditabur jadilah pohon sebagai bahan membuat meja dan kursi. Bahkan dengan rumput-rumput yang kita injak, itu semua bermanfaat untuk makanan sapi yang tentunya bisa dibuat susu dan mentega yang terhidang di meja makan. Semuanya berawal dari biji yang ditabur di ladang yang sempit, tenyata menjadi rezeki untuk semua orang. Pemikirannya dituangkan dalam Carita Erman halaman 23.

Meskipun menganut paham kebebasan berpikir tidak membuat Lasminingrat meninggalkan agamanya. Hal ini tergambar dari banyaknya petikan Carita Erman halaman yang bisa kita dapatkan di tiap halaman tentang kecintaannya pada Tuhan Yang Maha Esa. Salah satunya adalah :

_“… Allah anoe ngadjadikeun kana sagala perkara anoe katingali koe andjeun di sakoeriling ieu, djeung ngadamel langit toer noe sakitoe matak helokna, djeung deui noe ngadamel tjahja panonpoe toer ngadatangkeun tjaang kana sagala roepa noe aja di sadjero alam doenya, sarta noe ngasakkeun kana boeboeahan, saperti seuneu ngasakkeun kedjo oerang”_ (Carita Erman, halaman 28)

“… Allah yang menjadikan segala perkara yang terlihat oleh kamu di sekeliling ini, dan yang membuat langit yang begitu indahnya, yang membuat cahaya matahari dan mendatangkan terang segala rupa yang di seluruh alam semesta ini, serta yang mematangkan semua buah-buahan seperti api mematangkan nasi kita”. (Penj : penulis)

Siapa sangka peradaban Sunda terbangun dari kisah-kisah perempuan. Ada cinta, pemikiran, dan kebebasan yang diperjuangkan. Raden Ayu Lasminingrat. Perempuan yang lahir di bawah kaki gunung Papandayan tahun 1843 ternyata menjadi salah satu perempuan intelektual pertama di Indonesia. Tak banyak yang tahu tentang pergerakannya, namanya tenggelam diantara nama besar tokoh penggerak perempuan dari budaya lain yang lebih mendominasi. Kiprahnya lahir jauh sebelum media cetak pribumi didirikan di Hindia Belanda sehingga namanya jarang disebut, hal ini juga dikarenakan pada masa itu jarang sekali perempuan melampirkan namanya sendiri, ia harus menyandang nama suaminya, adapula yang mengatakan namanya sengaja ditenggelamkan Belanda karena dia sangat membangkang beberapa aturan pemerintah Belanda.

Perempuan di Sunda sejatinya sudah berkiprah dalam dunia pendidikan sedari dulu. Lasminingrat, salah satu yang bisa mengubah stigma negatif tentang perempuan Sunda yang katanya hanya gemar bersolek saja.

<br>

**Daftar Pustaka :**

Effendie, Deddy. 2011. Raden Ajoe Lasminingrat (1843-1948) Perempuan Intelektual Pertama di Indonesia. CV Studio Proklamasi : Bandung

Ekadjati S, Edi. Kautamaan Istri Konsep Pendidikan Raden Dewi Sartika. Seri Sundalana. Killat : Bandung

Moriyama, Mikihiro. 2013. Dalam makalah “Kesastraan Sunda dan Kolonialisme dalam Sejarah Garut: jejak langkah Moehamad Moesa, Lasminingrat, dan Kartawinata” diseminarkan di Museum Sri Baduga

Overzicht van de Inlandsche en Maleisisch-Chineesche pers, 1924, no 29, 15-01-1924

Het nieuws van den dag voor Nederlandsch-Indië 04-05-1927

De Preanger-bode 23-06-1918

<br>

\*_Artikel ini pertama kali terbit di_ [_KelakarFibrasi.com_](https://kelakarfibrasi.com/2019/02/23/perempuan-intelektual-di-sunda/) _dan dipublikasikan ulang dengan izin penulis dan penerbit._
