---
title: Membiara untuk Mencintai Seutuhnya
date: 2019-02-11T06:09:00.527Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - CME
  - Perempuan
  - Sumba
  - biarawati
  - Katolik
author: Martha Hebi
comments: true
img: /uploads/blog/membiara-untuk-mencintai-seutuhnya.jpg
---
![](/uploads/blog/membiara-untuk-mencintai-seutuhnya.jpg)



Pilihan menjadi biarawan atau biarawati Katolik bukan pilihan main-main atau sekedar coba-coba. Paling tidak ada ada 3 kaul yang menjadi landasan hidup yang dengan ikhlas dan sepenuh hati dijalani yaitu kaul kemiskinan, kaul kemurnian dan kaul ketaatan. Ada beberapa ordo yang tidak mewajibkan ketiga kaul itu. Misalnya kongregasi projo bukanlah (imam) biarawan.

Kaul kemiskinan mengandung spirit hidup sederhana dimana pelepasan sukarela hak atas milik atau penggunaan milik tersebut dengan maksud untuk menyenangkan Allah. Semua harta milik dan barang-barang sang biarawan ataupun biarawati akan menjadi milik kongregasi, termasuk semua hadiah yang diterima dari siapapun (mengacu pada Kitab Hukum Kanonik Gereja Katolik 668).

Kaul kemurnian atau selibat mengandung spirit penyerahan total kepada Kristus, disini ada kewajiban biarawan dan biarawati tidak menjalani kehidupan perkawinan dan menghindari segala sesuatu yang dilarang oleh perintah keenam (jangan berzinah) dan kesembilan (jangan mengingini isteri sesamamu). Mengacu pada Kitab Hukum Kanonik Gereja Katolik 599.

Sedangkan kaul ketaatan mengandung spirit ketaatan kepada pimpinan kongregasi termasuk di dalamnya ketentuan peraturan dan nasihat para pimpinan (mengacu pada Kitab Hukum Kanonik Gereja Katolik 602).

Masa ini ketiga kaul penting tersebut kerap diiritasi oleh guyonan sarkastik di kalangan umat Katolik dan menyatakan ini sebagai refleksi iman. Ada guyonan seperti ini ; Jika Anda bertanya, apakah kaul kemiskinan masih berlaku pada hari ini? Bukankah dengan mudah kita akan menemukan biarawan atau biarawati Katholik yang menggenggam 3 handphone keluaran terbaru, mengendarai mobil mewah, wangi parfum mahal menyeruap dari tubuhnya, jam tangan mahal, pakaian dengan merek berkelas? Atau diam-diam menyembunyikan aset duniawi yang tidak dilaporkan kepada pimpinan mereka (kongregasi) dan menikmatinya layaknya umat biasa? Dengan alasan basi, barang yang dianggap mewah ini hadiah dari umat yang tidak bisa kami tolak, karena bentuk cinta mereka; ya biarawan biarawati pun harus wangi, masak berjumpa dengan umat dengan bau ketiak nanti umat tidak betah berdiskusi. 

Tentang kaul kemurniaan; kasus skandal seks (mantan) pastor dan (mantan) suster yang berakhir dengan pembunuhan dan terkuak beberapa tahun silam cukup mengejutkan. Lagi-lagi kelompok sarkastik berguyon, apakah betul tidak ada pastor atau suster yang diam-diam dan dengan sadar melanggar kaul kemurniannya?

Dan kaul ketaatan melahirkan pertanyaan dari kelompok sarkastik; bagaimana jika pimpinan kongregasi terlibat korupsi, apakah sang biarawan atau biarawati tetap taat padanya? Bagaimana jika ada biarawan atau biarawati yang mengendus kasus tersebut diminta (dipaksa) mengundurkan diri oleh pimpinannya?

Sumba menyimpan kisah istimewa seorang biarawati yang menyerahkan seluruh hidupnya dengan menjalani ketiga kaulnya. Dia memilih Sumba untuk membaktikan dirinya hingga akhir hayatnya. Kisah yang menghalau kegelisahan tentang pilihan hidup membiara, menyembuhkan kesangsian para pihak tentang keikhlasan para biarawan biarawati dalam menjalani kaulnya. Kisah hidupnya bisa menjadi inspirasi hidup membiara masa kini dan bagi siapa saja. 

Suster Matea Wijaya, SCMM (1938-2018) membuat sebuah keputusan besar dalam hidupnya pada usia 18 tahun. Dia memilih meninggalkan kemewahan dunia, keluar dari zona nyaman kehidupan keluarganya yang pada masa itu adalah keluarga berada. 

Kakak sulung pianis Hendra Wijaya ini, mengabdikan hidupnya sebagai seorang guru, memberikan cintanya kepada anak-anak dengan mendirikan Panti Asuhan di Waikabubak, Sumba Barat. Dia selalu memiliki anak emas di dalam panti asuhan. Ya, anak-anak berkebutuhan khusus.

Suster Matea hadir dalam kenangan murid-muridnya sebagai guru yang keras sekaligus guru yang lembut dan santun. Dia mencintai Sumba dan memilih Sumba untuk menghabiskan masa tuanya. Di saat yang sama, Kongregasi SCMM menyediakan fasilitas yang cukup baik bagi para anggota biara yang sudah menjalani masa tuanya. Mengapa? Apa saja warisan Suster Matea untuk kita? 

Saat menyampaikan sambutannya dalam upacara pemakaman Suster Matea, Niga Dapawole (sahabat Suster Matea yang saat ini menjabat Bupati Sumba Barat) menuturkan, “Satu waktu Suster Matea berkunjung ke rumah kami. Sandal jepitnya yang sudah lusuh putus di rumah. Dia memegangnya dan ingin membawa pulang. Saya bilang “Suster, kasih tinggal sudah itu sandal di sini. Nanti beli baru saja”. Suster Matea bilang “ Bapa Ida, biar saya bawa pulang, saya masih bisa pakai. Nanti saya pasang peniti.”

Ikuti kisah selengkapnya dalam Buku Perempuan (Tidak) Biasa di Sumba Era 1965-1998.

\#PerempuanTidakBiasa di Sumba Era 1965-1998

_Suster Matea meninggal dunia saat kami sedang dalam proses mewawancarainya._
