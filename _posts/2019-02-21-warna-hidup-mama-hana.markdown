---
title: 'Warna Hidup Mama Hana '
date: 2019-02-04T06:35:23.304Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - CME
  - Perempuan
  - Sumba
author: Martha Hebi
comments: true
img: /uploads/blog/warna-hidup-mama-hana.jpg
---
![](/uploads/blog/warna-hidup-mama-hana.jpg)



Mama Hana tidak membutuhkan pelangi untuk mewarnai hidupnya. Dia menenunnya sendiri dengan cinta. Ketrampilannya membuat kemucing, anyaman dari pandan dan lontar serta kerajinan tangan lainnya adalah pelangi untuk mewujudkan mimpi besarnya bagi pendidikan anak-anak Sumba.



Disabilitas bukanlah sebuah keterbatasan. Disabiltas bukanlah penghalang bagi Mama Hana untuk menyekolahkan puluhan anak Sumba.



Jika Anda menyusuri Kota Waingapu, Anda pasti menjumpainya. Setiap hari Mama Hana menempuh 2-3 km berjalan kaki menjual karya tangannya untuk masa depan anak-anak Sumba yang tinggal bersamanya.



Perempuan (Tidak) Biasa di Sumba, Era 1965-1998.
