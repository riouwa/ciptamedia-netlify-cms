---
title: Proposal Lengkap - Novel Wabah Campak dan Gizi Buruk di Asmat - Intan Andaru
date: 2018-05-17
permalink: /ciptamediaekspresi/proposal-lengkap/novel-wabah-campak
layout: proposal
author: Intan Andaru
categories:
- laporan
- CME
- Novel Wabah Campak dan Gizi Buruk di Asmat
---

![0313.jpg](/static/img/hibahcme/0313.jpg){: .img-responsive .center-block }

# Intan Andaru - Novel Wabah Campak dan Gizi Buruk di Asmat

**Tentang penerima hibah**

Intan Andaru adalah seorang penulis sekaligus dokter yang senang bergiat di bidang sosial dan literasi. Penggagas komunitas dan rumah baca di Halmahera Selatan. Tulisannya sering mengangkat tema kedokteran, kebudayaan, dan isu kemanusiaan. Beberapa novelnya diterbitkan oleh Gramedia, Diva press, dan Basabasi.

**Kontak**

Tidak ada, permintaan untuk mengontak penerima hibah bisa dilakukan via panitia Cipta Media Ekspresi: info@ciptamedia.org dengan menyatakan keperluan.

**Lokasi**

  - Domisili: Banyuwangi, Jawa Timur.

  - Lokasi Proyek: Asmat, Papua.

**Deskripsi Proyek**

Novel mengenai wabah campak dan gizi buruk di pelosok Papua dan kebudayaan masyarakat Asmat, dilakukan dalam tiga tahap: riset, penulisan, dan penerbitan. Riset dilakukan langsung di kabupaten atau distrik di Papua dengan mengumpulkan data korban rumah sakit atau dinas kesehatan, dan wawancara keluarga korban atau pihak pihak yang bersinggungan langsung dengan menimbang struktur alam dan sosio-kultural Suku Asmat.

- **Tujuan**

  Selain bertujuan untuk menuliskan duka Indonesia atas wabah campak dan gizi buruk yang telah menewaskan puluhan anak di pelosok Papua, penulis juga ingin mengkaji lebih dalam mengenai kondisi sosiokultural masyarakat Asmat, tradisi yang berhubungan dengan konsep sehat-sakit, dan kebudayaan masyarakat Asmat khususnya yang berkaitan erat dengan perempuan.

- **Sasaran**

  - Sasaran riset: Masyarakat setempat di Kabupaten Asmat (Distrik Agats dan Distrik Akad)

  - Sasaran novel: Dewasa muda, mahasiswa, aktivis, dan instansi terkait (pemerintah maupun lembaga swadaya masyarakat)

- **Latar Belakang**

  1. Keterkaitan pada kategori: Perjalanan

     Berangkat dari pengalaman pribadi ketika bekerja sebagai dokter di pelosok Halmahera Selatan selama hampir dua tahun, pelaku protek mendapati masalah kesehatan di negeri ini yang sangat kompleks—khususnya di Indonesia bagian timur. Puncak masalah itu terlihat pada kasus wabah campak dan gizi buruk di Asmat yang menewaskan puluhan anak Papua. Pelaku proyek merasa masalah tersebut perlu dikaji menggunakan kacamata berbeda dan diabadikan dalam bentuk karya sastra.

     Mengingat belum banyak karya sastra yang mengusung tema lokalitas (terutama tentang tradisi-tradisi perempuan di Indonesia timur) dan pentingnya mengabadikan tragedi yang pernah terjadi di negeri ini (terutama yang menyangkut masalah kemanusiaan), maka penulisan novel bertema tersebut di atas dirasa cukup penting untuk direalisasikan.

  2. Masalah yang ingin diatasi dan keterkaitan dengan aktivitas

     Diharapkan Indonesia memiliki karya sastra yang mampu merepresentasikan kondisi kesehatan dan sosiokultural Indonesia Timur, khususnya tragedi kemanusiaan di Asmat Papua sehingga dapat mengenal lebih jauh akan kondisi negeri sendiri.

  3. Strategi

     Menggabungkan peristiwa gizi buruk dan wabah campak dengan kondisi setempat, kebudayaan masyarakat, juga masalah tradisi yang menyangkut perempuan Asmat dan menuliskannya dalam bentuk karya sastra (novel) sehingga diharapkan mampu mengubah perspektif masyarakat tentang latar belakang masalah kesehatan di Papua yang kompleks.

  4. Aktivitas dan keterkaitan pada sasaran

     Melakukan interaksi, mengamati, dan wawancara pada pihak-pihak terkait serta studi literatur.

  5. Latar belakang dan demografi pelaku proyek

     Pemimpin proyek adalah penulis perempuan (28 tahun), latar belakang pendidikan kedokteran, dengan pengalaman menulis karya fiksi sejak 6 tahun, dan menulis novel sejak 3 tahun. Tertarik untuk menuliskan kebudayaan dan kearifan lokal suatu daerah serta peristiwa-peristiwa penting yang berkaitan dengan tragedi kemanusiaan.

  6. Demografi kelompok target:

        - Riset: Masyarakat Asmat setempat: terutama keluarga korban, para perempuan, kepala suku, peneliti, pejabat, dan aktivis.

        - Novel: Mahasiswa, pembaca, penulis, dan komunitas terkait

  7. Hasil yang diharapkan dan indikator keberhasilan

     Terbit sebuah novel yang dapat dibaca banyak kalangan, dapat dijadikan arsip dalam bentuk karya sastra, dan dapat digunakan bahan rujukan/diskusi untuk mengenal Indonesia timur khususnya kebudayaan Asmat, Papua.

  8. Indikator keberhasilan

     1. Terbitnya novel bertema gizi buruk dan wabah campak serta kebudayaan Asmat tahun (2019-2020)

     2. Terdistribusikan novel (dalam bentuk cetak) sebanyak 1000 eksemplar (satu tahun atau dua kali pelaporan royalti) atau 200 unduhan (bila disediakan dalam media online)

     3. Publikasi sepuluh ulasan/resensi di media cetak/online

  9. Durasi Waktu Aktivitas dilaksanakan

     1-2 bulan

  10. Total kebutuhan dana

      50.000.000

  11. Dana yang diminta dari Ford Foundation melalui Cipta Media Ekspresi

      50.000.000

  12. Sumber dana lainnya

      Tidak ada

  13. Kontribusi perorangan

         - Melakukan riset, menulis, dan menerbitkan

         - Mengadakan peluncuran buku dan diskusi dengan komunitas-komunitas terkait.

  14. Kontribusi kelompok target

         - Kelompok Target Riset:

              - Dapat bekerja sama dalam hal riset dengan pelaku proyek: kooperatif dalam interaksi, wawancara, dan proses dokumentasi.

         - Kelompok Target Novel:

              - Membeli novel (cetak) atau mengunduh (gratis)

              - Membaca, membagikan, mengadaptasi, mengulas, dan mendiskusikan karya.
