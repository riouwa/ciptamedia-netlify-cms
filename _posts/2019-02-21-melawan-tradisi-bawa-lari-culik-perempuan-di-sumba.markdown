---
title: 'Melawan Tradisi Bawa Lari (Culik) Perempuan di Sumba '
date: 2019-02-10T05:58:03.417Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - CME
  - perempuan
  - Sumba
  - tradisi
author: Martha Hebi
comments: true
img: /uploads/blog/melawan-tradisi-bawa-lari.jpg
---
![](/uploads/blog/melawan-tradisi-bawa-lari.jpg)



Istilah bawa lari perempuan cukup dikenal dalam tradisi Sumba. Ini adalah jalan pintas untuk memperistri seorang perempuan idaman. Anda ingin membayangkan caranya?



Seorang perempuan lajang dengan beberapa rekannya sedang ke pasar mingguan di kampungnya. Tiba-tiba segerombolan laki-laki yang tidak dikenal menangkap perempuan lajang tersebut, mendekapnya dan memuatnya di kuda atau motor atau bemo (mobil) yang sudah disiapkan, kemudian dibawa pergi begitu saja. Meskipun perempuan (korban) itu berteriak minta tolong, meronta-ronta, menjerit, tidak akan ada yang melihat itu sebagai sebuah keanehan atau kejadian yang membutuhkan pertolongan atau kekerasan. Biasanya orang bilang, oh ada yang bawa lari perempuan. Biasanya akan ada urusan adat selanjutnya dimana keluarga lelaki akan mengutus juru bicara (wunang ke keluarga perempuan untuk memberi tahu bahwa anak perempuan mereka ada di rumah keluarga laki-laki, si penculik tadi). 



Hingga saat ini, hal tersebut masih terjadi di beberapa wilayah di Sumba. Di kalangan masayarakat Sumba Tengah ada 2 jenis bawa lari perempuan yaitu yappa marada dan palai ngidi mawini. Gambaran kejadian di atas digolongkan sebagai yappa marada. 



Sedangkan palai ngidi mawini biasanya perempuan dan laki-laki yang saling cinta namun ditentang oleh orang tua dan keluarga, nah sang lelaki mengajak perempuan untuk pergi dari rumah dan mereka akan pergi dari rumah sang perempuan. Biasanya juga akan ada utusan dari keluarga laki-laki yang ke keluarga perempuan untuk memberitahukan keberadaan anak perempuan mereka.



Kedua gambaran kejadian di atas tidak digolongkan sebagai kekerasan terhadap perempuan, bahkan pasca berlakunya UU No. 23 tahun 2004,  tentang Penghapusan Kekerasan dalam Rumah Tangga, UU No. 39 tahun 199 tentang Hak Asasi Manusia dan sejumlah kebijakan lainnya tentang perlindungan perempuan dan anak. Hingga tahun 2018 masih ada kejadian tradisi “penculikan” yappa marada. Bahkan atas nama tradisi Sumba, tahun 2017 terjadi penculikan anak di bawah umur, anak SD berumur 13 tahun. 



Salomi Rambu Iru (64 tahun), perempuan petani yang juga aktif sebagai pegiat sosial di Forum Perempuan Sumba (Foremba) Sumba Tengah melawan tradisi ini. Dia aktif mengkampanyekan anti kekerasan terhadap perempuan dan anak, mengajak warga untuk melindungi perempuan dan anak. Perempuan ini menggandeng pihak kepolisian untuk menghentikan penculikan perempuan dan anak. Banyak kasus yappa marada yang berhasil digagalkan bahkan dibawa ke jalur hukum. Apakah Mama Salomi mendapatkan tantangan, ancaman? Bagaimana caranya? Perempuan seperti apa saja yang bisa menjadi korban yappa marada?



Silakan baca kelanjutannya dalam Buku Perempuan (Tidak) Biasa di Sumba Era 1965-1998.



Ikuti juga penuturan perempuan sarjana korban yappa marada yang berhasil digagalkan berkat kerja sama keluarga, Mama Salomi dan pihak Kepolisian Sumba Tengah.
