---
title: '  Kisah di Balik Pentas Seorang Penari Kehidupan'
date: 2018-12-12T04:22:45.355Z
categories:
  - CME
  - Berita
  - 11 Ibu 11 Panggung 11 Kisah
tags:
  - teater
  - kisah perempuan
author: Kadek Sonia Piscayanti
comments: true
img: /uploads/blog/img_7216.jpg
---
Pentas ke 10 dalam rangkaian project 11 Ibu 11 Panggung 11 Kisah yang merupakan hibah Ford Foundation melalui Cipta Media Ekspresi karya sutradara dan penulis naskah Kadek Sonia Piscayanti sudah dilaksanakan pada 11 Desember malam di rumah sang penari, Ibu Cening Liadi di Jalan Pulau Seribu Gang Bhisma Penarungan Singaraja. Sang penari yang juga seorang Ibu, menceritakan kisahnya dengan jujur. Diawali dengan tari Sekar Jagat, Cening Liadi memulai pentasnya dengan percaya diri. Tarian yang ia bawakan sangat lembut dan menyentuh. Adegan selanjutnya adalah dia menembang dengan gaya menyanyi drama tari arja, menjelaskan arti namanya. Penonton terhanyut. 



Adegan demi adegan mengalir dan terjalin seperti sebuah tarian yang bercerita. Sutradara Kadek Sonia Piscayanti menggarap pentas Cening dengan berbeda, seperti sengaja menonjolkan keunikan khas Cening yaitu penari tradisional yang akrab dengan pakem tradisi. Pentas ini seperti tak lepas dari pola teater tradisi arja yang memadukan kisah dengan tarian dan nyanyian.  Bahkan cara bergerak, berpindah tempat dan menjalankan cerita dibawakan dengan prinsip bloking yang ketat dan terukur. Pentas ini seperti sebuah pondasi gerak tari, yaitu agem yang tekek (kokoh), tidak banyak gerak yang boros dan lepas. Seperti agem pula, Cening bergerak dengan kokoh, mengisahkan bahwa tantangan terhadap keluarganya terutama kepada anaknya yang bertubi-tubi telah membuatnya kokoh. Kisah Cening mengalir dengan lambat namun pasti, mengambil klimaks ketika Cening hampir putus asa saat anaknya hampir meninggal. Hanya dengan keyakinan saja, ia berdoa, agar anaknya disembuhkan. Dengan berbagai perjuangan, anaknya sembuh bahkan lulus ujian sarjana dengan predikat cumlaude. 



![](/uploads/blog/img_7216.jpg)



Pelajaran hidup yang begitu berat membuatnya sadar untuk lebih banyak mendekatkan diri pada Tuhan dan memberikan pelayanan sebaik-baiknya. Ia pun mendirikan Rumah Yadnya Tri Laksmi untuk berbagi secara gratis dengan perempuan Bali berupa kursus menari, megambel, mejahitan, dan menyanyi kidung kerohanian. Hal lain tanda ia berbagi adalah dengan mendirikan koperasi untuk melayani dan memperkuat perekonomian keluarga sekitarnya yang membutuhkan. 



Pentas diakhiri dengan adegan menari bersama aktor lain di project 11 ibu yang hadir, mereka menari bersama dan belajar bersama. Tampak jelas bahwa Cening Liadi adalah penari kehidupan yang sesungguhnya.



Dalam diskusi setelah pementasan, Cening mengaku ini adalah pengalaman pertamanya pentas teater. Ia sangat bahagia mendapat pengalaman baru, sebab pentas teater sangat berbeda dengan pentas tari. Pentas tari dilakukan tanpa ‘berbunyi’, tanpa bertutur, namun pentas teater dilakukan dengan bertutur, bahkan juga sambil menyanyi dan menari. Sehingga kompleksitas kesulitan pentasnya lebih tinggi daripada pentas tari. Dia menyebutkan bahwa sutradara membuatnya terpaksa bercerita, padahal ia adalah penari yang pendiam dan tertutup. Sutradara pun mengakui bahwa Cening adalah pribadi yang tertutup namun dengan semangat berbagi yang tinggi akhirnya dia mampu meyakinkan Cening bahwa kisahnya layak dibagi karena menginspirasi.



Audiens yang hadir sangat terpukau dengan penampilan Cening Liadi. Salah satunya Dian, mahasiswa Pendidikan Bahasa Inggris Undiksha, menurut Dian pentas Bu Cening sangat memukau dan menyentuh. Kisahnya menginspirasi dan memotivasi. Demikian pula komentar Ketua Wargas Buleleng, Mami Sisca yang mengatakan bahwa pentas ini sangat spesial dan menonjolkan keunikan Bu Cening. Seperti komentarnya pada tiap pentas project 11 Ibu, masing-masing ibu memiliki ciri khas keunikan yang berbeda satu dengan yang lainnya. Pentas ini diharapkan menjadi pembelajaran bagi perempuan lainnya dengan kompleksitas isu yang dihadapi masing-masing.
