---
title: Bangsawan yang Merdekakan Hambanya
date: 2019-02-09T08:07:13.194Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - Perempuan Tidak Biasa
  - CME
  - hamba
  - Sumba
author: Martha Hebi
comments: true
img: /uploads/blog/bangsawan-yang-merdekakan-hambanya.jpg
---
Bangsawan yang Merdekakan Hambanya



![](/uploads/blog/bangsawan-yang-merdekakan-hambanya.jpg)



Dalam sistem perbudakan tradisional Sumba, memiliki (menguasai) banyak hamba atau budak (bahasa Kambera : tau la umma atau ata ) dianggap akan membuat status sosial semakin “berkelas”. Kepemilikan hamba secara turun temurun layaknya “genetika sosial” ini terjadi di Sumba Timur. Bahkan ada yang dengan sengaja membeli(s) hamba dengan jalur tradisional hanya untuk menaikkan kelas sosialnya.  



Di Sumba, belis adalah bagian dari tradisi perkawinan dimana keluarga laki-laki akan menyerahkan sejumlah ternak kepada pihak perempuan sebagai mas kawin atau mahar. Balasannya, keluarga perempuan akan menyerahkan sejumlah kain, sarung, babi. Pengantin perempuan akan mengenakan (membawa serta) perhiasan muti anahida, anting dan kalung  emas, sarung tenunan indah dan mahal, dan tak jarang ada keluarga yang memberikan kuda tunggangan bagi anak perempuannya. Di masa sekarang, kuda tunggangan bisa berupa motor atau mobil.



Di satu sisi ada juga yang dinamakan belis hamba (bukan belis kawin mawin). Belis hamba dilakukan oleh keluarga bangsawan dengan menyerahkan sejumlah ternak kepada keluarga tertentu yang akan menyerahkan hamba perempuannya atau anak perempuannya. Anak perempuan atau hamba perempuan ini akan menjadi hamba dari keluarga pembelis. Kelak di kemudian hari, hamba perempuan atau anak perempuan ini akan dinikahkan dengan hamba laki-laki keluarga bangsawan yang membelis tadi. Banyak terjadi perempuan yang dibelis  masih di bawah umur. Di tahun 2004, kami menemukan anak berusia 12 tahun dari keluarga biasa dibelis oleh keluarga bangsawan di Kecamatan Nggaha Ori Angu, di tahun 2006 anak berusia 13 tahun dibelis oleh bangsawan di Kecamatan Kahaungu Eti.



Apakah ini kisah perbudakan masa lalu yang sudah menjadi kenangan? Tidak! Masih berlangsung hingga saat ini. Di Sumba khususnya Sumba Timur ada tiga jenis hamba yaitu ata ndai, ata ngandi, ata pa kei. Ata ndai adalah hamba yang dimiliki keluarga para maramba (bangsawan) secara turun temurun. Jika ada seorang laki-laki  bernama Umbu nai Luta itu artinya laki-laki ini adalah seorang bangsawan yang memiliki hamba (pribadi) bernama Luta. Umbu nai bermakna umbunya atau tuannya. Jika perempuan bernama Rambu nai Lemba, berarti perempuan ini adalah bangsawan yang memiliki hamba (pribadi) bernama Lemba. Ada keluarga bangsawan yang setiap anaknya memiliki masing-masing hamba pribadi. Ata ngandi adalah hamba yang dibawa oleh perempuan bangsawan saat menikah dan pindah ke rumah suami. Hamba ini akan selalu bersama tuannya, laksana asisten pribadi yang dapat dimintai untuk melakukan pekerjaan-pekerjaan rumah tangga. Namanya bisa juga melekat pada nama tuannya. Misalnya Rambu nai Lemba.



Sedangkan ata pa’kei  adalah hamba yang dibelis, seperti dijelaskan di atas. Dalam bahasa Kambera arti harafiah dari ata pa’kei adalah hamba yang dibeli. Ata berarti hamba, pa’kei berarti dibeli.



Menjadi hamba bukanlah pilihan. Menjadi hamba artinya kemerdekaan ada di tangan tuannya. Kesehatan, pendidikan, kepemilikan lahan, relasi dengan pihak lain, menikah, ya masa depannya semua ada di tangan tuannya.



Misalnya mau sekolah? Tuanlah yang mengatur, ya atau tidak. Kalaupun sekolah, ada yang hanya sekedar menjaga tuannya yang sedang bersekolah, membawakan bukunya, menuliskan tugas sekolah tuannya, melakukan apa yang diperintahkan tuannya. Jika tuannya berhenti sekolah, maka sang hamba pun berhenti sekolah. Jika tuannya melanjutkan sekolah, sang hamba belum tentu bisa melanjutkan sekolahnya.  



Apakah ada hamba yang bisa bersekolah bahkan lulus SMA atau mengecap pendidikan tinggi? Ya, ada. Tidak semua kisah pendidikan para hamba kelabu, seperti yang terjadi dalam keluarga Mama Tamu Rambu Margaretha dari Kampung Raja Pariliu. Mereka bisa menempuh pendidikan, bahkan diwajibkan bersekolah. 



Perempuan yang satu ini sadar sepenuhnya bahwa pendidikan adalah jalan pembebasan. Namun dia malah menyekolahkan hambanya. Dia tahu betul bahwa suatu waktu, dengan bermodalkan pendidikan yang dikecap oleh hambanya, dia akan ditinggalkan. Ini juga berarti berkuranglah “kuasanya”. Mengapa dia memilih jalan ini di saat banyak orang berlomba menaikkan status sosial dengan memiliki banyak hamba?



Apa kata gadis tau la uma Mama Tambu Rambu Margaretha yang sedang kuliah saat ini?



Tunggu kisah selengkapnya di Buku Perempuan (Tidak) Biasa di Sumba Era 1965-1998.
