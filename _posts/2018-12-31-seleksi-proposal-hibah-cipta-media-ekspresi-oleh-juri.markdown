---
title: Seleksi Proposal Hibah Cipta Media Ekspresi oleh Juri
date: 2018-04-02T06:04:35.805Z
categories:
  - CME
  - berita
tags:
  - rapat
  - seleksi
  - hibah
author: Ivonne Kristiani
comments: true
img: /uploads/blog/img_2305.jpg
---
![](/uploads/blog/img_2305.jpg)

Beberapa hari setelah pendaftaran hibah Cipta Media Ekspresi ditutup, para juri memulai kerja kerasnya untuk membaca satu per satu, mengevaluasi, serta menyeleksi ratusan proposal yang masuk. Seleksi tahap awal ini berlangsung pada 2-3 April 2018 di Hotel All Seasons Jakarta.

Walaupun proyek-proyek yang diajukan banyak yang menarik dan baik namun pada akhirnya hanya beberapa orang yang dapat memperoleh hibah ini. Para juri pun harus menentukan pilihan yang sulit untuk menggugurkan proposal yang satu dan meluluskan proposal yang lainnya karena keterbatasan dana yang akan diberikan. Juri pun menentukan kriteria dan prioritas untuk meloloskan proposal yang masuk. Banyak pertimbangan yang diberikan oleh masing-masing juri yang memiliki sudut pandang beragam dari latar belakangnya yang berbeda-beda.

Untuk memudahkan kerja para juri yang terbatas dalam menyeleksi proposal yang mencapai angka seribu ini, juri kemudian dibagi menjadi dua kelompok. Seluruh proposal yang masuk pun dibagi menjadi dua untuk dinilai oleh kedua kelompok juri. Diusahakan agar juri menilai proposal yang relevan. Pada akhirnya, kedua kelompok juri ini berkumpul kembali untuk mengambil keputusan akan proposal mana yang ditolak, mana yang diterima, dan mana yang akan diwawancarai demi mendapatkan informasi yang lebih lengkap. Wawancara akan diadakan baik melalui telepon ataupun Skype juga melalui tatap langsung dengan mendatangkan calon penerima hibah ke Jakarta.
