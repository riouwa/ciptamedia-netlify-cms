---
title: 'Bongkar: Siasat Feminis dalam Seni dan Budaya'
date: 2019-05-03T03:30:34.731Z
categories:
  - CME
  - kajian
tags:
  - perempuan
  - feminis
  - seni
  - budaya
  - feminisme
author: Intan Paramaditha
comments: true
img: /static/img/landing/akses.png
---
**_Tahun 2018 adalah tahun perebutan ruang dalam seni dan budaya, dengan menajamnya tegangan antara praktik ideologi dominan dengan beragam kelompok yang menggugat, mencari jalan tikus, dan menerobos masuk. Kabar baiknya: perempuan tak henti bersiasat, dan ini akan terus berlanjut._**

Dua puluh tahun Reformasi mengingatkan kita pada kembali maraknya gerakan perempuan setelah penyingkiran dan depolitisasi selama rezim Orde Baru, namun bias gender masih terasa kuat di mana-mana, termasuk di ranah seni dan budaya. Pada tahun 2018, forum kesenian, diskusi kebudayaan, maupun ajang perlombaan dan pemberian penghargaan masih saja diwarnai oleh “panel laki semua” alias all-male panels yang melanggengkan dominasi laki-laki sebagai suara otoritatif dan pembuat keputusan.

Sebagai contoh, tidak banyak nama perempuan beredar dalam nominasi penghargaan sastra dua-tiga tahun terakhir. Festival dan acara “_indie_” yang kita harapkan menjadi alternatif bagi konstruksi selera dominan juga kerap abai terhadap pentingnya suara perempuan. Bahkan dalam Kongres Kebudayaan di akhir tahun lalu (5-9 Desember 2018) cukup jarang kita temukan panel yang melibatkan perempuan sebagai pembicara atau ahli, apalagi yang menimbang perspektif feminis.

Di sisi lain, saya beruntung bisa terlibat dalam dua proyek aktivisme kultural dan bekerja bersama para perempuan yang terus melacak kemungkinan intervensi ruang. Di awal tahun lalu saya menjadi salah satu juri [Cipta Media Ekspresi](https://magdalene.co/story/hibah-seni-budaya-rp-35m-tersedia-untuk-perempuan-di-indonesia), hibah untuk perempuan pelaku kebudayaan di segala bidang seni. Hibah ini kemudian diikuti oleh program pendampingan, program penajaman karya dan kerangka pikir melalui Sekolah Pemikiran Perempuan (Yogyakarta 17-19 Oktober 2018), dan beberapa inisiatif lain yang berlanjut tahun ini.

Dalam skala yang lebih kecil, saya dan rekan penulis Lily Yulianti Farid mengadakan kelas menulis kritik sastra dan film [Period](https://jurnalruang.com/read/1547133046-intan-paramaditha-hegemoni-harus-dikritik) (17-18 Desember 2018) sebagai langkah turut membangun ekosistem seni yang lebih kritis, beragam, dan setara. Diajar oleh Melani Budianta dan Lisabona Rahman, kelas ini terbuka untuk siapa saja--perempuan, laki-laki, dan lainnya--namun menekankan pada perspektif feminis dalam proses kuratorial bahan bacaan maupun karya untuk ditelaah.

Pengalaman saya selama setahun belakangan menunjukkan bahwa: 1) Kita tidak kekurangan perempuan di bidang seni dan budaya; 2) Pengabaian suara perempuan maupun perspektif feminis dari ruang-ruang dominan dan posisi-posisi strategis terus terjadi; 3) Terlepas dari sempitnya ruang, perempuan terus bersiasat. Temuan ini bukan hal baru, dan kedua proyek yang saya sebut hanya segelintir dari bermacam inisiatif kolektif perempuan dalam menyikapi dominasi patriarki di bidang seni dan budaya. Namun saya merasa harus menuliskannya, lagi dan lagi.

Dalam Simposium Pertunjukan di Indonesia yang diadakan di Yogyakarta tahun 2009, sejumlah seniman dan aktivis perempuan dalam FGD Gender dan Seksualitas telah memetakan hal ini serta mengadakan sejumlah pertemuan untuk menentukan langkah selanjutnya. Sepuluh tahun kemudian, kita masih dihadapkan oleh banyak persoalan yang sama.

Empat puluh perempuan yang menerima Hibah Cipta Media Ekspresi mewakili Indonesia Barat, Tengah, dan Timur dengan beragam latar belakang seperti sinden, rocker, santri, buruh, ibu rumah tangga, dan tokoh adat. Sepanjang 2018, kabar tentang penerima hibah dalam berbagai fase proses kreatif, baik riset, penciptaan, atau pertunjukan terus mengalir, dan linimasa saya dipenuhi oleh nama-nama pencipta dan peneliti di bidang sastra, teater, tari, film, seni rupa, maupun musik. Mereka semua adalah perempuan atau mengidentifikasikan diri sebagai perempuan. Ini segera meruntuhkan alasan kurangnya partisipasi perempuan yang menjadi legitimasi dominasi laki-laki di forum-forum seni budaya. Pada kenyataannya, perempuan ada, berpikir, mencipta, membuat keputusan, bekerja sama.

Saya mencatat berbagai masalah perempuan dalam seni dan budaya serta ragam siasat yang muncul saat saya berkolaborasi dengan aktivis perempuan Andy Yentriyani dan rekan-rekan di Cipta Media Ekspresi dalam merancang program Sekolah Pemikiran Perempuan. Persoalan utama ranah seni budaya bukan absennya perempuan, tetapi ketiadaan kemampuan untuk mengenali dan menghargai karya-karya mereka. Maka yang seharusnya sejak lama dilakukan adalah membongkar ideologi dan praktik serangkaian institusi yang membatasi dan bahkan menihilkan kerja perempuan. Bagaimana institusi keluarga, pernikahan, dan agama menjadi rambu-rambu bagi perempuan untuk berkarya? Bagaimana “gawang-gawang” kebudayaan--sekolah, komunitas seni, kritikus, dewan kesenian--terus menentukan akses dan standar tanpa menimbang perspektif feminis? 

Selama setahun terakhir saya menyaksikan navigasi perempuan dalam bermacam rupa. Hadirnya pengajar dan narasumber perempuan dalam lokakarya Period, misalnya, bisa dilihat sebagai siasat cepat yang merespons “panel laki”. Namun ada juga siasat yang butuh waktu bertahun-tahun. Bagaimana menciptakan dan memelihara ruang yang memberikan pengakuan pada karya-karya perempuan? Bagaimana terus memastikan ketampakan seniman maupun intelektual perempuan di ruang publik?

Tulisan ini menyodorkan beberapa siasat feminis dalam seni dan budaya, sebuah catatan pengingat atau sontekan yang bisa dilihat terus-menerus sebab kita mudah lupa dan dilupakan. Di dalamnya saya menyebut beragam inisiatif yang digagas oleh perempuan. Saya tidak menyebutnya “langkah awal” sebab, seperti yang saya singgung di atas, kolektif perempuan telah lama bersiasat.

<br>

### Bongkar: Kata kunci pemikiran perempuan

Dalam Sekolah Pemikiran Perempuan Cipta Media Ekspresi di Yogyakarta, kami mendiskusikan beberapa kata kunci untuk ditelaah bersama, termasuk di dalamnya “tubuh,” “politik,” dan “kuasa.” Ada satu kata kunci penting yang perlu kita garisbawahi sebagai kata kunci feminis: “bongkar”. Hal pertama yang perlu kita lakukan, baik sebagai pencipta maupun pembaca atau penonton, adalah membongkar asumsi-asumsi di kepala kita. Semangat membongkar adalah semangat mengajukan pertanyaan ketimbang membuat pernyataan.

[Proyek seni rupa Ika Vantiani](https://magdalene.co/news-1857-seniman-ika-vantiani-pertanyakan-definisi-kata-perempuan.html) merupakan contoh penting bahwa perempuan bisa dan harus membongkar konsep yang dianggap lazim dan diterima begitu saja. Dalam pameran dan instalasi “Perempuan dan Kamus Besar Bahasa Indonesia” tahun lalu, Ika mempertanyakan definisi kata “perempuan” dalam KBBI dan menemukan bahwa kata “perempuan” tidak pernah keluar dari aspek reproduktif (“dapat menstruasi, hamil, melahirkan anak, dan menyusui”) maupun seksual (“perempuan lacur,” “perempuan nakal,” perempuan simpanan”). Mengapa tidak ada contoh seperti “presiden perempuan,” misalnya? Temuan Ika menunjukkan bagaimana sudut pandang patriarkal beroperasi dalam praktik legitimasi makna.

Perempuan selalu dididik untuk menyusun atau menata yang sudah ada ketimbang mencipta. Ia dilatih untuk membersihkan rumah, merapikan kamar, membangun rumah tangga, memastikan segala unsur dalam hidupnya harmonis. Sering kali dalam proses menyusun ia tidak dianjurkan bertanya sebab pertanyaan, gugatan, dan segala aktivitas membongkar berpotensi mengganggu status quo. Karena itu, sudah seharusnya perempuan mengklaim kata “bongkar” dan menjadikannya bagian dari transformasi feminis.

<br>

### Mengakui karya perempuan sebagai etika feminis

Siasat untuk mendukung perempuan di bidang seni dan budaya adalah menjadikan upaya mengenali dan mengapresiasi karya-karya perempuan sebagai bagian dari etika feminis. Siapa pun kita, baik sebagai kreator, pembaca, penonton, kritikus, atau aktivis, bisa memulainya dengan bertanya pada diri sendiri: Berapa banyak tulisan perempuan yang kita baca? Siapa sutradara, koreografer, atau komposer perempuan yang kita kenal? Jika nama perempuan sulit ditemukan, kita bertanggung jawab untuk bertanya dan melacak.

Dalam Sekolah Pemikiran Perempuan, kami mengadakan “klub cerap”, sebuah forum diskusi untuk menajamkan keahlian analisis terhadap karya. Dipandu oleh seniman Naomi Srikandi dan kritikus film/pelaku pengarsipan film Lisabona Rahman, kami membahas dua karya seniman perempuan, Melati Suryodarmo dan Mouly Surya. Dalam kelas kritik sastra dan film Period, peserta diminta menulis resensi atas karya-karya yang telah dipilih oleh penyelenggara dan pengajar. Sebagian besar karya yang ada dalam daftar adalah karya penulis dan sutradara perempuan.

Kenapa mesti bawa-bawa identitas dan tidak melihat seniman sebagai seniman saja, bukan laki-laki atau perempuan? Sebab tidak ada karya yang bebas nilai; karya apa pun, terlepas dari apakah ia mengklaim adanya agenda politik atau tidak, adalah produk ideologi. Pada kenyataannya, meski kita tahu beberapa nama perempuan yang menonjol dan (di)istimewa(kan), sebagian besar karya perempuan sering kali tidak disikapi secara serius. Ada bias gender dalam institusionalisasi standar estetika, juga bias dalam menilai karya perempuan berdasarkan stereotip-stereotip tertentu.

Salah satu siasat yang perlu terus dilakukan adalah menyikapi forum diskusi sebagai etalase pemikiran perempuan. Etalase di sini kita lepaskan dari asosiasi feminin, semisal manekin dengan gaun modis, dan kita reka ulang sebagai tempat memamerkan gagasan kreator dan intelektual perempuan. Ruang etalase, balik yang sementara maupun permanen, perlu terus kita ciptakan.

<br>

### Menelusuri metodologi feminis

Mempelajari metodologi feminis akan membantu menajamkan praktik penciptaan, penelitian, maupun aktivisme. Tentu ada bermacam-macam metodologi feminis, namun ada dua aspek yang ingin saya tekankan terkait seni dan budaya: 1) Kesadaran atas hubungan antara kuasa dengan institusionalisasi nilai sebagaimana yang kita lihat dalam praktik penyusunan kanon; 2) Kesadaran bahwa historiografi tidak bersifat netral.

Kurator Alia Swastika berbagi cerita soal seorang perupa perempuan yang tidak dianggap cukup baik di lanskap seni rupa arus utama. Ketimbang lekas-lekas berkompromi dengan penolakan ini, Alia sebagai kurator feminis melacak dari mana datangnya standar estetika tertentu, siapa yang menetapkannya, dan siapa yang dimenangkan dan disingkirkan berdasarkan standar tersebut.

Akademisi Heidi Arbuckle, dalam penelitiannya tentang karya-karya pelukis Emiria Soenassa, menemukan bahwa data tentang Emiria lebih banyak ia temukan di majalah perempuan ketimbang di majalah atau forum “seni tinggi”. Oleh karena itu Heidi menggunakan metodologi feminis yang melacak peran perempuan dalam sejarah melalui jalur-jalur alternatif seperti ingatan, tuturan, karikatur, anekdot, dan juga kolom gosip.

Apa yang ditulis, diapresiasi, dan dianggap pantas masuk ke dalam kanon adalah perihal struktur dan relasi kuasa. Kanonisasi karya sastra Barat telah lama dikritik karena terpusat pada karya laki-laki kulit putih. Maka cukup mengherankan bila, setelah muncul banyak debat tentang politik penyingkiran, panel soal kanonisasi sastra dalam Kongres Kebudayaan tahun lalu sama sekali tidak melibatkan narasumber perempuan. Wacana pentingnya kanon, juga kritik atas gagasan tentang kanon, tak akan beranjak ke mana-mana tanpa menimbang perspektif perempuan.

Institusionalisasi apa yang bernilai dan yang tidak bukanlah proses netral, dan demikian pula halnya dengan historiografi. Oleh karenanya, dalam esai [A Feminist Trajectory of Literary Influences](https://www.insideindonesia.org/a-feminist-trajectory-of-literary-influences), saya menggarisbawahi pentingnya menelusuri genealogi pemikiran perempuan. Jika wacana publik tentang kebudayaan kerap merujuk pada Pramoedya Ananta Toer, Umar Kayam, atau Goenawan Mohamad, mengapa kita tidak menggunakan konsep-konsep yang digagas oleh intelektual seperti Saparinah Sadli, Toeti Heraty, Melani Budianta, dan Julia Suryakusuma? Pencatatan sejarah, berikut praktik merujuk dan mengutip, adalah politik pengakuan dan penyingkiran.

Salah satu dari sekian banyak intervensi yang patut dicatat tahun lalu adalah Ruang Perempuan dan Tulisan. Proyek yang digagas Dewi Noviami dan Dewi Kharisma Michellia, serta melibatkan sejumlah penulis perempuan muda ini, mencoba menelaah dan mendokumentasikan kiprah penulis perempuan dari masa terdahulu. Sebagaimana kita duga, buah pikir dan nama mereka sering tersingkir dan terhapus dari sejarah.

<br>

### Menggugat hubungan perempuan dan estetika

“Bagaimana karya-karya perempuan bisa melampaui sifat personal dan otobiografis?” Pertanyaan ini pernah dilontarkan seorang tokoh sastra laki-laki dalam sebuah forum yang mendiskusikan karya-karya penulis perempuan. Seorang penulis perempuan muda mengritik pertanyaan ini dan menunjuk bahwa banyak karya laki-laki yang bersifat otobiografis namun dianggap memiliki nilai estetika tinggi.

Masih terkait dengan kata kunci bongkar, kita perlu terus menggugat asumsi di balik pertanyaan-pertanyaan yang menyederhanakan hubungan perempuan dan estetika. Apa benar karya perempuan bersifat personal dan otobiografis ketimbang eksperimental? Apa benar karya yang menyodorkan politik tertentu, seperti agenda feminis misalnya, cenderung mengabaikan estetika?

Dalam [diskusi mengenang karya dan kiprah Nh. Dini ](https://edukasi.kompas.com/read/2018/12/18/22344901/bbj-gelar-bincang-karya-nh-dini-pada-sebuah-novel)di Bentara Budaya Jakarta Desember lalu, saya kembali diingatkan pada asumsi-asumsi yang dilekatkan pada penulis perempuan. Di tahun 80-an, karya perempuan dianggap hanya memedulikan isu-isu feminin dan domestik (semisal persoalan asmara dan rumah tangga) dan tidak menyentuh gagasan-gagasan besar seperti bangsa dan nasionalisme. Padahal, jika kita membaca karya Nh. Dini secara mendalam, kita akan melihat gagasan kosmopolitanisme yang berhubungan sekaligus bersitegang dengan gagasan tentang kebangsaan.

Nh. Dini juga menyampaikan kritik atas hubungan Timur-Barat yang timpang di ranah global. Oleh karena itu kita perlu membongkar cara pandang kita sendiri. Sudahkah kita melihat berbagai kemungkinan interpretasi karya? Apakah cara kita memandang karya perempuan telah terlatih sedemikian rupa sehingga tanpa sadar kita memasukkan karya-karya ini ke dalam kotak-kotak yang telah ada sebelumnya?

<br>

### Mengenali ruang-ruang yang digagas perempuan

Kita perlu mengenali dan mengakui inisiatif perempuan menciptakan ruang-ruang alternatif. Fokus pada kejeniusan sastrawan, sutradara, koreografer, dan sebagainya pada akhirnya menjebak kita dalam logika romantik maskulin yang melepaskan infrastruktur dari konsep auteur. Padahal infrastruktur tersebut justru terdiri dari “kerja-kerja reproduktif” tak terlihat yang--sebagaimana diungkapkan Cecil Mariani dalam presentasinya--lebih banyak dilakukan oleh perempuan.

Keahlian perempuan menata dan menjalankan proyek kerap dianggap sebagai keahlian administratif, padahal keahlian ini juga yang melahirkan produser-penggagas hebat pasca 2000-an seperti Mira Lesmana, Shanty Harmayn, Nia Dinata, dan Meiske Taurisia. Perempuan-perempuan ini bukan cuma mereka ulang keproduseran tetapi juga, dalam kapasitas yang mereka miliki, membuka ruang bagi wacana tentang gender.

Dalam 10 tahun terakhir, saya menyaksikan begitu banyak inisiatif luar biasa yang dimotori oleh perempuan. Magdalene sebagai media berperspektif feminis terus menghadirkan suara perempuan yang ahli di bidangnya, dari bidang agama hingga hukum, sekaligus memberikan wadah bagi perempuan-perempuan muda untuk mengartikulasikan gagasan dan berdialog. Di luar kiprah individualnya sebagai seniman, Ika Vantiani merupakan salah satu penggagas kolektif [Mari Jeung Rebut Kembali](https://magdalene.co/news-727-campaign-aims-to-reclaim-women%E2%80%99s-authority-over-their-bodies-.html) yang menyuarakan pentingnya merebut otoritas tubuh perempuan. [House of the Unsilenced](https://magdalene.co/news-1853-house-of-the-unsilenced-giving-voices-to-sexual-survivors-through-arts.html) yang digagas Eliza Vitri Handayani kembali menggarisbawahi gentingnya isu kekerasan seksual melalui kolaborasi seniman dan penyintas.

Upaya membangun jaringan seniman perempuan, khususnya di luar Jawa dan Sumatera, terus dilakukan oleh Olin Monteiro lewat Arts for Women dan Penerbitan Buku-Buku Perempuan. Lalu pada bulan Maret mendatang, kolektif Perempuan Lintas Batas (Peretas) mengadakan pertemuan di Poso, Sulawesi Tengah, yang melibatkan 50 perempuan pekerja seni budaya untuk berdiskusi, menguatkan jaringan, dan menggali kemungkinan kolaborasi.

Contoh-contoh ini tentunya hanya sedikit dari begitu banyaknya ruang alternatif yang diupayakan perempuan. Karena sebagian besar inisiatif ini bersifat mandiri (do it yourself atau DIY) dengan pendanaan yang tak selalu pasti, keberlangsungan kerap menjadi tantangan besar. Namun keberadaannya mengingatkan kita untuk, pertama, mengapresiasi keberanian, sikap politik, dan kreativitas dalam ragam siasat perempuan; kedua, memahami bahwa ada banyak jalur lain di luar yang dominan.

Dalam diskusi publik Sekolah Pemikiran Perempuan, seorang peserta bertanya apakah ia harus mengubah diri dan karyanya agar bisa diterima orang banyak. Mentor sekaligus Ketua Juri Cipta Media Ekspresi, Lisabona Rahman menjawab, “Sebaiknya kita tidak berkompromi dalam soal gagasan. Jika satu pintu tertutup, cari pintu lain.”

Jika kantung-kantung budaya tertentu tidak berpihak pada seorang seniman perempuan, ia tahu bahwa ada beragam kolektif perempuan yang siap mendukungnya di jalur-jalur alternatif. Barangkali apa yang ia gagas memang lebih cocok berada di wilayah counterpublic, tempat wacana tandingan diputar, dan di sanalah sebagian besar proyek aktivisme kultural feminis berada.

<br>

### Memperjuangkan yang jamak

Berbagai siasat perempuan sepatutnya bertumpu pada orientasi feminis menuju ke arah yang jamak ketimbang yang tunggal. Pertama, “jamak” dapat kita artikan sebagai cara bertumpu pada kolaborasi dan fleksibilitas. Kolaborasi antarperempuan adalah perlawanan terhadap sistem patriarki yang cenderung memecah belah perempuan dengan memberi ruang pada segelintir perempuan saja. Mengapa kita harus patuh pada logika kompetisi untuk satu posisi elite tertentu bila kita punya kesempatan untuk mengobrak-abrik ruang agar lebih banyak perempuan terlibat?

Kritik atas penunggalan juga berarti fleksibilitas dalam bersiasat. Artinya, kita tidak berpikir dalam satu melainkan banyak cara. Dalam upaya menyebarkan pemikiran perempuan ke publik yang lebih luas, misalnya, kita butuh beragam taktik. Di satu sisi, kita masih memerlukan infrastruktur bagi perempuan dengan label “perempuan”, sebuah ruang yang memungkinkan perempuan bersuara lantang di dalam jaringan perempuan. Di sisi lain, kita juga membutuhkan strategi publik berbeda untuk melibatkan laki-laki menjadi pemangku kepentingan, menggunakan privilese yang mereka miliki untuk menyuarakan sikap kritis terhadap persoalan gender.

Terakhir, tekanan pada yang jamak berarti terus mengupayakan keadilan kolektif ketimbang puas dengan pencapaian individu. Tentu, kita menghargai perempuan-perempuan kuat yang mampu menerjang batas seperti Susi Pudjiastuti dan Sri Mulyani. Kita pun kerap memaknai pencapaian kita sendiri sebagai perjuangan feminis. Namun tekanan pada pencapaian individu ini semestinya tidak diletakkan pada zona nyaman sebab pencapaian tak terlepas dari sejumlah privilese yang dimiliki seseorang. Sebagian besar perempuan tidak memiliki privilese itu, dan kita telah diingatkan oleh penulis feminis Audre Lorde, “Saya belum merdeka bila perempuan lain tidak merdeka kendatipun belenggunya sangat berbeda dengan yang mengikat saya.”  

<br>

\*_Artikel ini pertama kali terbit di_ [Magdalene.co](https://magdalene.co/story/bongkar-siasat-feminis-dalam-seni-dan-budaya-di-indonesia) _dan dipublikasikan ulang dengan izin penulis dan penerbit._
