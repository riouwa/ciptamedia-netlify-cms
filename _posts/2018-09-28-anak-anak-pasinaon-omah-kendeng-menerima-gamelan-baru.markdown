---
title: Anak-anak Pasinaon Omah Kendeng Menerima Gamelan Baru
date: 2018-08-15T08:40:45.377Z
categories:
  - CME
  - Acara
  - Pembelian Gamelan
tags:
  - gamelan
  - pasinaon
  - omah kendeng
author: Endah Fitriana
comments: true
img: /uploads/blog/ft-9.jpg
---
![null](/uploads/blog/ft-9.jpg)

GAMBUH

Atur gunging panuwun

Awit sedaya peparingipun

Gamelan pepak kang sampun kula tampi

Sarana kangge sinau

Bisa ngerti temah klakon

Matur nuwun CME, Wikimedia Indonesia, Ford Foundation

(Ucapan terimakasih kami sampaikan atas semua pemberiannya, gamelan komplit telah kami terima.  Menjadi sarana bagi kami untuk belahar, sehingga kami menjadi mengerti.)	

Terima kasih CME, Wikimedia Indonesia, Ford Foundation

Satu bait macapat Gambuh, malam itu Sabtu 11 Agustus 2018 mengalun merdu dari anak-anak Pasinaon Omah Kendeng, sebelum mereka menabuh Lancaran Suwe Ora Jamu. Tembang itu saya tulis khusus sebagai ucapan terima kasih kepada CME, Wikimedia Indonesia dan Ford Foundation yang telah memberikan hibah kepada kami untuk kelengkapan sarana sinau di Pasinaon Omah Kendeng. Wajah wajah lugu yang kami temani untuk sinau bersama setiap Jumat sore, malam itu kelihatan sangat bahagia. Gamelan baru yang berkilau itu, setelah selesai ditabuh ada diantara mereka yang lantas mengelus dan mengelapnya. Ketika kutanya kenapa? Jawabnya mengejutkan dan membuatku tertawa,” eman-eman, Budhe”.

Malam itu banyak diantara orang tua anak-anak peserta pasinaon yang mendampingi anak-anaknya mengikuti acara “brokohan gamelan”. Selamatan sebagai ucapan rasa syukur atas hibah dan bersama memohon keselamatan. Segala pernak-pernik selamatan memang disiapkan oleh para orang tua anak-anak pasinaon. Dengan mengundang pula masyarakat sekitar Pasinaon Omah Kendeng selamatan segera dimulai. Sebelumnya saya didaulat untuk menceritakan suka duka selamat 7 tahun menyediakan diri mendampingi anak-anak Sedulur Sikep sinau gamelan dan macapat (nembang) tanpa mau dibayar, sampai akhirnya mendapat hibah gamelan. 

Tiba saatnya potong tumpeng. Tumpeng merah putih (dari beras merah dan beras putihI) dipotong, setelah dilengkapi dengan lauk yang ada, potongan tumpeng itu diserahkan oleh salah satu anak pasinaon kepada saya. Saat itulah tangis anak-anak pasinaon tak bisa dibendung. Satu persatu mereka menyalamiku lantas berpelukan, menangis, sampil mengucap” terimakasih Budhe”. Ketika kutanya kenapa menangis? Jawabnya,” tak menyangka akan mendapatkan sarana sinau yang bagus seperti ini, Budhe”

“Semakin rajin sinau, ya, ajak adik dan saudara-saudara kalian”, pesanku yang ternyata tak tahan juga untuk tidak meneteskan air mata.

“Iya Budhe”, senyumnya mengembang sambil mengusap air mata.

![null](/uploads/blog/ft-4.jpg)
