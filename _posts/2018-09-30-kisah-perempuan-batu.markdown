---
title: Kisah Perempuan Batu
date: 2018-09-30T07:36:26.786Z
categories:
  - CME
  - Esai
  - 11 Ibu 11 Panggung 11 Kisah
tags:
  - ''
author: Kadek Sonia Piscayanti
comments: true
img: /static/img/landing/akses.png
---
12 Agustus 2018 dipentaskan naskah berjudul “Aku, Perempuan Batu”, pentas kedua dalam Project 11 Ibu 11 Panggung 11 Kisah. Pentas ini bertempat di Rumah Belajar Komunitas Mahima Jalan Pantai Indah Singaraja.  Kisah Watik sebagai tukang batu sangat menyedihkan. Ia hanya tamat SD, tidak memiliki keahlian apa-apa. Menikah usia muda, di usia belasan, lalu menjadi pekerja kasar, hanya untuk sesuap nasi. Sebelum menjadi tukang, di Jawa ia menjadi buruh apa saja, dari buruh sabit rumput di hutan, buruh petik buah, hingga buruh angkut kotoran ternak. Dia pernah mengalami hampir tidak makan karena tidak ada uang sama sekali, pekerjaannya tidak layak dan penghasilannya sering kali minim. 

Bagi saya, menyutradarai seorang perempuan tukang batu adalah pertama kalinya dalam karir nya sebagai sutradara. Ia mengambil idiom perempuan batu, sebab hidup Watik keras seperti batu. Ia bekerja keras juga seperti batu, tak kenal lelah dan tak kenal putus asa. Dan keras kepala menghidupi semua keluarga. Anak, cucu, dan ibunya sendiri.

Dipilihnya Watik menjadi penting bagi project teater dokumenter ini sebab ia menjadi perspektif unik yang mewakili perempuan dari kalangan sosial bawah. Seperti tujuan project ini, bahwa teater adalah alternatif ruang dengar bagi perempuan, saya berupaya menyediakan wadah teater untuk Watik berbagi cerita. 

Malam kemarin, semua mata memandang hormat kepada Watik sebagai aktor. Sesuatu yang tak pernah ia bayangkan. Sesuatu yang di luar perkiraan. Saya sendiri merasa surprise melihta performancenya. Dia sangat lihai menutupi kekurangannya. Saya ingat proses menjadikan Watik menjadi aktor yang begitu berat prosesnya hingga Watik hampir menyerah. Pertama, tentu membaca teks naskah. Meski cerita sendiri, ketika menjadi naskah, ia menjadi naskah yang berjarak. Kedua, tentu masalah waktu. Sebagai tukang batu, masa kerjanya full dari pagi hingga sore hari. Waktu latihan sangat sempit. Hanya malam. Sejam dua jam. Lalu persoalan lain yaitu menampilkan akting natural. Meskipun ini sekali lagi tentang dirinya sendiri, Watik mengaku sulit menampilkan ceritanya. Namun semua proses itu terasa terbayar dengan pentas kemarin. Apresiasi penonton sangat baik. Bahkan sebagian besar menangis. Terjadi pentas kedua, setelah pentas usai yaitu semua ibu dalam 11 ibu memeluk Watik yang menangis di panggung. Itulah keharuannya. Itulah teater sesungguhnya.

Tokoh teater dari Jakarta, Roy Julian dari Kantor Teater mengatakan bahwa teater yang berlatar biografis ini sangat unik dan organik. Ide project ini sangat orisinal dan menyentuh sisi lain dari pentas teater pada umumnya. Paling tidak ada 3 catatan penting yang ia lihat. Pertama, teater menjadi ruang berbagi secara psikologis dan menyentuh hingga ke dalam ruang jiwa. Kedua, ruang yang benar-benar realis tercipta sebab mereka bermain di setting keseharian mereka. Ketiga, faktor keberanian aktor yang berani berbagi mengungkap fakta dirinya menjadi keunggulan tersendiri karena teks sangat organik dan real. Ia berharap project ini terus dikembangkan. 

Bagi saya sendiri, melihat Watik menangis haru usai pentas dan dirangkul 10 ibu lainnya adalah pentas bonus. Watik merasa hadir dan ada. Dia merasa memiliki keluarga. Dia merasa berharga. Disitulah teater memeluknya sebagai manusia. Yang benar-benar jujur dan ikhlas. Disanalah teater bekerja. Senyata nyatanya.  

Teater ini seakan belum mau berakhir meski pentasnya memang sudah berakhir. Seminggu setelah pentas, Watik pulang kampung. Ia disambut seperti pahlawan yang datang dari medan perang dalam keadaan selamat. Banyak yang menonton pentasnya live di facebook, dan dia dielu-elukan. Sesuatu yang sama sekali ajaib bagi Watik. Mereka memandang Watik seolah tak percaya. Siapa yang berdiri di depan mereka, sepeti Watik yang berbeda. Disanalah Watik sadar teater sekali lagi menjadikannya manusia yang lebih bermakna. Setidaknya bagi dia dan orang-orang terdekatnya. Ia menangis dan berterimakasih pada teater. Watik tidak memilihnya, namun ia terpilih dan dipilih oleh teater untuk menjadi. Menjadi Ibu yang berbeda, manusia yang berbeda.
