---
title: Seni Menjinakkan Politik
date: 2019-02-06T06:30:51.644Z
categories:
  - CME
  - Kajian
  - Perempuan (Tidak) Biasa di Sumba Era 1965 - 1998
tags:
  - CME
  - Perempuan
  - Sumba
author: Martha Hebi
comments: true
img: /uploads/blog/seni-menjinakkan-politik.jpg
---
![](/uploads/blog/seni-menjinakkan-politik.jpg)



Banyak yang menganggap politik itu liar, buas, kotor, dan hitam. Di tangan perempuan ini, asumsi tentang  politik tersebut jadi berbeda. Maria Imaculata Nudu bergelut di dunia politik  lebih dari 30 tahun. Pada awal tahun 1970an, Mama Ima, demikian dia disapa, mulai mengenal dunia politik. Kemudian selama 20 tahun ia menjadi anggota DPRD. Dua periode menjadi anggota DPRD Sumba Barat (1977-1987), dua periode menjadi anggota DPRD Provinsi NTT (1987-1997), merupakan bukti bahwa politik telah dijinakkannya. Bagaimana bisa?  



Seni! Ya, Mama Ima menjinakkan politik dengan seni. “Kalau sudah nyanyi, baca puisi, kita omong sedikit, masyarakat dengar dan ikuti, yahh semua senang, jadi sudah”, kata Mama Ima yang menggunakan seni sebagai alat pengerak hati rakyat.



Pada masa itu sedikit sekali perempuan yang berkecimpung di dunia politik. Politik sangat maskulin. Bahkan jika Mama Ima tidak cerdik, dia tidak dicalonkan lagi pada periode kedua di DPRD Provinsi. “Waktu itu, saya dengar nama saya akan dicoret, jadi pada pertemuan besar Partai Golkar, saya minta di MC, saya bilang kasih saya waktu 2 menit di panggung. Saya omong tidak lama”. Mama Ima diberi kesempatan dan perempuan yang terkenal sebagai juru kampanye Partai Golkar ini kembali dicalonkan sebagai anggota DPRD Provinsi. Apa yang dia lakukan di panggung? Dia membacakan puisi karyanya sendiri.



Kampanye



Sudah berbilang kutantang hidup ini

Berbilang pula lirik dan sapa memaju merebut simpati

Tapi dalam keteduhannya

Aku melihat berbagai yang datang dan pergi

Berbagai yang singgah dan bercerita

Di kerindanganmu itu…

Para musafir berteduh

Para petani melepas lelah menghapus keringat

Yang mampir di badan



Para gembala menjaga ternak

Sambil meniup seruling bamboo

Seruling bamboo mengisi waktu yang bergulir

Para nelayan merajut jala

Para pedagang menggelar dagangannya



Di kerindanganmu itu…

Pemusik berdendang ria sambil memetik gitar

Pelukis membuat model

Para banker menghitung duit

Filosof berfilsafah 

Tetua berpetuah

Seniman seniwati menyita inspirasi

Wartawan wartawati membuat berita

Gadis dan perjaka memadu janji



Di kerindanganmu itu…

Para ibu menisik kenangan sambil berkata:

“Nak, di kerindangan ini ninik, mamamu mengukir sejarah, di kerindangan ini pamanmu dan ayahmu berbulat tekad berikrar setia

mempertahankan Pancasila dan UUD 1945 yang membuatmu bisa

berlanglang buana

bisa santai membonceng kekasihmu

bisa berdebat dan diskusi mengemukakan pendapat

bisa asyik bercanda ria

bisa khusuk berdoa tanpa gangguan 

dan masih ada sejuta kebiasaan.”



Mei 1992

Wakil dari Golkar
