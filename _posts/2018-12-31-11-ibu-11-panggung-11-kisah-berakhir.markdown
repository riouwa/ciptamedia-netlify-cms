---
title: 11 Ibu 11 Panggung 11 Kisah Berakhir
date: 2018-12-30T03:10:09.283Z
categories:
  - CME
  - Berita
  - 11 Ibu 11 Panggung 11 Kisah
tags:
  - teater
  - kisah perempuan
author: Kadek Sonia Piscayanti
comments: true
img: /uploads/blog/img_20181230_150147.jpg
---
Rangkaian akhir project 11 Ibu 11 Kisah 11 Panggung adalah Focus Group Discussion (FGD). Tujuan FGD adalah untuk memberi ulasan, catatan, refleksi, evaluasi, dan menemukan kemungkinan kontinyuitas ke depan, serta kemungkinan-kemungkinan teater ke depan. Dalam FGD ini dihadirkan seorang budayawan Wayan Juniarta, sutradara 11 Ibu 11 Panggung 11 Kisah Kadek Sonia Piscayanti dan tentu saja Ibu-ibu  yang terlibat  dalam project ini. Juniarta menemukan paling tidak tiga hal dalam project ini. Pertama project ini mampu menyentuh hal-hal yang luput dieksplorasi oleh narasi-narasi besar yang diusung teater-teater besar, yaitu narasi kontemporer yang berakar di hati para ibu yang berlatar sosial beragam, dari tukang batu hingga profesor, dari bidan hingga tukang jahit, dari guru hingga pembaca tarot. 

Selama ini teater cenderung sangat eksklusif, mengambil narasi besar dengan agak mengabaikan narasi kecil. Sebuah contoh, narasi sejarah besar pasti diciptakan dari event besar dan orang besar di suatu peristiwa, jarang sekali sejarah mampu membidik narasi kecil-kecil yang tanpa disadari sebenarnya membentuk narasi besar tersebut. Hal itulah yang menarik dari project ini, bahwa ibu-ibu yang luput dari teropong publikasi kekinian, mampu menciptakan narasi kecilnya sendiri. Bahwa mereka sadar kisah mereka yang selama ini terpendam akan tercatat dan terbagikan dalam project ini. Ketiga, setting pentas 11 ibu dengan rumahnya masing-masing sebagai setting cerita menjadi catatan. Dua setting yang paling menarik adalah setting Ibu, Hermawati yang  pentas di kuburan keluarganya. Bagaimana setting kuburan keluarga menjadi latar yang kuat karena terbangun suasana personal aktor dengan leluhurnya. Ada juga seorang ibu yang pentas di bale gede rumahnya, setting yang bersifat personal, spiritual sekaligus filosofis. Menurut Jun, hal ini menjadi alternatif kemungkinan baru dalam konteks teater kontemporer. Panggung tidak lagi menjadi hal yang kaku, statis dan memisahkan aktor dengan audiens, bahkan memisahkan aktor dengan dirinya sendiri. Secara cermat dapat dipahami bahwa dengan setting yang akrab ini, seorang ibu tidak terpisah dengan dirinya, mengakrabi setting hidupnya sendiri. 



![](/uploads/blog/img_20181230_150147.jpg)



Catatan lain menurut Juniarta adalah bahwa sebagai produk teater, project ini adalah produk seni yang memadukan seni estetis, simbolis dan perspektif subjektif penciptanya dalam hal ini sutradara. Ia menonjolkan visi keberpihakan kepada perempuan khususnya ibu di project ini. Yang kedua sebagai sebuah eksperimen sosial, project ini mampu  menyatukan kesebelas ibu dari berbagai latar sosial dalam satu komunitas yang aman bagi mereka untuk saling berbagi. 

Salah seorang ibu yaitu Prof. Dr. Putu Kerti Nitiasih mengatakan bahwa project ini luar biasa mempersatukan 11 ibu dengan beragam latar belakang untuk saling mendukung di project ini. Beliau mengakui sebagai orang yang biasa didengar menjadi orang yang belajar mendengar di project ini. Dengan beragam persoalan sang profesor merasa banyak belajar dari project ini. Hal yang hampir sama dikatakan oleh Tuti Dirgha seorang kepala sekolah yang juga guru SD sekaligus penulis, bahwa project ini adalah project yang memberinya waktu untuk memaknai kembali perannya menjadi perempuan, istri, ibu dan nenek. 

Seorang ibu lain, Diah Mode mengatakan bahwa project ini telah membuatnya ‘lulus’ atau ‘wisuda’ sebagai ibu dan istri. Beragam peristiwa dan momen hampir gagalnya dalam berumah tangga menjadi terselamatkan karena project ini. Dia mulai membangun narasi dirinya dengan lebih dalam, lebih menghargai kesalahan sebagai sebuah pembelajaran, lalu membuat keputusan untuk mampu belajar lebih baik. 

Salah seorang seniman muda, Desi Nurani memberikan kesaksian bahwa project ini luar biasa, dan ia tak bisa membayangkan jika ibunya bergabung dengan komunitas ini apakah memiliki keberanian untuk melakukan hal yang sama dengan para ibu di project ini. 

Sutradara pementasan, Kadek Sonia Piscayanti mengatakan bahwa project ini adalah upaya mendengarkan ibu dari berbagai kalangan dan tidak menutup kemungkinan akan ada banyak lagi ibu-ibu lain yang kisahnya akan dipentaskan. Sebagai project inisiasi, Sonia mengatakan project ini adalah awal untuk munculnya kemungkinan lebih banyak dan lebih besarnya komunitas mendukung perempuan berbicara dan bercerita di masa depan.
