import csv
import json
import os
import sys

from ruamel.yaml import YAML
from IGitt.GitLab.GitLab import GitLab, GitLabPrivateToken
from IGitt.Interfaces.MergeRequest import MergeRequestStates

# These are Netlify row IDs, not Proyek nomor

unusable = [
    0, 3, 4
]

last_processed = 1282

nomor_deleted = set([
    229,
])

nomor_duplicates = set([
    490,
    600,
])

nomor_duplicate_no_mr = set([
    56,
    213,
    478,
    636,
])

nomor_rejected = set([
    52,
    53,
    131,
])

nomor_withdrawn = set([
    358,
])

nomor_unusable = nomor_deleted | nomor_duplicates | nomor_duplicate_no_mr | nomor_rejected | nomor_withdrawn

nomor_offsets = {
    5: 9,
}

private = set([
])

yaml = YAML()
yaml.width = 4096


def print_list(data):
    nomor = 0
    for i, row in enumerate(reversed(data)):
        nomor = get_nomor(i)
        if i in unusable:
            row_ind = '-'
        else:
            row_ind = ''
        nomor += nomor_offsets.get(i, 1)
        if nomor in nomor_deleted:
            nomor_ind = '-|'
        elif nomor in nomor_closed:
            nomor_ind = '-'
        else:
            nomor_ind = ''
        print('%d%s: %d%s: %s - %s' %
              (i, row_ind, nomor, nomor_ind,
               row['nama'] or
                  row['auth_name'] or
                  row['email'],
               row['proyek']))


def get_filename(nomor):
    if nomor in private:
        if isinstance(nomor, str):
            assert '-' in nomor
            nomor = 's%s' % nomor
        else:
            nomor = 's-%0.4d' % nomor
    try:
        return '_hibahcme/%0.4d.md' % int(nomor)
    except ValueError:
        return '_hibahcme/%s.md' % nomor


def load_yaml_file(nomor):
    filename = get_filename(nomor)
    with open(filename, 'r') as f:
        docs = list(yaml.load_all(f))
        return docs[0]


def load_hibah_file(nomor):
    filename = get_filename(nomor)
    with open(filename, 'r') as f:
        text = f.read()
        return text


def create_yaml_file(row, nomor, add_ktp=False, add_private=False, overwrite=False):
    print('Generating %s' % nomor)

    row = row.copy()
    if 'foto' in row and row['foto']:
        row['foto'] = json.loads(row['foto'])
    if 'file' in row and row['file']:
        row['file'] = json.loads(row['file'])

    if not add_private:
        del row['ip']
        del row['telp']
        del row['user_agent']
        del row['email']
        del row['auth_name']
        del row['created_at']

    if not add_private and not add_ktp:
        del row['ktp']

    filename = get_filename(nomor)
    try:
        text = load_hibah_file(nomor)[4:-4]
    except FileNotFoundError as e:
        print('skipping nomor %s: %s' % (nomor, e))
        print('   %r' % row)
        return

    try:
        data = load_yaml_file(nomor)
        if not data:
            print('File for %d empty' % nomor)
            data = {'nomor': nomor}
    except Exception as e:
        print('File for %d missing: %s' % (nomor, e))
        data = {'nomor': nomor}

    for key, value in dict(row).items():
        if key not in data:
            data[key] = value
        elif overwrite and data[key] != value:
            data[key] = value

    with open(filename, 'w') as f:
        f.write('---\n')
        if add_private or overwrite:
            yaml.dump(data, f)
        else:
            f.write(text)
            if add_ktp and 'ktp: ' not in text:
                f.write("ktp: '%s'\n" % data['ktp'])
        f.write('---\n')
        print('Wrote %s' % filename)


def output_yaml_files(gen, add_ktp=False, add_private=False, overwrite=False):
    for i, row in gen:
        if not row['dana']:
            print('No dana %s' % i)
            continue

        print('Processing row %d' % i)
        nomor = i
        if i >= 5:
            nomor += 9

        create_yaml_file(row, nomor,
                         add_ktp=add_ktp,
                         add_private=add_private,
                         overwrite=overwrite)


def load_csv(filename):
    with open(filename) as f:
        reader = csv.DictReader(f)
        return list(reader)


def get_closed_mrs(repo, label=None, inverse=False):
    return list(repo._search(search_type='merge_requests',
                             state=MergeRequestStates.CLOSED))


# def get_duplicate_mrs(repo, label=None, inverse=False):
#     return list(repo._search(search_type='merge_requests',
#                               label='Duplicate'))


def get_mr_titles(mrs):
    return [item['title'] for item in mrs]


def get_mr_nomor(titles):
    nomors = set()
    for title in titles:
        parts = title.split(' - ')
        if len(parts) != 3:
            continue
        nomors.add(int(parts[0]))

    return nomors


def all(data):
    IGL = GitLab(GitLabPrivateToken(os.environ.get('GL_TOKEN')))
    repo = IGL.get_repo('ciptamedia/ciptamedia.gitlab.io')

    closed_mrs = get_closed_mrs(repo)
    closed_titles = get_mr_titles(closed_mrs)

    nomor_unusable.update(get_mr_nomor(closed_titles))

    # Implemented as constant
    # duplicate_mrs = get_duplicate_mrs(repo)
    # duplicate_titles = get_mr_titles(duplicate_mrs)

    # nomor_unusable.update(get_mr_nomor(closed_duplicate))

    nomor = 0
    for i, row in enumerate(reversed(data)):
        nomor += nomor_offsets.get(i, 1)

        if nomor in missing_ktp:
            print('Missing ktp: %r' % row)

        if i in unusable:
            print('Unusable %d : %s' % (i, nomor))
            continue

        if nomor in nomor_unusable:
            print('Unusable %s' % nomor)
            continue

        if i > last_processed:
            print('Reached end %s' % last_processed)
            break

        row['nomor'] = nomor

        yield i, row


def non_private(data):
    for i, row in all(data):
        nomor = row['nomor']
        if nomor in private:
            continue
        else:
            yield i, row


def main(argv=None):
    filename = argv[1] if len(argv) == 2 else '../netlify-data.csv'

    data = list(load_csv(filename))
    gen = non_private(data)
    output_yaml_files(gen, add_ktp=True)


if __name__ == '__main__':
    main(sys.argv)
