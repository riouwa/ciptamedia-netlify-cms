import csv
import os
import sys

UPPERCASE = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z')


def split_header(code, lines):
    for i, line in enumerate(lines):
        line = line.strip('\n,')
        if not line:
            continue
        elif line.startswith(code + ' '):
            continue
        break
    else:
        raise ValueError('get_header scanned to EOF')
    print('%s: Removed %d lines' % (code, i))

    header = line

    if 'Jumlah,Unit,Jumlah,Unit' in header:
        header = header.replace(
            'Jumlah,Unit,Jumlah,Unit',
            'Jumlah,Unit,Jumlah2,Unit2'
        )
        header = header.replace(
            'Deskripsi,,',
            'Deskripsi,Deskripsi2,'
        )

    body = []
    for line in lines[i+1:]:
        if not line:
            continue
        # Rearrange ',A1,Foo' -> 'A1,,Foo'
        if line[0] == ',':
            if line[1] in UPPERCASE:
                comma = line[1:].index(',')
                # print('looking at %d %s' % (comma, line[2:comma+1]))
                try:
                    int(line[2:comma+1])
                except Exception:
                    pass
                else:
                    line = line[1:comma+1] + ',,' + line[comma+2:]

        line = line.strip('\n,')
        if not line:
            continue
        if not line.strip(' '):
            continue

        total_line_test = line.strip('"0123456789,.')
        if not total_line_test:
            print('removing probable total line: %s' % line)
            continue
        if total_line_test.lower() == 'total':
            print('removing probable total line: %s' % line)
            continue

        body.append(line)

    return header, body


def load_csv(code, filename):
    with open(filename) as f:
        lines = f.read().splitlines()
        header, body = split_header(code, lines)

        reader = csv.DictReader([header] + body)
        return list(reader)


def get_dana_approved(code):
    with open('_hibahcme/%s.md' % code) as f:
        lines = f.read().splitlines()
        for line in lines:
            if line.startswith('approved_dana: '):
                return int(line[16:].strip('\n\"\''))
    raise RuntimeError('Could not find approved_dana')


def is_empty_col0(data, col0_label):
    for row in data:
        cell0 = row.get(col0_label)
        if cell0.strip():
            return False

    return True


def is_col_letter_id(data, col_label):
    good_values = []
    int_values = []
    bad_values = []

    for row in data:
        cell = row.get(col_label)
        if not cell:
            continue
        cell = cell.strip()
        if not cell:
            continue
        try:
            int(cell)
            int_values.append(cell)
            continue
        except ValueError:
            pass

        if len(cell) > 2 and cell[1] == '.':
            cell = cell[0] + cell[2:]

        if len(cell) > 3:
            bad_values.append(cell)
            continue

        if cell[0] not in UPPERCASE:
            bad_values.append(cell)
            continue

        good_values.append(cell)

    if bad_values:
        print(bad_values)
        return False

    return good_values


def get_dimensions(row, dimension_headers, value_headers):
    dimensions = {}
    for i, header in enumerate(dimension_headers):
        label = row[header].strip().lower()
        if not label:
            continue
        value = row[value_headers[i]].strip()
        if not value:
            raise ValueError('a value is missing')
        try:
            value = int(value)
        except:
            value = float(value)
            print('Warning: %s is not an integer' % value)
        if label in dimensions:
            if value == 1:
                continue
            if dimensions[label] != 1 and value != 1:
                raise ValueError('cant add %d to %s:%d' % (value, label, value))
        dimensions[label] = value

    # These exceptions are all for 0701 only
    if get_row_code(row) == 'A11' and dimensions == {'orang': 6}:
        dimensions['makan'] = 3
        dimensions['hari'] = 3
    if get_row_code(row) == 'A14' and dimensions == {'orang': 6}:
        dimensions['hari'] = 3
    if get_row_code(row) == 'C1' and dimensions == {'orang': 6}:
        dimensions['cerpen per orang'] = 3
    if get_row_code(row) == 'D5' and dimensions == {'orang': 6}:
        dimensions['makan'] = 3
        dimensions['hari'] = 3
    if get_row_code(row) == 'D6' and dimensions == {'orang': 6}:
        dimensions['hari'] = 3

    # These exceptions are all for 1053 only
    if 'permonth' in value_headers:
        if 'tahun' not in dimensions:
            if get_row_code(row) == 'D2':
                dimensions['bulan'] = 8
            else:
                dimensions['bulan'] = 9

    return dimensions


max_total = 1000000000


def get_totals(row, total_headers):
    if len(total_headers) == 1:
        header = total_headers[0]
        value = row[header].replace('.','').replace(',','').replace('Rp', '').strip()
        value = int(value)
        return (value, value)

    low_total = max_total
    high_total = 0
    for header in total_headers:
        value = row[header].replace('.','').replace(',','').replace('Rp', '').strip()
        if not value:
            continue

        value = int(value)
        if value < low_total:
            low_total = value
        if value > high_total:
            high_total = value

    if low_total == max_total:
        raise ValueError('No cost for %s from %r' % (get_row_code(row), total_headers))

    # print("%4s: %9d %9d" % (get_row_code(row), low_total, high_total if high_total != max_total else 0))

    return low_total, high_total


def get_row_total(row, dimension_headers, value_headers, total_headers):
    dimensions = get_dimensions(row, dimension_headers, value_headers)
    low_total, high_total = get_totals(row, total_headers)

    total = low_total
    for value in dimensions.values():
        total = total * value

    return total


def get_row_code(row):
    return next(iter(row.values())).replace('.','').strip()


def skip_row(row):
    code = get_row_code(row)
    return len(code) == 1


def get_budget_total(data, dimension_headers, value_headers, total_headers):
    total = 0
    for row in data:
        if skip_row(row):
            continue
        row_total = get_row_total(row, dimension_headers, value_headers, total_headers)
        total += row_total
        # print("%4s: %9d %9d" % (get_row_code(row), row_total, total))

    return total


def print_list(data, headers, dimension_headers, value_headers):
    for row in data:
        letter = row[headers[0]].replace('.','').strip()
        description = row[headers[1]]

        if len(letter) == 1:
            print('%s : %s' % (letter, description))
            continue

        dimensions = get_dimensions(row, dimension_headers, value_headers)
        print('%s : %s -- %r' % (letter, description, dimensions))


def write_yaml(f, code, data, headers, dimension_headers, value_headers, description_headers, total_headers):
    total = get_budget_total(data, dimension_headers, value_headers, total_headers)
    f.write('hibah: cme\n')
    f.write('nomor: %s\n' % code)
    f.write('total_rencana_anggaran: %d\n' % total)
    f.write('total_hibah: %d000000\n' % get_dana_approved(code))

    f.write('\nlaporan:\n')

    for row in data:
        code = get_row_code(row)
        description_headers = [headers[1]] + description_headers
        description = []
        for header in description_headers:
            if row[header]:
                label = row[header].strip()
                if label:
                    description.append(label)
        description = ' - '.join(set(description))

        if skip_row(row):
            f.write('  %s:\n' % code)
            if description:
                f.write('    nama: %s\n' % description)
            f.write('    subkode:\n')
            continue

        dimensions = get_dimensions(row, dimension_headers, value_headers)
        if '"' in description or ':' in description:
            description = '\'%s\'' % description.replace("'", "\\'")
        elif '\'' in description:
            description = '"%s"' % description.replace('"', '\\"')

        f.write('      %s:\n        keterangan: %s\n' % (code, description))
        if dimensions:
            f.write('        dimensi:\n')
            for label, value in dimensions.items():
                f.write('          %s: %s\n' % (label, value))
        if total_headers:
            low_total, high_total = get_totals(row, total_headers)
            f.write('        harga: %d\n' % low_total)


def generate_yaml(code, data, headers, dimension_headers, value_headers, description_headers, total_headers):
    filename = os.path.join('_data/anggaran/', code + '.yml')
    print(filename)
    with open(filename, 'w') as f:
        write_yaml(f, code, data, headers, dimension_headers, value_headers,
                   description_headers, total_headers)


def process_file(code, filename):
    data = list(load_csv(code, filename))
    headers = list(data[0].keys())
    empty_col0 = is_empty_col0(data, headers[0])
    if empty_col0:
        raise ValueError('col0 (%s) was empty' % headers[0])

    letters = is_col_letter_id(data, headers[0])
    if not letters:
        print('Failed to find letters in %r' % headers)
        print(data[0])
        return

    unknown_headers = []
    dimension_headers = []
    value_headers = []
    total_headers = []
    subtotal_header = None
    total_header = None
    description_headers = []

    for header_orig in headers[2:]:
        if not header_orig:
            continue
        if 'ignore' in header_orig:
            continue

        header = header_orig.lower()
        if 'cost' in header or 'harga' in header:
            total_headers.append(header_orig)
            continue

        if 'number' in header or 'permonth' in header:
            value_headers.append(header_orig)
            continue
        if 'dimension' in header:
            dimension_headers.append(header_orig)
            continue

        if 'periode (month)' in header:
            continue

        if 'jml' in header or 'jumlah' in header:
            if len(value_headers) < 2:
                value_headers.append(header_orig)
            else:
                total_headers.append(header_orig)
            continue
        if 'satuan' in header:
            if len(dimension_headers) < 2:
                dimension_headers.append(header_orig)
            else:
                total_headers.append(header_orig)
            continue
        if 'unit' in header:
            dimension_headers.append(header_orig)
            continue
        if 'volume' in header:
            value_headers.append(header_orig)
            continue

        if 'kuantitas' in header or 'quantity' in header:
            value_headers.append(header_orig)
            continue
        if 'freq' in header or 'frekuensi' in header:
            value_headers.append(header_orig)
            continue
        if 'total' in header or 'price' in header or 'harga' in header or 'biaya' in header or 'nilai' in header or ' ket ' in header or 'budget' in header:
            total_headers.append(header_orig)
            continue

        if 'status' in header:
            continue

        if 'keterangan' in header or 'description' in header or 'deskripsi' in header:
            description_headers.append(header_orig)
            continue

        if 'waktu' in header:  # info/ empty
            description_headers.append(header_orig)
            continue

        unknown_headers.append(header_orig)

    if unknown_headers:
        print(unknown_headers)

    if len(dimension_headers) != len(value_headers):
        print(dimension_headers, value_headers)
        raise ValueError('mismatched dimensions columns')

    elif len(dimension_headers) != 2 or len(value_headers) != 2:
        print(dimension_headers, value_headers)
        print('number of dimensions columns is not 2')

    if not len(total_headers):
        print(headers)
        raise ValueError('no totals')

    if len(total_headers) > 2:
        print(total_headers)
        raise ValueError('too many totals')

    if len(total_headers) == 1:
        print('only one total_header: %s' % total_headers[0])

    # print_list(data, headers, dimension_headers, value_headers)
    generate_yaml(code, data, headers, dimension_headers, value_headers, description_headers, total_headers)


def main(argv=None):
    dirname = '../anggaran-csv'
    arg_code = sys.argv[1] if len(sys.argv) > 1 else None
    files = list(os.listdir(dirname))
    files.sort()
    for filename in files:
        code = filename[0:4]
        if arg_code and arg_code != code:
            continue
        print(filename)
        filename = os.path.join(dirname, filename)
        process_file(code, filename)
        print('')

if __name__ == '__main__':
    main(sys.argv)
