{% if site.environment != 'production' %}
function oauth() {
  var popup = window.open("", "_blank");

  $.ajax({
    url: "https://web.api.commento.io/oauth/new-empty-state",
    type: "GET",
    success: function(data) {
      var resp = JSON.parse(data);

      if (resp.success) {
        document.cookie = "state=" + resp.state + "; path=/";
        popup.location = "https://web.api.commento.io/oauth/google/redirect?state=" + resp.state;

        var interval = setInterval(function() {
          if (popup.closed) {
            clearInterval(interval);
            var json = {"state": resp.state};
            $.ajax({
              url: "https://web.api.commento.io/oauth/self",
              type: "POST",
              data: JSON.stringify(json),
              success: function(data) {
                var me = JSON.parse(data);
                console.log(me);
                if (me.success) {
                  console.log("successful google oauth for commento");
                  $('#commento-oauth').text('Sudah masuk!').attr('disabled', 'disabled');
                }
                else {
                  console.log("unsuccessful google oauth for commento");
                  // TODO: unsuccessful login (maybe the closed the popup?)
                }
              },
            });
          }
        }, 250);
      }
    },
  });
}
{% endif %}
