#!/bin/bash

set -e -x

JEKYLL_ARGS="--incremental --strict_front_matter"
: "${GIT_MTIME_DIRS:=}"

if [[ "$GITLAB_CI" ]]; then
  # GitLab Pages only serves content from 'public'.
  JEKYLL_ARGS="$JEKYLL_ARGS -d public"
fi

build() {
  echo "Setting mtimes ..."
  bundle exec git set-mtime > /dev/null

  if [[ -n "${JEKYLL_ENV}" && -f _config.${JEKYLL_ENV}.yml ]]; then
    JEKYLL_ARGS="$JEKYLL_ARGS --config _config.yml,_config.${JEKYLL_ENV}.yml"
  fi

  # Workaround for https://github.com/gjtorikian/jekyll-last-modified-at/issues/54
  # because the file mtimes are set above.
  PATH=".ci/nogit:$PATH" bundle exec jekyll build $JEKYLL_ARGS

  exit
}

if [[ "$CONTEXT" == "production" ]]; then
  build
fi

if [[ "$GITLAB_CI" ]]; then
  COMMIT_REF=$CI_COMMIT_SHA
fi

# Get commit information
COMMIT=$(git log -1) || exit

if [[ "$COMMIT" == *"[full-build]"* ]]; then
  echo "Commit message contains [full-build] keyword; Doing full build ..."
  build
fi

# List all modified files in the commit
MODIFIED_FILES=$(git show --name-only --diff-filter=ACMR --format="" $COMMIT_REF)

echo "Fast build mode"
echo "Removing: $PATH_TO_EXCLUDE"
echo "These committed file(s) are not removed:"

echo $MODIFIED_FILES | xargs tar -cvf modified.tar --files-from /dev/null
echo
rm -rf $PATH_TO_EXCLUDE
tar -xf modified.tar
rm modified.tar

build
