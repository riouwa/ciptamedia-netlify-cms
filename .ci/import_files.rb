#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

submission_file = ARGV[0]
data = YAML.load_file(submission_file)
data, upload = data.partition { |key, _value| !key.to_s.match(/^upload_/) }
                   .map(&:to_h)

dir = '_uploads'
filename = File.basename(submission_file, '.*')

upload.each do |key, value|
  next if value.empty?
  subdir = key.sub('upload_', '')
  extension = File.extname(value)
  file_path = "#{dir}/#{subdir}/#{filename}#{extension}"

  puts "Fetching #{key}: #{value} to #{file_path}"

  `mkdir -p #{dir}/#{subdir}`
  `curl -sSf #{value} -o #{file_path}`
  exit(1) unless $?.success?

  data[subdir] = "/#{file_path}".sub('/_', '/')
end

File.write(submission_file, data.to_yaml + "---\n")

`git add --all`
`git commit -m "Push files from #{submission_file}" -m '[ci skip]'`
