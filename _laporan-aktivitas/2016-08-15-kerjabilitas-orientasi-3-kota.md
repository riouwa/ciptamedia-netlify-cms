---
nama: Admin Kerjabilitas
proyek: Kerjabilitas
title: Orientasi 3 Kota
categories: laporan
date: 2016-08-15
endDate: 2016-08-20
lokasi: Kantor Saujana
alamat: Jl. Sidikan, Gg WijayaKusuma no 82A, Sorosutan,Umbulharjo,Yogyakarta
jam: 09.00 - 17.00 WIB
hadir: |-
  - Rubby Emir
  - Tety Sianipar
  - Ludmilla Wiliyanti
  - Billy Purwocoroko
  - Muh. Akbar Pribadi
  - Immawati Yusnaeni
  - Ndaru Patma
  - Anggrahini
  - Desta Ariyana
  - Alfian Firdaus
  - Anisah Puji
  - Riang Girinda
  - Sucipto
  - Katrina Lawrence
tujuan: |-
  - Meningkatkan fungsionalitas dan aksesibilitas Kerjabilitas.com dan aplikasinya dengan merekrut Staf Penjangkauan Mitra di 3 Provinsi.
  - Meningkatkan jangkauan program ke – 3 provinsi (termasuk 2 provinsi di luar jawa)
  - Meningkatkan jumlah penempatan kerja penyandang disabilitas.
  - Menjangkau para stakeholders di 3 wilayah, untuk memahami permasalahan penyandang disabilitas di wilayah tersebut.
hasil: |-
  - Merekrut tiga orang staf dari masing-masing kota jangkauan (Medan,Makassar dan Surabaya) dari total 50 orang yang mendaftar lewat website ataupun di iklan lowongan lain.
  - Melakukan masa orientasi terhadap staf dari 3 area (Medan,Makasar,Surabaya) tentang program Kerjabilitas.
  - Staf penjangkauan memahami jobdesk dan tanggung jawab mereka untuk mensukseskan program kerjabilitas di masing-masing kota.
evaluasi: |-
  - Tim Perekrutan menemukan kendala dalam mencari kriteria yang sesuai dengan program penjangkauan.
rekomendasi: |-
  - Apabila ada perekrutan selanjutnya bisa lebih spesifik ke daerah (pasang iklan lowongan di koran-koran daerah)
img:
  url: /uploads/Agustus_19_2016_Orientasi_3_Kota_2.jpg
img_caption: Pengenalan staf penjangkauan
img2:
  url: /uploads/Agustus_15_2016_Orientasi_3_Kota_1.jpg
img2_caption: Intro tentang Kerjabilitas dari Tim Developer
---
