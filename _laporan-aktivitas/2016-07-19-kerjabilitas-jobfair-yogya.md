---
nama: Admin Kerjabilitas
proyek: Kerjabilitas
title: Job Fair UNY
categories: laporan
date: 2016-07-19
endDate: 2016-07-21
lokasi: Aditorium Universitas Negeri Yogyakarta
alamat: Caturtunggal, Depok, Sleman, Yogyakarta
jam: 08.00 - 15.30 WIB
hadir: |-
  - Ndaru Patma
  - Anggrahini
  - Katrina Lawrence
  - Desta Ariyana
  - Ludmilla Wiliyanti
  - Tety Sianipar
  - Tri Iswanti
  - Wulandari
  - Riang Girinda
tujuan: |-
  - Menjaring lebih banyak lagi pengguna kerjabilitas dalam hal ini adalah penyandang disabilitas dan employer / penyedia kerja inklusif
  - Mempromosikan lagi apa itu kerjabilitas, kepada masyarakat
  - Ikut serta dalam program yang dijalankan dinas terkait yaitu dinas tenaga kerja dan transmigrasi, dinas pendidikan dalam hal ini adalah Universitas Negeri Yogyakarta.
  - Menyebarkan kesadaran disabilitas lewat flyer yang dibuat tentang gambaran umum jenis disabilitas.
hasil: |-
  - Pengunjung job fair pada hari pertama sangat ramai dan juga mengunjungi stand Kerjabilitas, namun selama hari pertama (19 Juli 2016) belum ada pengunjung penyandang disabilitas.
  - Pengunjung juga banyak tertarik untuk mengambil flyer yang telah disediakan, secara harfiah tujuan tentang disability awareness juga telah tercapai
  - Beberapa peserta job fair (perusahaan) mengunjungi booth kerjabilitas, untuk mengetahui apa itu kerjabilitas.
  - Disamping itu penjaga stand juga keliling ke booth perusahaan lain untuk menanyakan apakah lowongan yang tersedia, terbuka bagi penyandang disabilitas.
  - Pada hari kedua, pengunjung job fair penyandang disabilitas, tuna daksa mengunjungi booth, dan membuat akun kerjabilitas.
evaluasi: |-
  - Marketing Tools berupa backdrop ukurannya kebesaran dan kita harus sedikit melipat
  - Beberapa perusahaan yang masih belum mau membuka lowongannya untuk penyandang disabilitas.
rekomendasi: ''
img:
  url: /uploads/Job_Fair_UNY_2016_1.jpg
img_caption: Pencari Kerja sedang mengunjungi booth kerjabilitas
img2:
  url: /uploads/Juli_19_2016_Kerjabilitas_Job_Fair_UNY.jpg
img2_caption: Staf Program dan Volunter menjaga booth kerjabilitas
---
