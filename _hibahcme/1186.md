---
nomor: 1186
nama: Kumara Anggita
foto:
  filename: kumara.jpg
  type: image/jpeg
  size: 36521
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/64a9e5cb-5793-4ace-9b41-99c404351f5f/kumara.jpg
seni: lainnya
pengalaman: baru memulai
website: kumaragitaa.wordpress.com
sosmed: kumaragita
file: null
title: Narasi Perempuan
lokasi: 'Jakarta, papua, bali, sumatra utara'
lokasi_id: ''
deskripsi: >-
  Saya akan membuat website yang berisi tentang narasi perempuan dalam budaya
  lokal Indonesia dengan target audiens anak muda
kategori: perjalanan
latar_belakang: >-
  Tradisi yang diusung oleh budaya lokal ternyata melanggengkan diskriminasi
  terhadap perempuan. Proyek ini akan menarasikan pengalaman-pengalaman
  perempuan di dalam budaya lewat foto, klip video dan narasi singkat. Informasi
  ini akan disebarluaskan lewat website dan social media. Saya melihat karya
  seni adalah cara yang universal, menarik, mudah diterima dan mampu menguggah
  empati tentang persoalan social, sehingga lewat visual yang saya tampilkan,
  masyarakat akan melihat, menyadari dan tergugah untuk mendorong perubahan
  budaya, web ini nantinya akan menjadi web yang interaktif. Masyarakat bisa
  ikut memasukan karya dan idenya terkait pemberdayaan perempuan lokal atau
  tradisional
masalah: >-
  Saya melihat bahwa budaya lokal di Indonesia seperti misalnya di Papua, Bali,
  Batak,  masih banyak mengandung diskriminasi yang dianggap sebagai hal yang
  lumrah baik oleh masyarat. Di Papua hokum adat tidak memihak perempuan, bila
  terjadi perkosaan hukum adat hanya membebankan pembayaran berupa babi padahal
  ini tidak memberi keadilan bagi perempuan. Di Bali perempuan dijadikan ikon
  spiritual, tetapi budaya mendiskriminasi anak-anak perempuan di dalam akses
  terhadap pendidikan. Suku batak dalam hal keharusan perempuan untuk melahirkan
  anak laki-laki.  Poin utama proyek ini adalah menunjukkan realitas perempuan
  di berbagai daerah Indonesia, bahwa seebenarnya diskriminasi terjadi di
  berbagai daerah dengan tampilan (adat) yang berbeda-beda. 
durasi: 9 bulan
sukses: >-
  Web ini akan dikunjungi 8000 pengunjung dan ada kontribusi dari masyarakat
  terhadap website tersebut baik cerita, kritik, foto, dan video. 
dana: '300'
submission_id: 5ab7a6a81ebb075e37ceb6a4
---
