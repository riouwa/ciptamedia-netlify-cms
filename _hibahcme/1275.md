---
nomor: 1275
nama: TAJRIANI
foto:
  filename: Pas Foto_TAJRIANI THALIB_SULAWESI BARAT.jpg
  type: image/jpeg
  size: 161660
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/185761ba-7221-4737-9957-2ae9bc52534b/Pas%20Foto_TAJRIANI%20THALIB_SULAWESI%20BARAT.jpg
seni: seni_pertunjukan
pengalaman: 7 tahun
website: 'http://tajrianithalib.blogspot.co.id/'
sosmed: '@taaj_rianithalib'
file:
  filename: Kecapi Mandar Wonderful Indonesia Festival.JPG
  type: image/jpeg
  size: 4116951
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/a4520017-c883-4b47-87bd-038d60096af0/Kecapi%20Mandar%20Wonderful%20Indonesia%20Festival.JPG
title: Tobaine Mandar (Perempuan Mandar)
lokasi: 'Daerah Pesisir Polewali Mandar, Sulawesi Barat'
lokasi_id: ''
deskripsi: "Tobaine Mandar merupakan sebuah proyek pementasan dengan konsep kontemporer. Ide pementasan ini terinspirasi dari keseharian perempuan Mandar yang hidup di pesisir pantai,  mulai pada saat masih kanak-kanak, remaja, pranikah, hingga menjadi seorang ibu. Aktivitas sehari-hari perempuan Mandar sebenarnya sederhana, tetapi sentuhan kontemporer, untuk merumuskan bentuk pertunjukan yang menggabungkan musik, tari dan teater dengan iringan musik tradisional khas suku Mandar dipadu dengan musik modern akan mewujudkan simbol audio visual yang tepat.\r\n\r\nKeseharian perempuan Mandar selalu terkait dengan praktik kearifan lokal, maka dalam pementasan ini juga akan menampilkan kearifan lokal masyarakat suku Mandar yang khusus dilaksanakan oleh perempuan. Dalam proyek ini akan ada kolaborasi dari beberapa profesi seperti fotografer, videografer, peneliti, penulis dan para seniman yang semuanya adalah sumber daya manusia lokal yang berpengalaman. Selain pementasan akan dihasilkan pula booklet yang berisi informasi berupa foto-foto dan deskripsi tentang pementasan. Terdapat beberapa ide pementasan yang kemungkinan masih bisa dikelola di antaranya yaitu: permainan rakyat, Sayyang Pattuqdu (kuda menari), pandoeang (prosesi mandi sebelum menikah), massaulaq (prosesi khusus ibu hamil), miapi (memasak), peodoang dan manetteq (meidurkan anak dan menenun), pittepeang (penantian). Karena terdapat beberapa paket pementasan, maka jumlah personil dari masing-masing segmen pementasan akan disesuaikan dengan kebutuhan. \r\n"
kategori: kerjasama_kolaborasi
latar_belakang: >-
  Aktivitas sederhana pun sebenarnya dapat menjadi sebuah karya, aktivitas yang
  dimaksudkan dalam karya ini adalah kegiatan khas yang dilaksanakan oleh
  perempuan Mandar, terutama inisiasi dan proses patangan pribadi perempuan
  Mandar, namun hal ini nyaris tidak.pernah diperhatikan, apa lagi dieksplor,
  padahal sesungguhnya hal tersebut justru berperan besar dalam kehidupan
  keluarga dan masyarakat. Aktivitas yang dijadikan sebagai sebuah karya dan
  ditunjukkan pada sebuah pementasan untuk menyadarkan banyak orang bahwa
  sesungguhnya hal yang sederhana itu adalah sesuatu yang istimewa dan menyadari
  peran besar perempuan dalam kehidupan. 
masalah: >-
  Berawal dari rasa takjub pada perempuan-perempuan ibu rumah tangga yang juga
  bekerja serabutan demi keluarga. Setiap pagi dari bangun tidur, merapikan
  rumah lalu mengurus anak, memasak untuk makan siang, seringkali bekerja lalu
  pada malam hari kembali menyiapkan makan, dan mengurus kebutuhan keluarga.
  Peran perempuan begitu banyak dalam keluarga, dalam masyarakat. Namun
  seringkali, perempuan yang dianggap tidak memiliki karir yang baik
  disepelekan. Seorang ibu rumah tangga yang survive dengan kehidupannya yang
  monoton, setiap hari melakukan pekerjaan yang sama, segala sesuatu yang terus
  berulang, pemandangan yang selalu sama. Rumah, dapur, suami dan anak-anak.
  Bagaimana mungkin mereka bisa bertahan? bagaimana cara mereka tetap menikmati
  hari-harinya? Apa yang ada di dalam pikiran mereka? Tujuan dari pementasan ini
  adalah menumbuhkan kesadaran pada masyarakat bahwa para perempuan dengan
  status ibu rumah tangga adalah mereka yang memiliki kesabaran luar biasa, yang
  menjamin segala sesuatu tetap terkontrol dan berjalan baik dalam keluarganya,
  kesadaran tentang urgensi vital perempuan. 
durasi: 3 bulan
sukses: >-
  Hasil akhir dari pelaksanaan program yakni, pementasan yang akan dilaksanakan
  di Sulawesi Barat. Namun, pada perkembangannya konsep pementasan yang akan
  dihasilkan dapat pula dipentaskan pada event-event nasional dan internasional
  sebagai salah satu cara menginformasikan kultur Mandar kepada masayarakat
  luas. Selain itu terdapat pula booklet, foto serta video yang dapat diakses
  baik secara langsung, mau pun daring. Semuanya bertujuan untuk menginformasika
  karakter perempuan Mandar dan hal unik dari mereka kepada masyarakat luas. 
dana: '55'
submission_id: 5ab7c8f9b52f720963e25340
---
