---
nomor: 1191
nama: St. Haerana Machmud
foto:
  filename: Saya en Mira.jpg
  type: image/jpeg
  size: 43667
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/1266b8d5-8caa-49b3-8c1e-a26ba967a1d2/Saya%20en%20Mira.jpg
seni: sastra
pengalaman: 5 tahun
website: naskahyangterbuangblogspot.com
sosmed: posting foto melalui akun FB Haerana Mahmud Maulana
file:
  filename: IMG_20180130_062934.jpg
  type: image/jpeg
  size: 310337
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/f6c58df6-0185-436e-b7de-7bccbd6a0cca/IMG_20180130_062934.jpg
title: Melestarikan Dongeng Suku Bugis Makassar
lokasi: 'Kota Makassar dan Kabupaten Maros, Provinsi Sulawesi Selatan'
lokasi_id: ''
deskripsi: >-
  Saya ingin menghasilkan suatu buku kumpulan dongeng yang berasal dari suku
  Bugis Makassar (suku asal saya) dengan mengingat, mendengarkan dan
  mendokumentasikan dongeng-dongeng tersebut dari para tetua atau orang tua
  khususnya perempuan di sekitar saya, lalu bekerja sama dengan penerbit dan
  menerbitkannya secara indie (berbayar), setelah itu mendistribusikannya ke
  provinsi lain di Indonesia dan daerah lain selain Maros dan Makassar tanpa
  dipungut biaya lewat perpustakaan di daerah tersebut
kategori: lintasgenerasi
latar_belakang: >-
  Saya adalah penikmat dongeng, sejak kecil ayah dan kakak perempuan saya
  (karena ibu saya telah wafat) selalu mendongeng sebelum saya tidur,
  dongeng-dongeng itu saya masih ingat hingga sekarang, dimana zaman telah
  berubah, timbul kekhawatiran dongeng-dongeng ini akan hilang seiring wafatnya
  para penutur, sehingga saya berniat mengabadikannya dalam sebentuk karya
  sastra berupa buku kumpulan dongeng
masalah: >-
  Masalah saya sebagai seorang penulis pemula adalah biaya penerbitan buku,
  mengingat beberapa penerbit yang saya tahu memiliki genre tersendiri dalam
  menerbitkan buku, sehingga naskah-naskah unik sering ditolak dengan alasan
  tidak sesuai dengan selera pasar, dsb, jadi jalan terbaik adalah menerbitkan
  buku secara indie atau berbayar, karena tidak yang abadi di dunia kecuali yang
  tertulis dalam buku, kata mendiang Pramoedya Ananta Toer, semoga Cipta Media
  Ekspresi bisa jadi solusi untuk masalah ini
durasi: 9 bulan
sukses: >-
  Indikator keberhasilan buku ini adalah, pertama, bila terbit suatu buku
  kumpulan dongeng asal Bugis Makassar yang layak baca dan menggunakan bahasa
  Indonesia yang baik dan benar, kedua, jika buku telah terdistribusi ke setiap
  provinsi di Indonesia (skala nasional) dan daerah selain Makassar dan Maros
  (skala lokal), ketiga, jika buku kumpulan dongeng ini telah terpajang di toko
  buku di seluruh Indonesia, keempat, bangsa Indonesia dapat mengenal budaya
  Bugis Makassar dengan membaca dongeng-dongengnya.
dana: '20'
submission_id: 5ab7a99853f63e618f23359d
---
