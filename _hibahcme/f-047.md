---
nomor: f-047
nama: Lusia Neti Cahyani untuk Veronika Bulu Boli (Veronika Ratumakin)
foto:
  filename: f-047-Veronika.jpeg
  type: image/jpeg
  size: 119442
  url: /hibahcme/profile-pict/f-047-Veronika.jpeg
seni: seni_pertunjukan
pengalaman: 4 tahun, sejak tahun 2014
website: 'https://drive.google.com/folderview?id=14-gNvZhE4KyO6IpGWdDGf6upnt--hX8y'
sosmed:
file:
title: WARISAN. Pertunjukan Teater sebagai Upaya Mendialogkan Konflik 
lokasi: Flores Timur, NTT
lokasi_id: 
deskripsi: >-
  Proyek “WARISAN. Pertunjukan Teater sebagai Upaya Mendialogkan Konflik” yang diinisiasi oleh ibu Veronika Ratumakin bersama dengan Sanggar Sina Riang yang didirikannya ini adalah sebuah upaya untuk lebih mengaitkan praktek berteater pada isu-isu yang mereka dan masyarakat hadapi, juga upaya mengajak masyarakat penontonnya untuk berdialog, membicarakan persoalan di lingkungan mereka. 
  Melalui proyek ini, ibu Vero dan kawan-kawan yang tergabung dalam Sanggar Sina Riang (anggota aktif dari sanggar ini adalah ibu-ibu rumah tangga) akan mencoba pendekatan baru dalam proses berteater yaitu menggunakan seni, dalam hal ini teater sebagai alat untuk membaca/mempelajari/menginvestigasi, mendialogkan, dan menampilkan isu/persoalan yang terjadi dalam masyarakat. 
  Isu yang saat ini menjadi keprihatinan dan dipilih untuk dipelajari, didialogkan dan akan ditampilkan adalah persoalan perang antar kampung yang dipicu oleh perebutan kepemilikan atas tanah, yang terus berlangsung dari masa nenek moyang mereka hingga saat ini. 
  Proyek ini juga bermaksud untuk menggali dan mengumpulkan kembali nilai-nilai dan ajaran adat istiadat leluhur Adonara yang sekarang telah mulai hilang dan ditinggalkan. Hal ini dilakukan untuk mencari jawaban atas pertanyaan adakah tuntunan dari para leluhur yang bisa mendorong terjadinya perdamaian dan penyelesaian atas konflik yang terjadi. 
  Sebagai upaya untuk mempelajari isu dan menjadikannya isu bersama, proses penciptaan pertunjukan Warisan akan dilakukan secara bersama oleh seluruh tim yang terlibat yaitu dengan mengunjungi situs/desa yang masih menyimpan bukti-bukti sejarah, melakukan wawancara dengan orang-orang yang terlibat, mengalami, menyaksikan dan terdampak oleh perang, juga tetua adat dan tokoh budaya yang mempunyai pengetahuan atas nilai dan adat istiadat Adonara.
kategori: riset_kajian_kuratorial
latar_belakang: >-
  Adonara adalah sebuah pulau di Kepulauan Nusa Tenggara, yakni di sebelah timur pulau Flores, merupakan bagian dari Kabupaten Flores Timur, NTT. Sebagaimana umumnya di Flores Timur, masyarakat Adonara masih cukup kuat memegang adat sebagai tuntunan hidup sehari-hari dan tata kehidupan bermasyarakat. Masyarakat Flores Timur (termasuk Adonara, Solor) dan Lembata termasuk dalam ras Lamaholot yang multi suku/etnis dan kepercayaan/agama. Sejak jaman dahulu, orang Lamaholot sudah terbiasa hidup berdampingan dengan beragam perbedaan. 
masalah: >-
  Salah satu persoalan yang cukup sering muncul dan belum dapat sepenuhnya terselesaikan adalah perselisihan perihal batas kepemilikan tanah (adat). Di satu sisi, pemerintah tidak dapat menyelesaikan ataupun memediasi penyelesaikan konflik karena tidak adanya bukti kepemilikan yang sah menurut hukum. Cara penyelesaian yang kemudian ditempuh adalah penyelesaian secara adat, yaitu jika tidak tercapai kata sepakat maka jalan satu-satunya  adalah dengan perang. Pihak yang menang dalam perang akan diakui sebagai pihak yang benar.
  Namun pada kenyataannya, satu perang tidak menjadi jaminan selesainya persoalan. Ketidakpuasan akan memicu munculnya konflik lanjutan dan kembali muncul perang. Pihak yang paling merasakan dampak adanya perang tentulah kaum wanita dan anak-anak. Baik dari pihak yang menang maupun kalah, akan sama-sama memakan korban, sama-sama kehilangan. Para wanita kehilangan bapak, suami, anak karena laki-laki dalam keluarga harus turun perang.
  Situasi ini menjadi keprihatinan yang cukup mendalam bagi ibu Vero dan kawan-kawan di Sanggar Sina Riang, yang adalah ibu rumah tangga yang baik secara langsung mengalami kehilangan anggota keluarganya atas terjadinya perang tersebut, atau paling tidak merasakan dan menyaksikan perang terjadi di lingkungan dekatnya. Sebagai perempuan, mereka ingin merespon dan menyuarakan perasaan, pendapat dan harapannya serta menyerukan perdamaian melalui proyek pementasan teater Warisan.
durasi: 7 bulan (Mei s/d November 2018)
sukses: >-
  - Terjadi kunjungan ke lokasi terdampak perang antar kampung dan wawancara dengan masyarakat terdampak serta tokoh masyarakat/adat, budayawan Adonara
  - Tercipta 1 naskah pertunjukan 
  - Terjadi 3 kali pertunjukan teater yaitu di Nihaone (Desa Lebanuba, Kec, Ile Boleng), di Desa Rodentena (Kec. Kelubagolit), di Desa Horowura (Kec. Adonara Tengah).
  - Hadirnya 250 orang penonton di setiap pertunjukan 
  - Adanya dokumentasi pertunjukan berupa foto dan video
dana: '121.300'
---
