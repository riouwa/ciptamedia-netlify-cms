---
nomor: 1277
nama: STEPHANY
foto:
  filename: Screen Shot 2018-03-25 at 8.59.27 PM.png
  type: image/png
  size: 1453925
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/94ca0f78-2588-4a3d-af8d-67df6c93747f/Screen%20Shot%202018-03-25%20at%208.59.27%20PM.png
seni: kriya
pengalaman: 10 tahun
website: www.yayasung.com
sosmed: '@yayasung_'
file:
  filename: Screen Shot 2018-03-25 at 10.55.51 PM.png
  type: image/png
  size: 303851
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/d84b7cf8-1da0-41e2-b66a-196030bdd1e0/Screen%20Shot%202018-03-25%20at%2010.55.51%20PM.png
title: Proyek 59
lokasi: DKI Jakarta
lokasi_id: Q3630
deskripsi: >-
  Di dalam Proyek 59, saya ingin menghadirkan bentuk-bentuk baru yang berbasis
  dari sebuah majalah terbitan tahun 1959, bernama Api Kartini di dalam sebuah
  brand bernama “59”. Brand ini digunakan sebagai jembatan untuk bercerita
  tentang sejarah gerakan perempuan di Indonesia tahun 50an. Pendekatan budaya
  pop dan skema kapitalisme digunakan menjadi strategi utama dari proyek ini.
  Hasil akhir dari proyek ini adalah sebuah launching concept pop-up store dan
  pameran yang berlokasi di Jakarta secara offline dan juga didukung lewat
  online store. Produk-produk yang dihasilkan oleh brand ini berupa pakaian,
  perhiasan, dan merchandise keseharian. Produk dan pameran karya yang akan
  diluncurkan pada acara ini adalah produk dan karya yang berbasis dari majalah
  Api Kartini. Aplikasi desain grafis, materi cerita, gambar, dan karya foto
  akan diambil atau terinspirasi dari majalah tersebut. Beberapa jalur media
  juga akan digunakan sehingga perlu adanya kolaborasi dan kerja sama dengan 1
  videografer, 1 fashion desainer, 1 produk desainer, 1 web desainer, dan 1
  produser musik. 3 tokoh feminis juga akan dipilih sebagai brand ambassador
  dari “59”. Tujuan utama dari kolaborasi-kolaborasi tersebut tentu saja untuk
  merentangkan gaung dari majalah Api Kartini sebesar-besarnya agar cerita
  sejarah dari gerakan feminisme di Indonesia dapat sampai lintas generasi
kategori: kerjasama_kolaborasi
latar_belakang: >-
  Gagasan berawal dari pertanyaan: “Bagaimana cara menghadirkan kembali sebuah
  fakta sejarah tentang gerakan perempuan Indonesia dengan cara yang relevan
  dengan zaman/masa sekarang?” Sering saya merasa kesulitan dalam menciptakan
  sebuah karya yang berbasis dari riset sejarah. Bahasa visual seperti apa yang
  harus saya pilih, agar cerita sejarah yang saya temukan bisa menarik /
  menyulut rasa keingintahuan dari audiens? Bagaimana agar presentasi
  cerita-cerita dalam sejarah tidak membosankan atau “out-of-date”? Proyek “59”
  merupakan sebuah upaya untuk menghadirkan kembali cerita, materi desain
  grafis, dan foto yang ada pada sebuah majalah berjudul Api Kartini yang
  pertama kali terbit tahun 1959 dalam bentuk-bentuk baru. Pendekatan dengan
  budaya pop dan skema kapitalisme digunakan secara sadar sebagai sebuah
  strategi yang ampuh untuk memberikan nafas baru untuk majalah terbitan tahun
  50-an ini. Mengapa Kapitalisme? Konsep kepemilikan, tukar-menukar (barang
  dengan barang atau barang dengan uang) atau dagang sangat dekat dalam
  kehidupan kita sehari-hari. Strategi ini juga cukup tepat untuk direalisasikan
  di ibu kota Jakarta. Angka “59” dari judul proyek ini juga sebagai penanda
  telah 59 tahun umur majalah Api Kartini di tahun 2018.
masalah: "Masalah utama yang ingin diangkat adalah miskonsepsi dari kata “Gerwani”. Kontras fakta antara lembaran sejarah yang saya pelajari dalam riset buku, artikel, wawancara, dan terutama majalah Api Kartini dengan sejarah versi orde-baru tentang Gerwani bertolak belakang 180 derajat. Temuan-temuan baru ini memberikan api semangat kepada saya untuk menghadirkan narasi baru ini ke ruang publik. Gerwani hidup dan jaya pada masa setelah kemerdekaan hingga menjelang tahun 1966. Lalu apa urgensi/pentingnya berbicara tentang Gerwani saat ini? Berbicara tentang Gerwani berarti berbicara tentang: Perempuan, Pembangunan, Ketidaksetaraan gender, Politik-Sosial-Ekonomi, Fitnah, Pembungkaman, Kekerasan Seksual, Penghilangan Paksa, Genosida, dan Keadilan atas seluruh perempuan di Indonesia. Bagaimana bisa mengurai satu dari runutan persoalan diatas, apabila mengucapkan kata “Gerwani” masih sangat tabu hingga sekarang? Selama 32 tahun kita dibutakan, ditempa oleh narasi satu arah, dipisahkan oleh jurang besar antara sejarah perjuangan perempuan di Indonesia. Perlu ada upaya dan bentuk-bentuk baru yang mewakili narasi-narasi alternatif dalam sejarah. Saya sepenuhnya percaya, gerakan feminisme di Indonesia akan berkembang lebih progresif ketika kita secara bebas memiliki akses dan edukasi tentang langkah Gerwani dalam sejarah. Saya berharap kita akan segera sampai ke masa waktu dimana cap/remark “Gerwani” sudah tidak lagi digunakan untuk menjatuhkan kredibilitas perempuan.\r\n"
durasi: 9 bulan
sukses: "Indikator sukses dari proyek 59 ini adalah proses kolaborasi yang lancar antar kolaborator, launching pop-up store online dan offline yang tepat waktu dan liputan dari berbagai media baik cetak dan online\r\n"
dana: '275'
submission_id: 5ab7c92cbcd8e852543072b8
---
