---
permalink: /hibahcme/907
nomor: 907
nama: RR HADDI YARTANING LIENA
foto:
  filename: CSC_0427.JPG
  type: image/jpeg
  size: 1907404
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/ca05febe-08e6-4e71-873d-e66b15bddb56/CSC_0427.JPG
seni: seni_rupa
pengalaman: 10 TAHUN
website: www.batikfennyab.com
sosmed: facebook (liena fennyAbe)  Lkp Fennyke Yogyakarta,Lkp Multi Talenta Yogyakarta,  IG (LienafennyABEbatik)
file:
  filename: karya batik.docx
  type: application/vnd.openxmlformats-officedocument.wordprocessingml.document
  size: 4915568
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/27049fe0-5dc7-4294-a5bf-2e32ad5ca8fd/karya%20batik.docx
title: MENGOLAH LIMBAH MENJADI BERKAH
lokasi: LKP Fennyke Jl. Godean km 8,5 Sidokarto Godean Sleman
lokasi_id: ''
deskripsi: Batik go green (warna alam yang ramah lingkungan) yang diambil dari pemanfaatan limbah lingkungan seperti dari bagian-bagian tanaman, misal:kayu, daun, bunga, akar, kulit buah, dan lain sebagainya.Ada beberapa keunggulan zat warna alam yaitu :\r\n-\tWarna soft/pastel yang jarang ditemui pada kain batik dan mempunyai karakteristik warna khas yang sulit ditiru oleh zat warna sintetis \r\n-\tAman bagi kulit sensitive, karena tidak mengandung pewarna kimia\r\n-\tLimbah tidak mencemari alam,bahkan beberapa orang mengklaim bisa jadi pupuk\r\n-\tJika tidak dibuang pun, sisa pewarna bisa dicampur dengan pewarna baru\r\n-\tTidak perlu impor zat warna,hampir semua yang ada di Indonesia bisa dimanfaatkan\r\n-\tTidak berbahaya bagi tangan para pencelup atau perupa batik\r\n
kategori: akses
latar_belakang: Melihat kondisi lingkungan sekitar, adanya  banyak limbah pepohonan yang roboh dan mengotori lingkungan,sisa sisa penggergajian yang hanya dibuang,kami berpikir dan  berusaha memanfaatkan limbah tersebut untuk diolah menjadi pewarna alam yang ramah lingkungan sebelum dijadikan kayu bakar.Biji - bijian,buah,daun yang berjatuhan bisa di olah menjadi kalung,asesoris dan bermacam souvenir yang bagus dan unik dan harganya terjangkau.\r\n
masalah: BATIK RAMAH LINGKUNGAN\r\nPewarnaan yang menggunakan limbah kayu dan daun,yang sudah di rebus menjadi ekstrak, aman dan bisa menjadi pupuk air sisa limbah pewarna setelah di gunakan.\r\nProses pewarnaan yang tidak melalui perebusan yaitu melalui permentasi.yaitu dengan blue Indigo yang bahan baku nya adalah tanaman Tom\r\n
durasi: 5 bulan
sukses: Adanya Program Bantuan hibah Dari CIPTA MEDIA EKSPRESI tentang Penyelenggaraan Batik GO GREEN (ramah lingkungan) akan membantu kelancaran pelatihan baik teori maupun praktek,merenovasi tempat untuk showroom dan melengkapi sarana prasarana   yang kurang,penyempurnaan work shop.Juga memberikan bantuan peralatan untuk peserta agar bisa memproduksi secara berkelompok,kami bisa memberikan motivasi ini setelah dilaksanakan agar  ber dampak baik bagi peserta ,semakin mencintai budaya peninggalan leluhur kita di masa lampau.\r\n      Kami juga ingin mengikut sertakan untuk uji kompetensi di Lembaga Sertifikasi Kompetensi Batik di TUK “ FENNYKE”  . Karena diharapkan peserta dapat diakui kompetensinya secara nasional setara dengan pendidikan formal.Dengan demikian ketrampilan meningkat dan bermanfaat dalam era MEA saat ini.\r\n
dana: '200'
---
