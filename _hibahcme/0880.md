---
permalink: /hibahcme/880
nomor: 880
nama: Bernadeta Verry Handayani
foto:
  filename: Foto profil 1.jpg
  type: image/jpeg
  size: 325379
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/12849578-1955-4788-852d-0d72b1aa68f1/Foto%20profil%201.jpg
seni: seni_pertunjukan
pengalaman: 19 tahun
website: ''
sosmed: ''
file:
  filename: DSC_1693 2.jpg
  type: image/jpeg
  size: 340672
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/0c6f7975-b480-4690-b3db-33831b837056/DSC_1693%202.jpg
title: Desa Peduli, Manusia-manusia Berhati
lokasi: Jangkaran, Kulon Progo, Yogyakarta
lokasi_id: ''
deskripsi: Saya ingin melakukan sebuah proyek teater di desa Jangkaran, Kulon Progo, Yogyakarta. bekerjasama dengan beberapa elemen masyarakat seperti bidan, dokter, warga setempat, pemerintah daerah, dan beberapa pihak yang terkait dengan isu yang ada di desa ini. Sebelumnya, saya ingin melakukan riset atas isu, yaitu HIV/AIDS. Saya akan berdiskusi dengan beberapa narasumber yang paham betul dengan isu ini, seperti dokter, LSM, dan aktivis yang bergerak di isu ini untuk memperkaya isu ini dari berbagai perspektif\r\n\r\nPada proses kolaborasinya, saya ingin mengajak sebagian warga masyarakat setempat  untuk ikut terlibat bermain bersama beberapa aktor profesional membuat pertunjukan teater berdasarkan isu/masalah yang ada di desa ini. Rencana pertunjukan dilakukan di desa ini dan di salah satu gedung pertunjukan di Yogyakarta. Pertunjukan akan dilakukan pada saat peringatan hari HIV sedunia\r\n
kategori: kerjasama_kolaborasi
latar_belakang: Tahun 2013 desa Jangkaran, ini pernah menjadi tempat penelitian saya untuk sebuah karya pertunjukan saya yang bertemakan Perempuan Buruh Migran. Seorang bidan di sana, yang juga menjadi salah satu narasumber saya, dua tahun yang lalu, kembali menghubungi saya untuk memfasilitasi warga masyarakat di sana membuat pertunjukan teater dengan isu yang sedang hangat di sana, yaitu HIV.\r\n\r\nDia bercerita bahwa desa ini mendapatkan predikat Desa Peduli HIV dari pemerintah setempat atas sikap warga  yang bisa menerima dan mendukung perjuangan hidup penderita HIV. \r\n\r\nSetelah beberapa kali latihan, dengan beberapa alasan, proyek tersebut gagal dilaksanakan.Di luar kegagalan proyek tersebut, ada satu pengalaman berkesan yang sesungguhnya saya simpan sampai sekarang. Pada sebuah kesempatan, saya dipertemukan dengan pasangan penderita HIV yang ada di sana. Di saat mereka mengulurkan tangan untuk bersalaman, saya mengalami stigma yang selama ini disematkan pada penderita HIV, bahwa jika kita bersentuhan, kita akan ketularan. Di satu momen saya sempat tergeragap, namun akhirnya saya bisa mengutuk diri saya sendiri atas stigma itu. Maka, saya pun akhirnya bisa mengulurkan tangan saya. Buat saya, peristiwa itu menjadi momen berharga saya dan menjadi satu pesan penting untuk diangkat bagaimana kita harus meluluhkan stigma itu dengan informasi dan pengetahuan yang cukup bisa diterima dan dipahami.   \r\n
masalah: 'Berangkat dari pengalaman saya mendapatkan momen berharga di atas dan cerita tentang warga setempat dari proses yang awalnya menolak dan ingin mengusir pasangan suami istri penderita HIV tersebut dari desa mereka menjadi warga yang peduli dan pada akhirnya menjadi satu contoh inspiratif sehingga desa ini mendapatkan predikat Desa Peduli HIV/AIDS, buat saya menjadi inspirasi yang sungguh luar biasa. Perubahan sikap warga ini sesungguhnya juga karena usaha keras seorang bidan di sana yang memberikan banyak pengetahuan ke warga. Sikap peduli yang ditunjukkan warga di sana, misalnya dengan membuat program 2000, yaitu setiap seminggu sekali warga mengumpulkan donasi sejumlah Rp.2.000, untuk penderita HIV untuk membantu proses bertahan hidupnya.\r\n\r\nLalu, warga juga memberikan kesempatan lahan pekerjaan ringan kepada penderita agar si penderita masih bisa bersosialisasi di masyarakat. Buat saya, peristiwa semacam ini sangat pantas untuk diangkat dan disampaikan, melalui media pertunjukan teater sebagai pesan bagaimana stigma yang terlalu kuat dan kadang tak berdasar itu memmbuat kita lupa akan hakikat seseorang sebagai manusia. Apa yang dilakukan warga setempat yang akhirnya bisa peduli adalah peristiwa kemanusiaan yang luar biasa.  '
durasi: 9 bulan
sukses: Ukuran keberhasilan proyek ini adalah, pertama, terjadinya kolaborasi antara warga setempat dan pihak-pihak terkait dengan seluruh tim pertunjukan. Kedua, pertunjukan teater terwujud dan dipentaskan di dua tempat, yaitu di Desa Jangkaran dan di salah satu ruang pertunjukan di Yogyakarta. Ketiga, dan yang menurut saya paling penting adalah, adanya perubahan atau peluluhan sikap/mindset masyarakat atas stigma yang disematkan pada penderita HIV/AIDS. Bahwa, mereka adalah juga manusia biasa yang sedang berjuang mempertahankan hidupnya, dan bukan pembawa penyakit menular yang bisa menyebabkan kematian.
dana: '53'
---
