---
permalink: /hibahcme/663
nomor: 663
nama: Nova Ruth Setyaningtyas
foto:
  filename: nova-live-1.png
  type: image/png
  size: 562338
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/8877e1be-3dce-4317-9fa4-11c07ced136c/nova-live-1.png
seni: musik
pengalaman: 18 tahun
website: 'http://novaruth.blogspot.com/'
sosmed: 'https://www.youtube.com/user/filastine/videos'
file:
  filename: 03 Perbatasan_1.mp3
  type: audio/mp3
  size: 3524129
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/0193b7db-4537-486e-95c5-08b095336211/03%20Perbatasan_1.mp3
title: 'Perbatasan: Catatan Perjalanan'
lokasi: Malang
lokasi_id: ''
deskripsi: "Perbatasan, rencananya akan menjadi sebuah judul buku musikal yang berisi kumpulan cerita saya dalam meretas perbatasan di perjalanan bermusik. Di dalamnya dapat ditemukan observasi tulis saya tentang perbatasan sebagai perempuan yang terlahir di Indonesia dan album akustik yang menelanjangi karya-karya yang pernah saya buat dari genre hiphop, jazz, rnb, sampai pentatonik jawa. Observasi yang berupa buku ini akan saya tulis di bawah bimbingan Ibu Saraswati Sunindyo Phd, seorang penulis, pembimbing dan penggiat budaya di jamannya. Bu Saraswati dahulunya adalah aktifis hak asasi manusia di Jakarta, anggota Teater Bulungan dan Teater Keliling, yang menerima ancaman dari rezim orba dan akhirnya harus pergi untuk tinggal di Amerika. Beliau lalu sempat mengajar gender studies di University of Washington, Seattle, namun akhirnya berhenti karena masalah kesehatan dan sekarang bekerja sebagai perawat kebun. Harapannya hibah ini dapat mendukung komitmen waktu bagi saya dan Ibu Saraswati sehingga kami berdua dapat fokus kepada pengerjaan karya tulis. Sebagian dari dana juga akan membantu biaya proses rekaman album akustik bagian dari karya ini di Malang. Keutuhan karya Perbatasan diharapkan selesai dan siap disebarkan di tahun 2019."
kategori: lintasgenerasi
latar_belakang: "Selama proses berkarya, saya mendapati banyak ketidaksetaraan dan stereotipe terhadap perempuan. Ketika memutuskan untuk berkarya secara internasional, ketidaksetaraan tersebut semakin jelas dan menimbulkan stres berkepanjangan yang akhirnya mempengaruhi kesehatan secara fisik. Tantangan seperti ini tentu tidak hanya terjadi pada saya. Meskipun permasalahan ini belum terselesaikan secara global, saya menemukan titik-titik keberhasilan dalam meretas batas dan menolak ketidaksetaraan yang layak untuk dibagi ceritanya kepada perempuan lain terutama yang bergerak di bidang seni. Saya juga berharap buku musikal ini dapat menambah kesadaran akan ketidaksetaraaan gender ke level yang lebih luas dan tidak terbatas untuk perempuan saja. Dengan memilih Ibu Saraswati sebagai mentor, saya juga ingin menambah pengetahuan tentang isu perempuan Indonesia menurut pandangan kaum akademis. Selain itu minat membaca di Indonesia sebagai negara berbudaya tutur juga sangat kurang. Dengan mengemas bacaan yang lebih dari sebuah buku, saya berharap strategi ini dapat ikut andil tidak hanya untuk menambah minat membaca, namun juga menambah daftar nama penulis perempuan Indonesia."
masalah: "- Ketidaksetaraan gender di dunia seni,
- Keterbatasan hak untuk berpindah-pindah sebagai perempuan Indonesia,
- Keterbatasan dalam berekspresi sebagai perempuan,
- Kurangnya kolaborasi antar perempuan dari berbagai latar belakang yang berbeda,
- Kurangnya minat membaca di Indonesia,
- Daftar pendek nama penulis perempuan Indonesia."
durasi: 9 bulan
sukses: "- Selesainya buku dan album ini dalam waktu kurang dari satu tahun,
- Jumlah penjualan buku dan album ini di paska produksi,
- Jumlah dukungan dari berbagai pihak lintas generasi dan gender,
- Kontinuitas karya tulis oleh penulis perempuan yang merdeka"
dana: '160'
approved_dana: '50'
submission_id: 5ab0105704d9e1774164f0ee
---
Proyek ini adalah inisiatif dua musisi-aktivis perempuan berbeda latar belakang dalam belajar sambil menggagas karya secara lintas generasi. Inisiatif ini adalah gagasan yang segar di tengah status quo industri musik yang tak pernah ramah perempuan, di situasi kesenjangan pengetahuan antar generasi dan di krisis alih pengetahuan antar gerakan sosial perempuan. Nova Ruth, musisi muda yang selalu mencari bentuk baru kekaryaan sekaligus juga menyoal isu-isu lingkungan dan tradisi. Saraswati Sunindyo, maestro musik tradisi dengan filosofi sufi, akademisi yang juga berpengalaman dalam aktivisme hak asasi manusia. Keduanya memadukan talenta dan pengetahuan dalam proses belajar sekaligus produksi. Proses eksperimentasi praktek pengetahuan lewat kekaryaan ini harapannya menghasilkan corak karya baru yang menginspirasi baik musisi, aktivis, komunitas-komunitas seni budaya, dan pencinta musik lintas generasi.
