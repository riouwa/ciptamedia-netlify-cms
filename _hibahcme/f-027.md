---
nomor: f-027
nama: Jacoba Vera Yamanop
foto:
  filename: f-027-jacoba.jpg
  type: image/jpeg
  size: 1515493
  url: /hibahcme/profile-pict/f-027-jacoba.jpg
seni: kriya
pengalaman: 8 Tahun
website:
sosmed:
file:
  filename: f-027-karya-jacoba.pdf
  type: application/pdf
  size: 251152
  url: /hibahcme/contoh-karya/f-027-karya-jacoba.pdf
title: Penerapan Desain Motif Dekoratif Pada Media Kain
lokasi: Jl. Asrama Koramil Hawai Sentani, Papua
lokasi_id:
deskripsi: >-
  Proyek penerapan desain motif dekotartif pada media kain adalah kegiatan membuat motif dan mengaplikasikannya pada media kain.Kain yang telah memimiliki motif tersebut dapat dijadikan aneka produk. Produk yang dihasilkan akan dipamerkan dan fokus pada produk perlengkapan ruang tamu seperti sarung bantal dan taplak meja. Pameran akan dilaksanakan selama tiga hari.
kategori: kerjasama_kolaborasi
latar_belakang: "Saya ingin mengaplikasikan motif-motif desain saya pada media kain. Selama ini saya hanya membuat desain motif pada media kertas. Keinginan saya untuk membuatnya pada media kain terhalang dana untuk membeli bahan-bahan penunjang terutama karena beberapa bahan tidak tersedia di Jayapura. Harapannya setelah mendapatkan dana hibah saya dapat mewujudkan mimpi saya itu dan membuat produknya. Tahap selanjutnya saya dapat mengadakan pameran dan dapat memberikan informasi kepada masyarakat tentang karya seni ini." 
masalah:
durasi: 6 bulan
sukses: "Indikator sukses yang diharapkan dari proyek ini:
  1. Pemahaman masyarakat tentang penerapan desain motif pada kain.
  2. Produk kain bermotif untuk ruang tamu (sarung bantal dan taplak meja)
  3. Brosur produk.
  4. Pameran hasil karya selama 3 (tiga) hari."
dana: '54.000'
---
