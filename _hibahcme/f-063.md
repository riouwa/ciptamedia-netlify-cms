---
nomor: f-063
nama: Hendriyetty Rambu Boba
foto:
  filename: f-063-hendriyetty.jpg
  type: image/jpeg
  size: 268110
  url: /hibahcme/profile-pict/f-063-hendriyetty.jpg
seni: kriya
pengalaman: Baru memulai
website:
sosmed:
file:
title: Dari Perempuan untuk Perempuan
lokasi: Ds. Anajiaka, Kec. Umbu Ratu Nggay Barat - Kab. Sumba Tengah
lokasi_id:
deskripsi: >-
  Mengangkat kembali peran periuk tanah liat di Kabupaten Sumba Tengah.
kategori: kerjasama_kolaborasi
latar_belakang: "Kerinduan melihat alat dapur yang vital di masa kecil terdokumentasikan lewat essay cerita foto."
masalah: >-
  Periuk tanah liat atau dalam bahasa Sumba disebut "ARRU TANA" pada jaman dulu merupakan alat sentral dlam kehidupan dapur atau juga dalam pelbagai aktifitas kehidupan masyarakat Sumba.
  Sebagai media untuk kesehatan herbal, periuk tanah liat juga memiliki peran penting untuk menjadi tempat penyimpanan obat, lalu dalam keperluan menimba air, periuk tanah liat ini juga lah yang menjadi andalan masyarakat untuk mengangkut air dari sumber ke rumah.
  hal-hal itulah yang menjadi dasar untuk pengajuan proposal ini. Saya merasa perlu bekolaborasi dengan penduduk yang masih menekuni pembuatan periuk tanah liat ini secara manual.
  ada dua tempat di Kabupaten saya yang masih melestarikan pembuatan periuk tanah liat ini, yang ingin saya angkat sebagai penghargaan akan masa kecil saya yang organic dari alat alat masak di dapur keluarga saya di kampung yang kini telah tergantikan dengan alat-alat masak dari aluminium. Sekarang, hanya tersisa dua tempat yang menekuni ini yaitu di Desa Praimadeta dan Desa Umbu Jodu. Saya berharap, lewat masyarakat di dua tempat inilah keragaman yang ada di daerah saya ini dapat saya angkat pesonanya lewat karya essay foto, memunculkan mereka ke dunia luas dengan dukungan yang ada lewat proyek dana hibah yang telah panitia sosialisasikan di Sumba Barat yang lalu.
durasi: 6 bulan
sukses: "80% sukses karena ini kerinduan saya yang terpendam selama ini. Proyek ini akan membuatnya terealisasi."
dana: '27'
---
