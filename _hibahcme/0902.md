---
permalink: /hibahcme/902
nomor: 902
nama: Cindy Hapsari Novrita
foto:
  filename: Cin Hapsari Tomoidjojo.jpg
  type: image/jpeg
  size: 491206
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/4c1b8db4-6eda-4bc9-b8fa-3c899d62450e/Cin%20Hapsari%20Tomoidjojo.jpg
seni: sastra
pengalaman: Mulai sekitar thn 2005 & sudah terlalu lama vakum. Karya terakhir ‘Jawa-Islam-Cina; Politik Jatidiri dalam Jawa Safar Cina Sajadah’  diterbitkan o/ WWS, 2012.
website: '-'
sosmed: '-'
file:
  filename: Cin Hapsari Tomoidjojo CV.pdf
  type: application/pdf
  size: 4550552
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/eb036c08-eb25-4ca1-b6f5-3919522f48ea/Cin%20Hapsari%20Tomoidjojo%20CV.pdf
title: DIVINE WISDOM ORACLE CARD  for Mental Health Project
lokasi: Jogjakarta
lokasi_id: ''
deskripsi: Fenomena gangguan jiwa bukan lagi sesuatu yang asing. Bahkan tanpa harus menunjuk hidung orang lain, kita sendiri pasti pernah mengalami kegagapan dalam hidup yang pada akhirnya melahirkan krisis. Berangkat dari fenomena tersebut kami berupaya untuk membuat sebuah alat bantu yang dapat mendukung proses self-healing.  \r\n\r\nKami percaya sejatinya manusia dibekali kemampuan untuk menyembuhkan diri. Oracle card kami pilih karena dari studi psikologi, simbol diyakini sebagai bahasa yang digunakan bawah sadar. Dengan menerobos masuk ruang tersebut, kita memiliki kesempatan untuk membangun life script baru yang berdaya-guna serta mampu melepas beban ingatan yang merintangi perkembangan diri.\r\n\r\nNiat pembuatan oracle ini sebenarnya sempat terlintas ketika Cin membuat ilustrasi buku ‘Nyanyian Kedamaian’ karya Guru Gede Prama  dan cover  ‘Jalur Tua Awan Putih’ milik Master Zen Thich Nhat Hanh. Perjumpaan dengan guru simbolik itu menyibak kenyataan jika banyak kebijaksanaan dan nilai-nilai luhur yang tak terhubung dengan baik dengan orang-orang yang mengalami terpaan dalam hidup. \r\n\r\nProject ini melibatkan empat orang perempuan dengan latar belakang berbeda. Seorang tenaga pendidik yang sempat menjadi project manager sebuah NGO di Jogja, seorang perempuan Tionghoa berkulit coklat dengan penyakit yang belum tersedia obatnya, seorang artis muda berbakat dan seorang yang sempat aktif di dunia media (penulisan, riset & kreatif).\r\n
kategori: kerjasama_kolaborasi
latar_belakang: '"The mass of men lead lives of quiet desperation" --- Thoreau.\r\n\r\nStatistik WHO (2016) menunjukkan peningkatan penderita gangguan kejiwaan. 450 juta orang di dunia mengalami gangguan mental dan 1 juta orang tiap tahun melakukan bunuh diri. Indonesia sendiri secara global berada di urutan ke-4 dalam daftar negara-negara dengan tingkat depresi paling tinggi di dunia dan di urutan ke-6 dalam hal gangguan kesehatan mental dan kejiwaan secara keseluruhan (usnews.com).  \r\n\r\n‘Jatah masalah’ memang tak tertolakkan adanya. Tetapi masalah yang lahir atas nama kekerasan jelas tak dapat dibiarkan. Kekerasan dalam berbagai bentuknya dapat melahirkan depresi, trauma, suicide dan bukan tak mungkin melahirkan anomali kepribadian. Tengok saja, seorang wanita berbelanja di  apotek tanpa busana; seorang ibu meniadakan anak balitanya dengan semprotan nyamuk karena sang anak sering ngompol; seorang perempuan muda harus mengalami dissociative identity disorder (D.I.D) dengan 10 kepribadian akibat bullying sekaligus kekerasan yang dilakukan ayah kandungnya.\r\n\r\nSelain itu, kami sendiri menyaksi betapa banyak orang disekeliling kami yang tenggelam, hancur luluh dan nyaris tanpa mendapat penerangan dari lentera jiwa ataupun dukungan kelompok profesional. Hal yang sama terjadi dengan siswa yang melakukan tindak kekerasan ataupun siswi yang hamil di luar nikah. Betapa mengharukan jika ternyata penyebabnya adalah ‘kerinduan akan rumah’.'
masalah: Minimnya tools dan wacana self-help menjadi fokus perhatian kami. Bagi kami, adalah penting untuk membangun swadaya dari dalam diri melalui langkah-langkah yang tepat dan tidak sembrono. Pada kenyataannya, tidak semua orang yang mengalami gangguan kejiwaan memiliki keberanian untuk memeriksakan diri. Dan banyak pula dari mereka yang tidak pernah menyampaikan permasalahan yang mereka hadapi. Mereka memendam semua hal seorang diri. Oracle card dapat berperan sebagai media self-help dan dapat diposisikan sebagai upaya preventif yang mampu menghindarkan seseorang dari keinginan destruktif yang merugikan diri ataupun lingkungannya.\r\n\r\nSelain itu, sedikitnya tenaga medis yang tersedia menjadi catatan tersendiri bagi kami. Oleh karena itu adalah baik untuk membekali lebih banyak orang yang peduli pada isue kesehatan mental dengan tools yang mendukung. Kami berharap persebaran oracle ini dapat berguna bagi para fasilitator maupun konselor yang mengangani kasus-kasus tersebut.\r\n
durasi: 9 bulan
sukses: 'Dikenal dan diterimanya oracle card tersebut sebagai salah satu medium self-help ataupun alat bantu konseling, baik oleh masyarakat umum maupun kelompok profesional. Selain itu kami juga berharap langkah ini menjadi awalan yang baik bagi perkembangan wacana self-help dan lahirnya kesadaran untuk membangun swadaya diri. '
dana: '333'
---
