---
permalink: /hibahcme/861
nomor: 861
nama: Diyah Wara Restiyati
foto:
  filename: Pas Foto.JPG
  type: image/jpeg
  size: 76966
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/011d7b65-4b0e-451f-b70c-c020661c969c/Pas%20Foto.JPG
seni: penelitian
pengalaman: 3 tahun
website: 'www.diyahwararestiyati.wordpress.com, http://www.kompasiana.com/didy/59c279ec4d11b773bb41e242/indahnya-batik-banyumas, https://www.kompasiana.com/didy/59f6aec38dc3fa14f6422252/rempah-berharga-dari-nusantara-yang-terlupakan, https://www.kompasiana.com/didy/5a7ad6d65e13734c3b3f6e12/menelisik-tenun-tanimbar, '
sosmed: 'instagram @waradiyah, facebook : diyahwara '
file:
  filename: Chiouthau53.JPG
  type: image/jpeg
  size: 703052
  url: http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/2653da89-9e29-4f38-ad18-d509dc6e66f4/Chiouthau53.JPG
title: The Wonderful Batik of Banyumas
lokasi: Banyumas, Jawa Tengah
lokasi_id: ''
deskripsi: 'Project ini merupakan penelitian mengenai batik Banyumas, Jawa Tengah, dengan metode antropologi. Penelitian ini akan dibukukan menjadi buku antropologi visual dengan bahasa populer. '
kategori: riset_kajian_kuratorial
latar_belakang: Batik Banyumas memiliki kekhasan tersendiri, dengan motif-motif yang tidak ditemukan di tempat lain. Dipengaruhi oleh batik dari Solo dan Yogyakarta, namun batik Banyumas juga dipengaruhi oleh Batik Pesisir seperti Pekalongan karena akulturasi yang terjadi di Banyumas, dimana suku Jawa dan Tionghoa tidak berbeda, dan membaur serta berakulturasi secara alami. Akulturasi tersebut tergambar dalam batik Banyumas.
masalah: 'Pelestarian Batik Banyumas sudah sering di angkat untuk dijadikan seragam pegawai pemerintahan Banyumas, namun sayangnya literasi atau buku mengenai Batik Banyumas yang berdasarkan penelitian mendalam mengenai sejarah, makna, filosofi dan kehidupan para pembatik nya belumlah ditemukan. Penelitian ini akan mengangkat mengenai Batik Banyumas dari perspektif Antropologi.   Buku ini diharapkan menjadi standar bagi semua pihak yang peduli dengan pelestarian Batik Banyumas. '
durasi: 3 bulan
sukses: 'Adanya data-data yang bisa dituliskan untuk dimasukkan ke dalam sebuah buku. '
dana: '100'
---
