---
nomor: 1206
nama: Chesa Nuria Rahmi
foto:
  filename: d1b3e547-5211-4fc8-9c09-73f758ec2c33.jpg
  type: image/jpeg
  size: 105131
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/72e0939a-8df0-42d6-8e2c-9354da5594ee/d1b3e547-5211-4fc8-9c09-73f758ec2c33.jpg
seni: lainnya
pengalaman: 3 tahun
website: ''
sosmed: '@chesanoeria'
file:
  filename: works sample & pictures of indigenous tribe Mentawai.pdf
  type: application/pdf
  size: 2710022
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/580ed9a9-9e69-4a6e-8b6f-06191b57d987/works%20sample%20&%20pictures%20of%20indigenous%20tribe%20Mentawai.pdf
title: MAYARAKAT MENTAWAI (INDIGENOUS TRIBE MENTAWAI)
lokasi: DI Yogyakarta
lokasi_id: Q3741
deskripsi: >-
  Output dari gagasan ini adalah pameran dan pertunjukan yang kompleks
  menjangkau semua aspek seni dan budaya mentawai yang terancam punah, akan saya
  kemas sedemikian rupa, agar mampu menjangkau banyak pihak, terutama kaum muda
  yang produktif. Acara ini akan direncanakan terlaksana pada Januari 2019 di
  Sangkring Art Space Yogyakarta selam 5 hari, acara tersebut diantaranya;
  Pameran foto, Pameran dan Peragaan Busana Mentawai oleh saya sendiri,
  Pertunjukan Tattoo Tradisional Mentawai oleh Durga Sipatiti, Pertunjukan Musik
  dan Tari oleh suku asli Mentawai, Pemutaran Film oleh As World Divide,
  Workshop kain dan aksesoris Mentawai oleh saya dan seorang pengrajin asli
  Mentawai, Diskusi Budaya Mentawai oleh pihak-pihak yang terkait. Acara ini
  juga akan mendatangkan beberapa lapak organisasi yang mendukung pelestarian
  kearifan lokal mentawai seperti Yayasan Pendidikan Mentawai, Mentawai
  Ecotourism, As Worlds Divide, dan lapak kerajinan tangan Mentawai. Proyek dan
  gagasan ini melibatkan banyak sekali pihak, dari dokumentasi, seniman tua,
  Kurator, masyarakat suku asli Siberut , organisasi yang sudah lebih dulu
  mencanangkan pelestarian kearifan lokal Siberut, teman-teman seniman muda, dan
  para pengrajin. Sebelum melalui tahap produksi kreatif, saya perlu mebekali
  diri untuk datang langsung ke pulau Siberut mentawai, melakukan residensi
  selama sebulan lebih, turunlangsung untuk menjawab pertanyaan saya yang tidak
  ditemukan di buku atau artikel terkait
kategori: lintasgenerasi
latar_belakang: >-
  Gagasan ini saya rancang dari satu tahun yang lalu, awalnya saya hanya ingin
  tahu tentang busana mentawai untuk saya adaptasi dalam rancangan busana saya,
  lalu setelah membaca beberapa referensi, saya jatuh cinta dengan seni dan
  budaya mentawai, tidak adil jika saya lakukan ini hanya untuk diri saya
  sendiri. Lewat kesempatan ini saya merancang gagasan baru untuk melaksanakan
  proyek yang menjangkau seluruh aspek dan seni budaya, namun dapat dikonsumsi
  oleh semua orang dengan renyah, agar tujuan dari proyek ini tidak sia-sia.
  Durga adalah seniman tattoo yang perannya untuk memajukan seni dan budaya
  tattoo tradisional sudah dikenal dimancanegara, dengan membawa Durga ikut
  serta itu berarti saya dapat semakin memotivasi orang lain untuk lebih banyak
  datang dan menyuapi mereka dengan energi-energi baru dan positif lewat acara
  ini, sayapun sudah melakukan brainstorming dengan Durga, sangat luar biasa,
  dukungan yang saya dapat tidak pernah saya bayangkan sebelumnya, walaupun
  banyak pihak lain yang mencibir dan meremahkan saya saat saya mencoba
  melakukan brainstorming tentang gagasan ini. Inti dari gagasan ini bagi saya
  adalah bagaimana peran perempuan disana? Selain mereka mengasah gigi dan
  mengasuh anak, ini harus saya jawab sendiri agar saya dapat menghasilkan
  output yang berguna bagi orang lain selain hanya menarik mereka untuk hadir
  agar tau.
masalah: "1.\tSeni dan Budaya dengan keanekaragaman hayati dan ekosistem yang terancam punah \r\n2.\tPeran pemerintah yang mengatasnamakan pariwisata dan hubungannya dengan investor pembangunan\r\n3.\tTatto tradisonal sebagai busana suku asli mentawai adalah ttaoo tertua didunia yang dipandang sebagai aspek kriminal di era modernitas\r\n4.\tMenggali peran perempuan dalam kehidupan di mentawai yang terkubur oleh masalah-masalah general di Mentawai\r\n5.\tPembuatan busana dan aksesoris kerajinan mentawai harus didukung untuk membangun industri kreatif perbaikan ekonomi masyarakat di pedalaman mentawai\r\n6.\tPola pikir generasi modern dalam peran memepertahankan warisan budaya ataupun melestarikannya dengan cara yang ber etika.\r\n"
durasi: 9 bulan
sukses: "Jangka pendek; proyek ini mampu mendatangkan lebih dari 500 orang untuk datang  agar tau, agar mengenali budaya yang ada di Mentawai, dan visi-visi dari organisasi yangikut melestarikan kebudayaan Mentawai.\r\n\r\nJangka panjang; 1.  proyek ini mampu memotivasi pengunjung yang datang untuk merespon kebudayaan atau aspek lain yang ada di Mentawai dengan menghasilkan karya baru, gagasan baru, penelitian baru untuk dihadirkan kepada orang lain. 2. Dengan adanya gagasan yang mempertanyakan peran perempuan di Mentawai, dapat merespon peneliti lain untuk menggali lebih dalam agar lebih banyak referensi yang dapat dikaji 3. Masyarakat yang ingin melakukan kunjungan wisata atau penelitian dapat denganbijak memilikh tour guide agar tidak merugikan wisatawan dan juga masyarakt lokal Siberut. 4.persepsi masyarakat mengenai seni tattoo tradisional dapat berubah, agar kelestariannya tidak punah.\r\n5. pemerintah memikirkan ulang mengenai kebijakan dalam pengolahan hutan dan cara memajukan pariwisata agar dapat memerhatikan kesejahterahan masyarakat lokal.\r\n"
dana: '680'
submission_id: 5ab7b1b6881ebd6635657880
---
