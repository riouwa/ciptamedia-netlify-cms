---
nomor: 1231
nama: SYF. RATIH KOMALA DEWI
foto:
  filename: 20171026_051743.jpg
  type: image/jpeg
  size: 22844
  url: >-
    http://5c4cf848f6454dc02ec8-c49fe7e7355d384845270f4a7a0a7aa1.r53.cf2.rackcdn.com/f1436398-919b-4eb5-bd9b-aa80ceb38c93/20171026_051743.jpg
seni: penelitian
pengalaman: 3 Tahun
website: www.lpsair.org
sosmed: >-
  http://www.worldcat.org/title/mencari-ruang-publik-di-warung-kopi-fenomena-warung-kopi-dan-es-teler-di-kota-pontianak/oclc/682831289
file: null
title: jejak seni dan budaya kajian Perempuan di Kalimantan Barat
lokasi: Kalimantan Barat
lokasi_id: Q3916
deskripsi: >-
  Melalui pendekatan analisis kajian penelitian ini menggali potensi tersembunyi
  dari peran perempuan di ranah seni tradisi dalam membangun ruang budaya di
  bumi Kalimantan Barat khususnya. Teruntuk membangun pengetahuan melalui seni
  tradisi yang dihidupi oleh perempuan.   Aspek budaya dan peran perempuan dalam
  merawat kehidupan di tengah hingar bingar kapitalisasi budaya yang mendera,
  yang seharusnya dapat menjadi landasan bagi gerakan kebudayaan seringkali
  terabaikan. Peran perempuan di ranah seni memiliki nilai-nilai yang dapat
  digunakan sebagai sumber inspirasi melalui merekam seni budaya kolektif.
  Tradisi-tradisi akan tergerus bila peran dan tantangan mereka tidak tercatat
  dan terakomodir dengan baik, Penelitian ini bertujuan untuk mengumpulkan suara
  budaya yang “terpinggirkan” dan peran perempuan dengan membangun dan
  mensosialisasikan pengetahuan baru tentang peran perempuan melalui seni
  tradisi.
kategori: riset_kajian_kuratorial
latar_belakang: >-
  Peneliti sekaligus Pegiat Kebudayaan saya melihat banyak hal dalam ranah seni,
  tradisi dan budaya yang perlu diangkat untuk mendukung khasanah literasi.
  Salah satunya adalah menggali aspek seni tradisi yang dikembangkan oleh
  perempuan-perempuan di akar rumput. Saya mengangkat situasi kreativitas
  perempuan dalam bentuk ensiklopedi dengan upaya mencatat sejarah. Persoalan
  keberagaman budaya dan rentang wilayah juga menjadi tantangan. Masuknya
  kapitalisme budaya global terkikisnya kekayaan budaya akan keberagaman suku
  dan seni tradisi. Tentu saja ini mengancam proses keberlanjutan budaya dan
  literasi. 
masalah: >-
  Dalam hal ini, tidak tercatatnya secara buku besar peran perempuan di ranah
  seni, budaya serta kreatifitas dalam naungan tradisi yang merupakan sumber
  kekuatan budaya. Pemantapan ensiklopedi peran perempuan dalam peradaban budaya
  melalui seni tradisi di beberapa lokasi di Kalimantan Barat. Mengangkatnya
  melalui dokumentasi dalam buku, film documenter, maupun video treaser.
  Momentum hasil karya ini dengan adanya launching dan sosialisasi penerbitan
  buku dengan diiringi film documenter dan video treaser yang menampilkan seni
  tradisi perempuan dari daerah penelitian guna penyebaran pengetahuan
  perempuan, sekaligus keberlanjutan fundraising.
durasi: 8 bulan
sukses: "1. Adanya ensiklopedi dari catatan kritis-analitis tentang pemikiran dan peran perempuan dalam ranah seni dan budaya yang mengcover tradisi di Kalimantan Barat.\r\n 2. Terbangunnya gerakan literasi yang dapat mengembangkan tradisi kebudayaan melalui FGD keterlibatan pemuda. Konsolidasi dengan pemerintah, pemuka agama, pegiat seni budaya, tokoh adat, peneliti, akademisi dan aktivis di lintas daerah dan disiplin ilmu dalam kegiatan-kegiatan seni, budaya dan kreatifitas.\r\n3. Tersosialisasikannya peran dan suara perempuan dalam seni tradisi melalui diskusi buku, film dokumenter dan video treaser di sekolah, kampus, media dan ruang publik lainnya. \r\n4. Adanya fundraising yang berkelanjutan dari kegiatan dan penjualan buku melalui LPS AIR Publishing--sebagai pemantik gerakan seni tradisi perempuan dari buku dan film yang dihasilkan dari penelitian ini. \r\n5. Upaya memetakan peran perempuan melalui seni, budaya dan kreativitas dengan dokumentasi dan kegiatan ini.\r\n"
dana: '200'
submission_id: 5ab7bba2bcd8e8450b3072b8
---
