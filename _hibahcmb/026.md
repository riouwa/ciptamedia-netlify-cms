---
title: Solusee.com
date: 2011-09-16 11:08:00 +07:00
nomor: '026'
foto: /static/img/hibahcmb/026.jpg
nama: Sony Arianto Kurniawan
lokasi: Jakarta
dana: 92 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: 2011 – 2013 (sebelum pemilu 2014)
deskripsi: Sebuah layanan search engine untuk pencarian alamat, telepon, dan informasi
  untuk area Indonesia
masalah: Belum ada search engine yang cocok dan komprehensif di Indonesia
solusi: Mebuat search engine solusee.com yang akan terus dikembangkan. Pihak yang
  akan diuntungkan melalui proyek ini adalah seluruh rakyat Indonesia
sukses: Semakin banyak pengguna website ini dan ada feedback positif yang masuk
organisasi: Solusee
target: Semua orang
---


### {{ page.nohibah }} - {{ page.title }}

---
