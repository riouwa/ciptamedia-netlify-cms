---
title: Reboisasi dan Kemanusiaan Bencana
date: 2011-09-16 11:08:00 +07:00
nomor: 723
foto: /static/img/hibahcmb/723.jpg
nama: Jaringan Advokasi Bencana Geologi Papua
lokasi: Nabire, Papua
dana: 550 Juta Rupiah
topik: Pemantauan media
durasi: 5 oktober – 29 November
deskripsi: |-
  Reboisasi:
  proyek Reboisasi kami akan lakukan beberapa kabupaten baru di tanah Papua, khususnya Papua Tengah

  Proyek Kemanusiaan Bencana:
  Ini kami akan memberikan kepada masyarakat yang mengalami akibat bencana geologi di tanah Papua
masalah: Bencana antropogenik di tanah papua seperti banjir, longsor, gerakan tanah,
  penebangan hutan secara liar, akibat penambangan dll.
solusi: |-
  Kami akan kerjasama dengan stake holder yang ada dan kepala-kepala suku akar rumput.
  Pihak yang diuntungkan adalah demi masyarakat akar rumput, di daerah papua khususnya nabire, paniai, deiyai, dogiyai dan intan jaya.
sukses: Memberikan pandangan melalui seminar dan turun kerja di lapangan.
organisasi: Jaringan Advokasi Bencana Geologi Papua
target: kegiatan ini demi masyarakat akar rumput, di daerah papua khususnya nabire,
  paniai, deiyai, dogiyai dan intan jaya
---


### {{ page.nohibah }} - {{ page.title }}

---
