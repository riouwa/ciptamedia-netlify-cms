---
title: Petani Membangun Gerakan Aksi Advokasi Media
date: 2011-09-16 11:08:00 +07:00
nomor: 172
foto: /static/img/hibahcmb/172.jpg
nama: Mediyansyah
lokasi: 'Surakarta, Jawa Tengah '
dana: 200 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: Januari – Desember 2012 (1 tahun)
deskripsi: Proyek ini memfasilitasi warga desa (petani) agar memiliki kapasitas dan
  kapabilitas untuk mampu proaktif dalam arus informasi menuju masyarakat yang tidak
  sekadar sebagai konsumen pasif media yang ada, tetapi juga  berlaku sebagai warga
  yang sadar hak-haknya, serta mampu berperan dalam arus informasi yang sehat. Proyek
  ini menyiapkan dan mendampingi warga pedesaan atau petani untuk dapat berperan memanfaatkan
  media yang ada, dapat melakukan analisis isu, memetakan stakeholder (termasuk media),
  dan mengangkat isu/persoalan riil masalah-masalah sosial/pembangunan di desa mereka
  itu, dalam sebuah aksi/advokasi media
masalah: Warga pedesaan, terutama petani dengan segala masalahnya, sering luput dari
  agenda pemberitaan; kurangnya pemberitaan mengenai masalah sehari-hari di daerah
  pedesaan, warga desa hanya dijadikan sekedar konsumen pasif media, kesetaraan kases
  informasi belum mendapatkan perhatian serius,  serta kurangnya kapasitas dan kapabilitas
  warga desa untuk bersikap proaktif dalam arus informasi, termasuk mengenai masalah
  pembangunan
solusi: Warga pedesaan, terutama petani dengan segala masalahnya, sering luput dari
  agenda pemberitaan; kurangnya pemberitaan mengenai masalah sehari-hari di daerah
  pedesaan, warga desa hanya dijadikan sekedar konsumen pasif media, kesetaraan kases
  informasi belum mendapatkan perhatian serius,  serta kurangnya kapasitas dan kapabilitas
  warga desa untuk bersikap proaktif dalam arus informasi, termasuk mengenai masalah
  pembangunan
sukses: Adanya sejumlah warga desa/petani yang mengikuti pelatihan (teknik &
  etik) jurnalistik, adanya hasil analisis terhadap masalah-masalah pembangunan pedesaan/
  pertanian atau masalah publik lain yg ada di desa, tersusunnya rencana aksi/advokasi
  media yang dibuat warga desa/petani, untuk masalah - masalah publik di pedesaan
  tersebut, berjalannya aksi/advokasi media yang dilakukan warga desa/petani, dan
  pada akhir proyek, setiap desa sasaran memiliki sebuah contoh (pengalaman) melakukan
  aksi/advokasi media yang dilakukan petani secara mandiri, terhadap problem publik
  yang ada di desa
target: Penerima manfaat langsung adalah warga pedesaan/petani yang berada di
  30 desa di lokasi Eks-Karesidenan Surakarta, yakni Kabupaten Wonogiri, Sukoharjo,
  Karanganyar, Sragen, Boyolali dan Klaten, yang akan dipilih 5 desa dari masing-masing
  kabupaten
organisasi: Yayasan Duta Awam Solo (YDA Solo)
---


### {{ page.nohibah }} - {{ page.title }}

---
