---
title: Rockto.com – Cara Seru Berbagi Rekomendasi
date: 2011-09-16 11:08:00 +07:00
nomor: 269
foto: /static/img/hibahcmb/269.jpg
nama: PT Rocktokom
lokasi: Depok
dana: 400 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Januari – Desember 2012
deskripsi: Sebuah wadah rekomendasi sosial yang bertujuan menyebarkan rekomendasi-rekomendasi
  yang berguna bagi para pengguna sesuai dengan minat dan kebutuhan mereka masing-masing.
  Selain menyebar di dalam Rockto, rekomendasi juga dapat tersebar ke akun jejaring
  sosial media masing-masing pengguna (facebook/twitter) sehingga efeknya akan sangat
  besar
masalah: Seiring dengan kemajuan teknologi saat ini, informasi sangat mudah didapatkan.
  Namun, terlalu banyak informasi di luar sana mengakibatkan masyarakat kesulitan
  untuk memilah dan mendapatkan informasi yang sesuai dengan minat dan kebutuhan mereka
solusi: Rockto memilah dan mengelompokkan informasi berdasarkan topik yang dapat ditentukan
  oleh pengguna sehingga memudahkan pengguna lain dalam menerima informasi. Proyek
  ini akan memberi keuntungan kepada pengguna usia 18-30 tahun, pemilik bisnis, dan
  media
sukses: Rockto dapat menjadi pilihan utama bagi masyarakat untuk mendapatkan
  informasi yang sesuai dengan minat dan kebutuhan
organisasi: PT Rocktokom
target: Pengguna usia 18-30 tahun, pemilik bisnis, dan media
---


### {{ page.nohibah }} - {{ page.title }}

---
