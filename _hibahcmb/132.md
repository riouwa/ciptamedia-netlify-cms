---
title: UU & Kebijakan Kehutanan dalam bahasa daerah
date: 2011-09-16 11:08:00 +07:00
nomor: 132
foto: /static/img/hibahcmb/132.jpg
nama: Didin Alfaizin
lokasi: Makassar, Sulawesi Selatan
dana: 500 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: November 2011 - April 2012
deskripsi: Sebuah proyek penerjemahkan UU dan Kebijakan Kehutanan ke dalam beberapa
  bahasa daerah setempat sehingga nantinya masyarakat yang kurang mampu atau yang
  tidak mengetahui bahasa Indonesia sama sekali dapat mengerti isi dan makna dari
  UU dan Kebijakan tersebut
masalah: Seringnya kemunculan konflik antara masyarakat dan pemerintah (Dinas Kehutanan)
  terkait yang disebabkan oleh ketidaktahuan masyarakat mengenai permasalahan yang
  dihadapi
solusi: Dengan menerjemahkan UU dan Kebijakan mengenai masalah tersebut ke dalam bahasa
  daerah terkait sehingga lebih mudah untuk dipahami dan ditaati. Diharapkan dengan
  adanya proyek ini, jumlah konflik yang muncul dapat ditekan
sukses: Masyarakat memahami isi UU dan Kebijakan sehingga melakukan kegiatan
  yang di sesuaikan dalam UU dan Kebijakan tersebut
target: Masyarakat sekitar hutan di semua kabupaten yang ada di Provinsi Sulawesi
  Selatan
organisasi: Tim Layanan Kehutanan Masyarakat (TLKM)
---


### {{ page.nohibah }} - {{ page.title }}

---
