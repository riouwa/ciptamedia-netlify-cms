---
title: Media Rakyat
date: 2011-09-16 11:08:00 +07:00
nomor: 781
foto: /static/img/hibahcmb/781.jpg
nama: Heru Suprapto
lokasi: Jakarta
dana: 800 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: November 2011 – Mei 2012 (18 bulan)
deskripsi: Aktivitas jurnalistik, fotografi, dan pengembangan media (web, blog, cetak)
  akan difokuskan ke kaum miskin kota (2 kampung) dan 5 sekolah menengah. Bingkai
  Merah selama ini mengorganisir beberapa kampung di Jakarta dan mengajar jurnalistik
  dan fotografi di beberapa sekolah (ekskul dan muatan lokal) di Jakarta. Semua media
  akan dilakoni oleh komunitas dan berjejaring dengan kanal Bingkai Merah untuk mengimbangi
  arus besar media industri. Informasi-informasi yang dikelola dan dikeluarkan berperspektif
  keadilan sosial dan ekonomi, politik rakyat, kesetaraan, dan lingkungan.
masalah: Akses informasi dan media sulit didapat kelompok-kelompok marjinal. Akibatnya,
  informasi yang diterima cenderung merugikan mereka. Media selama ini dimiliki oleh
  kelompok pemodal dan politis sebagai alat reproduksi informasi kepentingan mereka.
  Rakyat yang tidak melek informasi dan tidak berdaulat secara media hanya menjadi
  objek media. Dengan demikian, suara dan tuntutan mereka tidak pernah terakomodir
  media-media mainstream.
solusi: |-
  Membangun media-media rakyat melalui blog dan media cetak yang dikelola oleh rakyat/target.
  Pihak yang diuntungkan adalah kaum miskin kota di dua kampung di jakarta dan pelajar 5 sekolah menengah di Jakarta dan Bekasi.
sukses: |-
  1. Adanya pendidikan jurnalisme dan fotografi di 2 kampung dan 5 sekolah menengah
  2. Adanya absensi, modul, dokumentasi, notulensi
  3. Adanya produk-produk jurnalistik (web/blog, media cetak) dari tiap komunitas.
  4. Adanya web Bingkai Merah
  5. Adanya statistic view web dan blog
  6. Adanya aksi-aksi
  7. Adanya penguatan dan perluasan komunitas
  8. Adanya ruang-ruang diskusi
target: Kaum miskin kota di dua kampung di jakarta dan pelajar 5 sekolah menengah
  di Jakarta dan Bekasi.
organisasi: Bingkai Merah
---


### {{ page.nohibah }} - {{ page.title }}

---
