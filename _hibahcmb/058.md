---
title: Desa Online
date: 2011-09-16 11:08:00 +07:00
nomor: 058
foto: /static/img/hibahcmb/058.jpg
nama: Hasan Kamal
lokasi: Demak
dana: 300 Juta Rupiah
topik: Kebebasan dan etika bermedia
durasi: Januari 2012 – Desember 2013 (24 bulan)
deskripsi: Sebuah proyek untuk membangun website yang memuat berbagai kejadian atau
  ide pengembangan yang terjadi di suatu desa sehingga informasi ini akan segera tersebar
  secara terkini ke seluruh Indonesia
masalah: danya peristiwa-peristiwa di wilayah kecil yang seringkali tidak diangkat
  dan ditutupi. Di samping itu, proyek ini akan mencerdaskan dan mengenalkan teknologi
  kepada masyarakat Indonesia
solusi: Membangun media online yang mampu menghubungkan desa-desa sehingga setiap
  kegiatan dapat ditayangkan secara langsung, serta memberikan akses teknologi bagi
  masyarakat. Pihak yang diuntungkan adalah desa-desa di daerah Demak, Kecamatan Sayung
  yang berjumlah 15 desa
sukses: Adanya aktivitas, laporan, sharing, dan pengumuman desa yang diberikan
  masyarakat desa
organisasi: Peduli Indonesia
target: Desa-desa di daerah Demak, Kecamatan Sayung yang berjumlah 15 desa
---


### {{ page.nohibah }} - {{ page.title }}

---
