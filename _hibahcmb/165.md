---
title: Pelatihan Komputer bagi Masyarakat Sekolah dan Umum yang Belum Melek Teknologi
date: 2011-09-16 11:08:00 +07:00
nomor: 165
foto: /static/img/hibahcmb/165.jpg
nama: Joko Wirstan
lokasi: Bondowoso, Jawa Timur
dana: Rp 100.000.000,00
topik: Pemantauan media
durasi: Setiap bulan
deskripsi: Sebuah proyek yang bertujuan melatih masyarakat sekolah dan umum hingga
  mampu mengoperasikan dan memanfaatkan teknologi
masalah: Kurangnya dana untuk mendukung biaya operasional kegiatan ini
solusi: Bekerja sama dengan instansi dan mitra PLIK tiap Kecamatan di Kabupaten Bondowoso
sukses: Peserta melek teknologi dan bisa memanfaatkannya
target: Masyarakat sekolah dan umum
organisasi: Bondowoso Computer Community (BCC)
---


### {{ page.nohibah }} - {{ page.title }}

---
