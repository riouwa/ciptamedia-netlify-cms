---
title: Penerbitan dan Distribusi Majalah Gratis 3 Versi (Online, Digital, Cetak)
date: 2011-09-16 11:08:00 +07:00
nomor: 195
foto: /static/img/hibahcmb/195.jpg
nama: Ayi Sumarna
lokasi: Bandung, Jawa Barat
dana: 240 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: Oktober 2011 – September 2012 (1 tahun rintisan) untuk selanjutnya diserahkan
  kepada organisasi atau komunitas lokal yang ada
deskripsi: Penerbitan sekaligus distribusi majalah gratis (Majalah Sekebuluh) khususnya
  bagi warga Kampung Sekebuluh Desa Ciburial Kecamatan Cimenyan, Kabupaten Bandung,
  Jawa Barat. Majalah Sekebuluh terdiri dari 3 (tiga) versi, yaitu versi online, versi
  digital, dan versi konvensional (cetak). Tiga versi Majalah Sekebuluh ini dimaksudkan
  agar dapat meningkatkan minat baca dan minat menulis warga kampung Sekebuluh. Majalah
  ini didedikasikan oleh warga Kampung Sekebuluh, dengan slogan dari Sekebuluh, oleh
  Sekebuluh, untuk Indonesia
masalah: Rendahnya tingkat pendidikan masyarakat, rendahnya minat baca, dan budaya
  tulis masyarakat, serta terbatasnya akses masyarakat terhadap akses media cetak
solusi: Menumbuhkan kesadaran akan pentingnya informasi (membangun masyarakat “melek
  informasi”), menampung opini, pendapat masyarakat dan menerbitkannya ke dalam majalah,
  dan menerbitkan serta mendistribusikan majalah secara gratis dalam 3 versi (online,
  digital, dan cetak). Pihak yang diuntungkan adalah warga masyarakat kampung Sekebuluh
  yang mampu membaca aksara latin
sukses: Majalah dapat diterbitkan dan didistribusikan secara gratis kepada masyarakat
organisasi: NA
target: Warga masyarakat kampung Sekebuluh yang mampu membaca aksara latin
---


### {{ page.nohibah }} - {{ page.title }}

---
