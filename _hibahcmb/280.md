---
title: Pembuatan Media Pembelajaran Wirausaha berbasis Potensi Lokal
date: 2011-09-16 11:08:00 +07:00
nomor: 280
foto: /static/img/hibahcmb/280.jpg
nama: Fahmi Tibyan
lokasi: Bojonegoro, Jawa Timur
dana: 100 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: 4 bulan
deskripsi: Banyak pemuda dari desa yang berbondong-bondong ke kota untuk mencari kerja,
  padahal di desa banyak potensi wirausaha yang bisa dikembangkan. Lewat proyek ini
  akan dibuat media pembelajaran dengan format modul dan audio visual untuk membuat
  model pembelajaran kewirausahaan berbasis potensi lokal. Media ini bisa menjadi
  model pembelajaran yang memanfaatkan media AV yang efektif sebagai gerakan memandirikan
  pemuda desa
masalah: Masalah pengangguran bagi pemuda di desa karena keterbatasan model untuk
  melatih jiwa kewirausahaan dan belum adanya model pembelajaran yang efektif lewat
  media audio visual tentang model kewirausahaan bagi pemuda di desa
solusi: Mengindentifikasi potensi lokal yang ada, kemudian membuat konsep, script
  dan story board. Dilanjutkan dengan menyeleksi calon partisipan program, membuat
  model pelatihan kewirausahaan secara komprehensif,  mendokumentasikannya melalui
  modul dan audio-video, dan membuat model pembelajaran audio visual yang efektif.
  Proyek ini akan memberi keuntungan kepada target grup sebanyak 14 pemuda desa yang
  berasal dari sekitar desa Brabowan, Kecamatan Ngase, Kabupaten Bojonegoro
sukses: Pascaprogram pemuda-pemuda desa mempunyai usaha yang produktif dan mampu
  menularkan jiwa wirausaha kepada warga sekitar
organisasi: NA
target: Target grup sebanyak 14 pemuda desa yang berasal dari sekitar desa Brabowan,
  Kecamatan Ngase, Kabupaten Bojonegoro
---


### {{ page.nohibah }} - {{ page.title }}

---
