---
title: Budidaya Jamur Tiram
date: 2011-09-16 11:08:00 +07:00
nomor: 265
foto: /static/img/hibahcmb/265.jpg
nama: Lembaga Pemberdayaan Masyarakat Pondok Kacang Barat
lokasi: Tangerang Selatan
dana: 15 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: September – Desember 2011
deskripsi: Sebuah proyek untuk menstimulasi masyarakat Pondok Kacang Barat untuk memanfaatkan
  lahan terbatas dengan membudidayakan jamur tiram putih untuk memenuhi kebutuhan
  pasar lokal
masalah: Masyarakat memerlukan stimulasi untuk memulai usaha dan diperlukan pilot
  project sebagai stimulan
solusi: Bekerjasama dengan pelaku usaha dan pelaku pasar yang sudah establish dan
  menyediakan lahan usaha untuk pilot project. Proyek ini akan memberi keuntungan
  kepada kelurahan dan masyarakat Pondok Kacang Barat, yang berlokasi di Kecamatan
  Pondok Aren, Kota Tangerang Selatan, Banten
sukses: Berdasarkan hasil penjualan usaha budidaya selama 3 bulan berturut-turut
  dan ada masyarakat yang ikut membuka usaha budidaya sebagai indikator hasil dari
  stimulasi
organisasi: Lembaga Pemberdayaan Masyarakat Pondok Kacang Barat
target: Kelurahan dan masyarakat Pondok Kacang Barat, yang berlokasi di Kecamatan
  Pondok Aren, Kota Tangerang Selatan, Banten
---


### {{ page.nohibah }} - {{ page.title }}

---
