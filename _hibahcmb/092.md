---
title: Paduan Seni Lokal dan Modern
date: 2011-09-16 11:08:00 +07:00
nomor: 092
foto: /static/img/hibahcmb/092.jpg
nama: Ali Usman
lokasi: Jakarta
dana: 2 Milyar Rupiah
topik: Kebebasan dan etika bermedia
durasi: September 2011 - Maret 2012
deskripsi: Sebuah proyek yang memadukan perkembangan teknologi dengan nilai-nilai
  luhur dan kebudayaan asli Indonesia agar kebudayaan tersebut tetap lestari
masalah: Belum adanya perpaduan antara kesenian lokal dengan kemajuan teknologi sekarang
  ini
solusi: Dengan mengadakan tour ke berbagai lembaga sosial untuk meningkatkan capacity
  building dengan bekerja sama dengan ahli kesenian tradisional
sukses: Terbukanya akses baru bagi lembaga sosial terhadap dunia luar
organisasi: GENREZERS CLUB
target: Semua orang, terutama anak jalanan dan anak dampingan lembaga sosial
---


### {{ page.nohibah }} - {{ page.title }}

---
