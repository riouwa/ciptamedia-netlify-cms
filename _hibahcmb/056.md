---
title: Majalah dKmagz (Job and Career)
date: 2011-09-16 11:08:00 +07:00
nomor: '056'
foto: /static/img/hibahcmb/056.jpg
nama: Samuel Wahyu Rahmat Samodro
lokasi: Tangerang Selatan, Banten
dana: 300 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Oktober 2011 – Oktober 2012 (1 tahun)
deskripsi: Sebuah proyek pendirian Majalah dKmagz (Job and Carier) yang sudah berjalan
  11 bulan dengan membagikan majalah gratis (hingga 10 ribu eksemplar) yang berisi
  informasi mengenai info lowongan kerja. Majalah ini tidak sekedar memberikan lowongan,
  tetapi juga arahan dan edukasi kepada calon pekerja. Sasaran utama majalah ini adalah
  lulusan setingkat SMK/D1-3
masalah: Ingin mengatasi lamanya waktu pengganguran, membentuk tenaga kerja Indonesia
  yang lebih bermutu, dan menjadi jembatan antara apa yang diperlukan suatu perusahaan
  dengan dunia pendidikan
solusi: Memberikan informasi mengenai peluang kerja dan dinamika karis di dunia kerja
  sebulan sekali secara gratis sehingga mereka akan mendapatkan bekal tambahan sebelumnya.
  Pihak yang diuntungkan adalah calon pekerja lulusan SMU/SMK dan D1-3/politeknik
  (18-23 tahun) di area Jabodetabek
sukses: Meningkatnya jumlah pencetakkan majalah karena adanya permintaan dari
  SMK dan politeknik
organisasi: PT Ratu Prima Media
target: Calon pekerja lulusan SMU/SMK dan D1-3/politeknik (18-23 tahun) di area
  Jabodetabek
---


### {{ page.nohibah }} - {{ page.title }}

---
