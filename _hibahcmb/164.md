---
title: Media Cetak Suara Rakyat
date: 2011-09-16 11:08:00 +07:00
nomor: 164
foto: /static/img/hibahcmb/164.jpg
nama: Maulana Hasanuddin
lokasi: DKI Jakarta
dana: 100 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: Keadilan dan kesetaraan akses terhadap media
deskripsi: Sebuah media cetak yang bebas meluapkan dan menyampaikan informasi untuk
  kepentingan bersama
masalah: Kurangnya dana, donatur, dan sponsor untuk ciptakan media cetak suara rakyat
solusi: Bersosialisasi pada publik untuk menjadi mitra media dalam terwujudnya media
  cetak suara rakyat
sukses: Menjalin biro usaha dan bermitra kepada setiap masyarakat, instansi
  pemerintah atau swasta
target: Masyarakat Indonesia,terutama di DKI Jakarta
organisasi: DPD PPWI DKI Jakarta
---


### {{ page.nohibah }} - {{ page.title }}

---
