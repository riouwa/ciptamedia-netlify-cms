---
title: Social Networking and Community Web for Moslem
date: 2011-09-16 11:08:00 +07:00
nomor: '027'
foto: /static/img/hibahcmb/027.jpg
nama: Muhamad Ihsan Firdaus
lokasi: Bojongsereh, Bandung
dana: 200 Juta Rupiah
topik: Kebebasan dan etika bermedia
durasi: Agustus 2011 – Agustus 2012
deskripsi: Sebuah proyek yang ditujukan untuk menyediakan berbagai informasi dan artikel
  yang berkaitan dengan kebutuhan umat Muslim dalam sebuah website portal berita Muslim
  berbasis komunitas. Saat ini telah ada 2000 member lebih, tetapi sayangnya website
  ini masih belum mampu memenuhi kebutuhan komunitas.
masalah: Maraknya permasalahan kekerasan dan radikalisme agama yang disebabkan kurangnya
  pemahaman mengenai kedamaian Islam yang sesungguhnya
solusi: Membuat media informasi yang memberikan informasi secara berkala terutama
  untuk kalangan masyarakat Muslim. Pihak yang diuntungkan melalui proyek ini adalah
  masyarakat dan komunitas Muslim di Indonesia
sukses: Banyak member yang tergabung dalam komunitas ini dan tingginya jumlah
  hit pengunjung
organisasi: NA
target: Masyarakat Muslim Indonesia
---


### {{ page.nohibah }} - {{ page.title }}

---
