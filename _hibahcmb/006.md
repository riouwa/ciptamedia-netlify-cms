---
title: Doa (Festival Menyanyi)
date: 2011-09-16 11:08:00 +07:00
nomor: '006'
foto: /static/img/hibahcmb/006.jpg
nama: Fransiska Rosmeri
lokasi: DKI Jakarta Utara
dana: 100 Juta Rupiah
topik: Kebebasan dan etika bermedia
durasi: Oktober 2011 – Februari 2012
deskripsi: Konser atau festival menyanyi di Jakarta
masalah: Sulitnya mengumpulkan dana untuk mengadakan konser atau sebuah festival menyanyi
solusi: Mencari donatur. Yang diuntungkan dalam proyek ini adalah generasi muda (20-40
  tahun) di wilayah Jabodetabek
sukses: Banyak anak muda yang ikut andil dan peduli akan masa depannya
organisasi: Paduan Suara Hosana
target: Generasi muda (20-40 tahun) di wilayah Jabodetabek
---


### {{ page.nohibah }} - {{ page.title }}

---
