---
title: Indonesia Bersih Penipuan
date: 2011-09-16 11:08:00 +07:00
nomor: '053'
foto: /static/img/hibahcmb/053.jpg
nama: Nugroho Praptono
lokasi: Semarang, Jawa Tengah
dana: 10 Miliar Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Saat ini hingga 2015
deskripsi: Sebuah proyek yang mengusung konsep pasar tradisional dimana penjual dan
  pembeli harus saling bertemu dan bergaul terlebih dahulu sehingga transaksi online
  dapat lebih aman dan nyaman
masalah: Membangun Indonesia tahun 2015 yang bebas dari scam dan fraud, serta membangun
  kejujuran masyarakat Indonesia. Juga dilakukan pemantauan terhadap offline seller
  dan menjadikan mereka online seller
solusi: Memberantas penipuan online. Pihak yang diuntungkan adalah semua orang yang
  berjualan
sukses: Dari banyaknya anggota yang bergabung
organisasi: NA
target: Semua orang yang berjualan
---


### {{ page.nohibah }} - {{ page.title }}

---
