---
title: Pancasila di Dadaku Para Penyiar Radio
date: 2011-09-16 11:08:00 +07:00
nomor: 552
foto: /static/img/hibahcmb/552.jpg
nama: Radio Rakosa
lokasi: DI Yogyakarta
dana: 850 Juta Rupiah
topik: Kebebasan dan etika bermedia
durasi: Januari – Desember 2012
deskripsi: Proyek pembekalan dan pemahaman kembali tentang nilai, norma dan etika
  dalam siaran agar memiliki jiwa Pancasila
masalah: Agar program siaran yang disajikan memiliki jiwa persatuan dan bhineka tunggal
  eka sesuai konten lokal, pemberdayaan potensi lokalnya yang sesuai dengan norma
  dan nilai pancasila
solusi: |-
  Mengadakan pelatihan dan pendampingan tentang keradioan dan etika kepenyiaran dan pendalaman tentang Pancasila.
  Pihak yang menerima manfaat dari proyek ini adalah para praktisi radio dan mahasiswa komunikasi.
sukses: Melihat dari program yang disiarkan
organisasi: Radio Rakosa
target: para praktisi radio dan mahasiswa komunikasi
---


### {{ page.nohibah }} - {{ page.title }}

---
