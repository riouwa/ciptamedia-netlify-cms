---
title: Ibu Rumah Tangga Melek Media Internet
date: 2011-09-16 11:08:00 +07:00
nomor: 120
foto: /static/img/hibahcmb/120.jpg
nama: Muslimin B. Putra
lokasi: Makassar, Sulawesi Selatan
dana: 100 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: September-Januari 2012 (enam bulan)
deskripsi: Sebuah proyek pemberdayaan media internet badi para ibu rumah tangga dalam
  berpartisipasi aktif sebagai subjek dan pelaku pertukaran informasi
masalah: 'Beberapa masalah yang ingin diatasi melalui proyek ini adalah: (1) mengenalkan
  media internet sebagai peluang bisnis, dan peluang lainnya yang berhubungan dengan
  kegiatan ibu rumah tangga; (2) mendekatkan ibu rumah tangga sebagai pelaku informasi,
  bukan sekedar konsumen informasi'
solusi: 'Cara mengatasinya dengan (1) pengenalan media melalui pertemuan informasi
  para ibu rumah tangga seperti arisan RT, pengajian, dll; (2) pelaksanaan workshop
  menulis bagi ibu rumah tangga; (3) pelaksanaan media visit bagi ibu rumah tangga;
  (4) pembuatan/pengenalan beberapa media yang dapat diakses ibu rumah tangga '
sukses: Berdasarkan tingkat kompetensi ibu rumah tangga dalam membuat tulisan/berita/artikel
  dan frekuensi ibu rumah tangga sebagai subyek informasi dalam menyebarkan informasi
  yang dibuatnya
target: Pemerintah setempat dan ibu rumah tangga
organisasi: NA
---


### {{ page.nohibah }} - {{ page.title }}

---
