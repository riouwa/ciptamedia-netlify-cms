---
title: Media Lokal berbasis Citizen Jurnalism
date: 2011-09-16 11:08:00 +07:00
nomor: 156
foto: /static/img/hibahcmb/156.jpg
nama: Sarinah
lokasi: Palu, Sulawesi Tengah
dana: 25 Juta Rupiah
topik: Kebebasan dan etika bermedia
durasi: Januari - Juli 2012
deskripsi: Sebuah proyek yang didirikan untuk mendorong publik ditingkatan lokal untuk
  menulis dengan website sebagai pusat konsentrasi informasi dari segala jaringan.
  Media ini juga akan dibentuk dalam versi cetak untuk menjangkau komunitas yang belum
  mampu mengakses internet. Proyek ini juga ditujukan untuk pengembangan manajemen
  mandiri, pengawalan dan advokasi pengembangan media lokal berbasis citizen jurnalism
masalah: Budaya lisan yang masih dominan dalam kesadaran publik, publik yang masih
  pasif sebagai informan sehingga informasi kebanyakan bersifat top-down, dan citizen
  jurnalism masih dipandang sebelah mata
solusi: Bertindak sebagai alat pendorong agar publik menjadi aktif dalam memberikan
  informasi (sebagai jurnalis), mengadakan kampanye dan pelatihan kepada publik yang
  bertujuan membumikan citizen jurnalism, dan mengembangkan kualitas citizen jurnalism
  dari segi konten, kualitas tulisan, teknologi, media, etika dan manfaat
sukses: Secara fisik, jika ada media lokal berbasis citizen jurnalism yang berwujud
  cetak dengan sirkulasi yang regular. Secara kultural, jika terjadi peningkatan kuantitatif
  maupun kualitatif partisipasi publik lokal dalam program citizen jurnalism ini
target: Publik lokal/komunitas kota Palu dan sekitarnya
organisasi: NA
---


### {{ page.nohibah }} - {{ page.title }}

---
