---
title: Taman Bacaan Masyarakat Berbasis Multimedia
date: 2011-09-16 11:08:00 +07:00
nomor: 103
foto: /static/img/hibahcmb/103.jpg
nama: Didy Mu'arif
lokasi: Cilacap, Jawa Tengah
dana: 150 Juta Rupiah
topik: Meretas batas - kebhinekaan bermedia
durasi: Diatas 1 tahun dengan jangka waktu yang tidak ditentukan
deskripsi: Taman Bacaan Masyarakat (TBM) yang berbasis multimedia, computer and internet
  working dimana akan menjadi TBM pertama di Kabupaten Cilacap yang berbasis multimedia.
  TBM di pedesaan digunakan sebagai media untuk meretaskan batas, dimana selama ini
  terjadi kesenjangan dalam memperoleh informasi dan komunikasi antara desa dan kota.
  Dengan berbasiskan multimedia, TBM ini akan memiliki konten intranet, internet dan
  e-Library, yang diharmoniskan dengan kebudayaan lokal. Dengan sasaran pengunjung
  adalah penduduk desa, para pengunjung akan diberikan kenyaman dalam memperoleh informasi,
  karena tempat yang dirancang di alam terbuka dan arena yang nyaman untuk anak-anak
  dalam berekspresi setelah mendapatkan informasi yang diinginkan
masalah: Kurangnya akses informasi di daerah pedesaan
solusi: Dengan mendirikan taman bacaan yang berbasis multimedia yang didukung oleh
  konten e-library, intranet dan internet untuk memberikan kenyamanan dalam mendapatkan
  akses informasi
sukses: Berdasarkan banyaknya jumlah pengunjung pada Taman Bacaan Masyarakat
  tersebut dan survey yang akan kami lakukan tiap 3 bulan sekali
organisasi: NA
target: Anak-anak, orang dewasa, dan orang tua yang berada dipedesaan di wilayah
  Kecamatan Jeruklegi, Kabupaten Cilacap
---


### {{ page.nohibah }} - {{ page.title }}

---
