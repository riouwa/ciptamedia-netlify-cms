---
title: Kantor Berita Komunitas
date: 2011-09-16 11:08:00 +07:00
nomor: 087
foto: /static/img/hibahcmb/087.jpg
nama: K. Wahyu Mulyawan
lokasi: Aceh
dana: 120 Juta Rupiah
topik: Keadilan dan kesetaraan akses terhadap media
durasi: Januari 2012-Desember 2012 (satu tahun)
deskripsi: Menyebarluaskan informasi kepada masyarakat melalui radio komunitas, buletin
  warga, dan sms guna mengurangi kesenjangan informasi antara kaum yang sudah kenal
  media online dengan masyarakat desa yg miskin informasi
masalah: Akses informasi melalui internet dan media cetak di daerah pedesaan masih
  sangat sedikit. Walau demikian, pendengar radio dan pemilik hp relatif lebih banyak
  sehingga dengan adanya kantor berita komunitas diharapkan akan ada keadilan dan
  kesetaraan bagi akses informasi kelompok marjinal
solusi: Membagikan buletin mingguan di warung kopi, menyebarkan berita pendek melalui
  sms kepada anggora komunitas, dan penyiaran radio melalui siaran bersama/berjaringan
sukses: Dengan melakukan survey baseline dan endline
organisasi: Radio Komunitas Genta FM
target: Warga marjinal pedesaan di wilayah Aceh Barat dan Selatan
---


### {{ page.nohibah }} - {{ page.title }}

---
