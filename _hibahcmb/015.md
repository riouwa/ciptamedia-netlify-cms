---
title: Dunia Wayang adalah Cermin Dunia Kita
date: 2011-09-16 11:08:00 +07:00
nomor: '015'
foto: /static/img/hibahcmb/015.jpg
nama: Pitoyo Amrih
lokasi: Sukoharjo, Solo, Jawa Tengah
dana: 236 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Januari 2012 – Desember 2013 (2 tahun)
deskripsi: Sebuah proyek yang berusaha untuk menjadikan wayang sebagai jembatan antara
  generasi muda dengan nilai kearifan budaya lokal, dimana pemberdayaan diri adalah
  fondasi, untuk membentuk karakter bangsa Indonesia
masalah: Adanya jarak antara generasi muda dan nilai kearifan budaya lokal dimana
  wayang bisa menjembatani hal tersebut
solusi: Membuat proyek buku dan web mengenai kisah wayang secara lengkap berdasarakan
  3 perspektif wayang (pustaka, seni pertunjukkan, dan nilai). Yang diuntungkan melalui
  proyek ini adalah generasi muda Indonesia
sukses: Nilai kearifan budaya Jawa yang diwakili melalui budaya wayang dikenal
  luas terutama oleh kalangan berusia 15-30 tahun
organisasi: Pitoyo Amrih (pitoyo.com)
target: Generasi muda
---


### {{ page.nohibah }} - {{ page.title }}

---
