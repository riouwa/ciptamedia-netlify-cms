---
title: Membangun Budaya Damai di Papua
date: 2011-09-16 11:08:00 +07:00
nomor: 628
foto: /static/img/hibahcmb/628.jpg
nama: SKPKC Fransiskan Papua
lokasi: Jayapura, Papua
dana: 200 Juta Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Januari 2012 – Juni 2012
deskripsi: Kami mau mengkampanyekan Papua Tanah Damai melalui video. Dengan video
  ini diharapkan masyarakat di Papua yang saat ini sedang bergejolak baik antara Papua
  dan non Papua, beda agama, semuanya sadar dan bersama-sama membangun perdamaian
  di Tanah Papua.
masalah: Masalah yang akan kami atasi adalah mendorong upaya terwujudnya perdamaian
  di tengah gejolak masyarakat Papua (khususnya kelompok muda) yang mau merdeka.
solusi: 'a. Mendiskusikan rencana video Papua Tanah Damai bersama tokoh agama, pemuda
  dan suku-suku. b. Pembuatan video; alur cerita, pengambilan gambar dan editing.
  c. Sosialisasi/pemutaran video. d. Diskusi: Target group adalah pemuda sebanyak
  enam group masing-masing dua group di setiap kota (Jayapura, Abepura & Sentani)'
sukses: 'Terjadi dialog/diskusi untuk membangun budaya damai antara: Papua &
  non Papua, kelompok-kelompok agama (Kristen, Islam, Hindu, Budha & Katolik).'
organisasi: SKPKC Fransiskan Papua
target: Target group adalah pemuda sebanyak enam group masing-masing dua group
  di setiap kota (Jayapura, Abepura & Sentani)
---


### {{ page.nohibah }} - {{ page.title }}

---
