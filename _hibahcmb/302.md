---
title: Trashgraph
date: 2011-09-16 11:08:00 +07:00
nomor: 302
foto: /static/img/hibahcmb/302.jpg
nama: Revolustar Environmental Graphic Design
lokasi: Bandung, Jawa Barat
dana: 1,5 Miliar Rupiah
topik: Meretas batas – kebhinekaan bermedia
durasi: Januari 2012 – Januari 2014 (2 tahun)
deskripsi: Sebuah proyek perancangan tempat sampah yang unik agar menarik minat masyarakat
  untuk memilah sampah dan menyertakan program lanjutan pemberdayaan masyarakat melalui
  pengolahan sampah dan pemanfaatan teknologi informasi sebagai pelengkap media tersebut
masalah: Menjamurnya media-media informasi dimasyarakat yang tidak terkontrol dengan
  baik sehingga menjadi tidak efisien dan menjadi “sampah visual” karena bersifat
  masif, searah, dan tidak memberi manfaat bagi masyarakat juga pola kebiasaan masyarakat
  yang membutuhkan media-media edukatif lingkungan sehingga dapat tercipta budaya
  masyarakat yang ramah lingkungan
solusi: Perlu dirancang media-media informasi yang ditempatkan secara bertahap (mulai
  dari lingkungan yang kecil) beserta edukasi tentang media yang ditempatkan beserta
  cara penggunaanya. Juga disebutkan nilai-nilai positif yang akan diambil manfaatnya
  oleh masyarakat. Proyek ini akan memberi keuntungan kepada masyarakat luas dan lokasi
  berdasarkan area yang disepakati untuk penempatan media ini (baik sekolah, perkantoran,
  perumahan, juga masyarakat luas)
sukses: Terciptanya budaya mengurangi, memilah, dan mengolah sampah yang menjadi
  sumber daya berbasis kerakyatan
organisasi: Revolustar Environmental Graphic Design
target: Masyarakat luas dan lokasi berdasarkan area yang disepakati untuk penempatan
  media ini (baik sekolah, perkantoran, perumahan, juga masyarakat luas)
---


### {{ page.nohibah }} - {{ page.title }}

---
